"""A minimal model for deeevoLab"""

from time import sleep

import numpy as np

import deeevolab as dvl
import deeevolab.universe

log = dvl.setup_logger(__name__, level=dvl.get_log_level())

class Minimal(dvl.universe.Universe):
	"""A minimal universe, just sleeping ..."""

	def __init__(self, *args, **kwargs):
		"""Initialise a minimal universe.

		This is used for testing the basic functionality of deeevolab.
		"""
		# Add some attributes that will be monitored
		self.delay = None
		# NOTE this needs to happen before initialising the parent in order to
		# allow monitor initialisation

		# Call the parent
		super().__init__(*args, **kwargs)

		# And any initialisation post-processing
		self._init_finished()

	def run_simulation_step(self, min_delay: float, max_delay: float) -> bool:
		"""The method to perform one step of the simulation.

		For testing, this only sleeps for a random amount of time
		"""
		self.delay = min_delay + np.random.rand() * (max_delay - min_delay)
		log.state("    Continuing with {} s delay ...".format(self.delay))
		sleep(self.delay)

		return True

# ...and a special name for the CLI of deeevoLab to get the default universe
Universe = Minimal
base_uni_cfg = {}
