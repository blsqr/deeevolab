"""This module tests the Multiverse class."""

import os
from pkg_resources import resource_filename

import pytest

from deeevolab.universe import Universe
from deeevolab.multiverse import Multiverse
from deeevolab.config import load_cfg_file

from .minimal import Minimal
from .network_model import NetworkModel

# Local variables
MINIMAL_CFG = resource_filename('tests.deeevolab', 'cfg/minimal_cfg.yml')


# Fixtures --------------------------------------------------------------------

@pytest.fixture
def mv(tmpdir) -> Multiverse:
	"""Returns a Multiverse of the Minimal universe"""
	return Multiverse(Minimal, out_basedir=tmpdir)

@pytest.fixture
def minimal_cfg() -> dict:
	"""Load a configuration for the Minimal Universe defined above"""
	return load_cfg_file(MINIMAL_CFG)

# Tests -----------------------------------------------------------------------

def test_init(tmpdir):
	"""Test the initialisation of a Multiverse object."""
	mv = Multiverse(Minimal, out_basedir=tmpdir)

def test_run_single(mv, minimal_cfg):
	"""Run a single simulation"""
	mv.run_single(minimal_cfg['pspace'])

	# Assert that directory was created, data written
	uni_dir = os.path.join(mv.dirs['universes'], "uni0")
	assert os.path.isdir(uni_dir)
	assert os.path.isfile(os.path.join(uni_dir, "data", "universe.hdf5"))

def test_parameter_sweep(mv, minimal_cfg):
	"""Run a parameter sweep"""
	mv.parameter_sweep(pspace=minimal_cfg['pspace'])

	# Assert that universe folders were created
	assert os.path.isdir(os.path.join(mv.dirs['universes'], "uni00"))
	assert os.path.isdir(os.path.join(mv.dirs['universes'], "uni11"))
