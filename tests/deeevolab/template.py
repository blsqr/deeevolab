#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''This is a template Universe class, which can be used to create new projects.

Just copy the whole file and fill in all the # TODO parts ...'''

__version__ 	= "0.0"

from typing import Union

import deeevolab as dvl
from deeevolab.logging import setup_logger
from deeevolab.config import get_config
from deeevolab.universe import Universe

# Initialise loggers and config file
log 	= setup_logger(__name__, level=dvl.get_log_level(is_universe=True))
cfg 	= get_config(__name__, is_project_module=True)
log.debug("Loaded module, logger, and config.")
# -----------------------------------------------------------------------------

class Template(Universe):
	'''The Universe is where everything takes place. It is a child of the ParentUniverse class and provides an interface for management of the simulation, plotting of data and saving of data.

	# TODO write what this specific Universe class does
	'''

	def __init__(self, *args, template_init: dict, **kwargs):

		# Initialise the parent universe
		super().__init__(*args, **kwargs)

		# Initialise attributes and other things ...
		# TODO Do something with template_init here. You can also add additional kwargs, just make sure that it represents the config file structure

		# Initialise monitors here, if needed
		self.setup_monitors(**cfg.universe_monitors)
		# TODO configure monitors in the project's config file

		# Save the initialisation parameters as metadata
		# args and kwargs are saved in ParentUniverse
		log.debug("Adding metadata to init_kwargs ...")
		self.metadata.update_item('init_kwargs',
		                          dict(template_init=template_init))
		# TODO add other initialisation data metadata

		# Use the method implemented in ParentUniverse to finish initialisation
		self._init_finished()

		log.debug("Finished initialisation of Template Universe.")

		return

	# Simulation ..............................................................

	def run_simulation_step(self, **step_kwargs) -> bool:
		'''Runs one step of the network simulation. Is called by run_simulation(), which is implemented in the parent Universe.

		Args:
			# TODO specify the arguments explicitly, not with *+step_kwargs
			# TODO write documentation for step_kwargs
		'''

		# TODO Perform the simulation step ...

		# Everything ok, return True.
		# Returning False here triggers the stop of the simulation; however, this should best be detected by the stop conditions -- they allow fine-grained control over when, why, and how the simulation stopped -- and not by just returning False here.
		return True

		# Action methods ..........................................................

	# Action methods ..........................................................
	# Analysis . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	# Plotting . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	# Saving . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	# Stop Conditions .........................................................
	# To implement project-specific stop conditions, you only need to add private methods here, which have the name pattern _sc_{method_name}, and add the corresponding configuration to cfg.pspace.run_kwargs.stop_kwargs (in your project's config file!) -- it is not necessary to subclass check_stop_conditions.
	# Example below

	def _sc_my_stop_cond(self, **kwargs) -> Union[bool, str]:
		'''A template for a stop condition, which receives'''

		# TODO Evaluate a custom stop condition here

		# If False is returned, the stop condition is not fulfilled; if anything evaluated to True is returned, it is fulfilled. If a string is returned, it can be used as the stop condition message.
		return False
	_sc_my_stop_cond.REQUIRED_MONS = [] # TODO enter the required monitors here


	# Helper methods ..........................................................

	def _increment_steps(self):
		''' Increment the step size of the universe and all Environment-derived objects, that are affected by this class's run_simulation_step method, i.e. the compartments.
		'''
		super().increment_steps() # increment those of the universe itself

		# TODO may want to do something here, if there are any Environment-derived objects that this Universe keeps track of ...

		return

