#!/usr/bin/env python3
"""CLI for evaluation of deeevoLab simulation data

This script provides an interface for the deeeval package.
"""

import os
import copy
import shutil
import argparse
from time import sleep
from typing import Sequence

import deeeval
from deeeval.tools import tty_centred_msg

if __name__ != '__main__':
	print("This script should not be imported!")
	sys.exit(1)

# Setup logging
log	= deeeval.get_logger(__name__)
log.debug("Logger loaded.")

# Local constants
SLEEP_TIME = 2.  # between KeyboardInterrupts to be propagated (in seconds)

# -----------------------------------------------------------------------------
# Helper methods

def process_args(clargs: dict) -> Sequence[dict]:
	"""Processes the command line arguments and returns a sequence of
	dictionaries that are used to run one instantiation of a call."""

	# Create multi arguments list; each entry of this is a dictionary that has
	# the configuration for a single evaluation and plotting run.
	if not clargs['batch']:
		# A single configuration run, consisting only of the passed args dict
		log.note("Using command line arguments.")
		margs = dict(_=clargs)

	else:
		# A multi configuration run, consisting of the arguments given there, updated with the values from the passed command line arguments.
		log.note("Using information from file for batch evaluation.")
		log.state("  %s", clargs['batch'])

		# Create the dict by loading from config file
		margs = deeeval.load_cfg_file(clargs['batch'])['batch']

		# Remove the disabled entries
		margs = {k:v for k, v in margs.items() if v.get('enabled', True)}

		# Update each entry with the command line arguments
		log.note("Updating batch evaluation configuration with command line arguments.")

		for k, args in margs.items():
			margs[k] = {k:v if v is not None else copy.deepcopy(args.get(k))
			            for k, v in clargs.items()}
			# NOTE this makes sure that all entries that were defined in the
			# CLI are present, even if with None as default value; at the same
			# time it updates the arguments read from the config file with
			# those passed in the command line

	# Finished preparations

	# Iterate over the list and update the content of each of the elements by processing the arguments defined there
	for n, kv in enumerate(margs.items()):
		# Split into key and value pairs
		key, args = kv

		if len(margs) > 1:
			log.highlight("Batch evaluation %d/%d: processing arguments ...",
			              n+1, len(margs))

		# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
		# If `--from-file` argument was given, use that to fill the args dictionary
		if args['from_file']:
			log.progress("Reading arguments from file...")
			log.state("Full path:\n  %s", args['from_file'])
			ffargs = deeeval.load_cfg_file(args['from_file'])
			log.debugv("File contents:\n  %s", deeeval.PP(ffargs))

			# Create a dictionary with the values from the already present args and the ffargs, where the cliargs take priority
			args = {k:v if v is not None
			        else copy.deepcopy(ffargs.get(k))
			        for k, v in args.items()}

			log.debugv("Combined arguments from file with command line "
			           "arguments (the latter taking priority).\n  %s",
			           deeeval.PP(args))
		else:
			# Not using arguments from file
			ffargs = {}

		# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
		# Now get to the loading of configurations. These might have been passed from file and could also have been supplied as dictionaries already, in which case it is not necessary to load them from file.

		# Determine the load dictionary
		if args['load_cfg']:
			if isinstance(args['load_cfg'], str):
				log.progress("Loading custom load configuration ...")
				load_cfg = deeeval.load_cfg_file(args['load_cfg'])

				log.state("  load_cfg <- yml\n  %s", args['load_cfg'])

			else:
				load_cfg = args['load_cfg']

				# Inform about which one is being used
				if ffargs.get('load_cfg'):
					log.state("  load_cfg <- file")
				else:
					log.state("  load_cfg <- batch config")
		else:
			load_cfg = None


		# Determine the plot dictionary by loading the specified config file
		if isinstance(args['plot_cfg'], str):
			log.progress("Loading custom plot configuration ...")
			plot_cfg = deeeval.load_plt_config(args['plot_cfg'],
			                                   plot_only=args['plot_only'])

			log.state("  plot_cfg <- yml\n  %s", args['plot_cfg'])

		elif args['batch'] or ffargs:
			plot_cfg = args['plot_cfg']

			# Inform about which one is being used
			if ffargs.get('plot_cfg'):
				log.state("  plot_cfg <- file")
			else:
				log.state("  plot_cfg <- batch config")

		else:
			raise ValueError("No plot config given, neither directly nor from "
			                 "a batch run or a file.")


		# The update dicts for both the load and the plot configs
		update_load_cfg = None
		update_plot_cfg = None

		if args['update_load_cfg']:
			if isinstance(args['update_load_cfg'], str):
				log.progress("Loading config file to update load configuration ...")
				update_load_cfg = deeeval.load_cfg_file(args['update_load_cfg'])
				log.state("  update_load_cfg <- yml\n  %s",
				          args['update_load_cfg'])

			else:
				update_load_cfg = args['update_load_cfg']

				# Inform about which one is being used
				if ffargs.get('update_load_cfg'):
					log.state("  update_load_cfg <- file")
				else:
					log.state("  update_load_cfg <- batch config")

		if args['update_plot_cfg']:
			if isinstance(args['update_plot_cfg'], str):
				log.progress("Loading config file to update plot configuration ...")
				update_plot_cfg = deeeval.load_cfg_file(args['update_plot_cfg'])

				log.state("  update_plot_cfg <- yml\n  %s",
				          args['update_plot_cfg'])

			else:
				update_plot_cfg = args['update_plot_cfg']

				# Inform about which one is being used
				if ffargs.get('update_plot_cfg'):
					log.state("  update_plot_cfg <- file")
				else:
					log.state("  update_plot_cfg <- batch config")


		# Put the loaded dictionaries into an args sub-dictionary
		args['dicts'] = dict(load_cfg=load_cfg,
		                     update_load_cfg=update_load_cfg,
		                     plot_cfg=plot_cfg,
		                     update_plot_cfg=update_plot_cfg)

		if len(margs) > 1:
			log.progress("Finished processing arguments for this entry.")
		# Done with this loop iteration
	# Done with the loop
	# Return list of argument dicts
	return margs

def backup_cfg_files(*, args: dict, cfg_dir: str) -> None:
	"""Copies the involved config files to the output directory.
	
	Args:
	    args (dict): Command line arguments
	    cfg_dir (str): The path to the config directory
	"""

	log.progress("Copying provided config files to output directory ...")
	dst_names = [
		'load_cfg', 'update_load_cfg',
		'plot_cfg', 'update_plot_cfg',
		'fromfile'
	]
	src_files = [args.get(dn, None) for dn in dst_names]

	# Go over the config files and copy them to the output cfg directory
	for src, dst_name in zip(src_files, dst_names):
		if not src:
			log.debugv("No %s given to copy.", dst_name)
			continue
		elif not isinstance(src, str):
			log.debugv("Was not a file path: %s", src)
			continue

		# Create destination path and create the directory
		dst = os.path.join(cfg_dir, dst_name+".yml")
		os.makedirs(os.path.dirname(dst), exist_ok=True)

		# Perform the copying.
		shutil.copy2(src, dst)
		# NOTE using copy2 as it preserves metadata and permissions
		log.debug("Copyied %s to output directory ...", dst_name)
		log.debugv("Full path:\n  %s", dst)

	# Also save the batch file, if there is one
	if args['batch']:
		shutil.copy2(args['batch'],
		             os.path.join(cfg_dir, os.path.basename(args['batch'])))
	# Done.
	log.note("Config file backup finished.")

def plots_enabled(plot_cfg) -> bool:
	"""Check if there is at least one plot enabled in the given plot configuration."""
	log.debug("Checking, if there is at least one plot enabled in the given plot configuration ...")
	if not plot_cfg:
		log.debug("No plot configuration given.")
		return False

	for name, params in plot_cfg.items():
		if params.get('enabled', True):
			log.debug("Plot '%s' is enabled. Returning True ...")
			return True
	return False

def load_and_plot(dm, *, name, args) -> None:
	"""Load the data and perform the plots. """

	# Check if at least one plot is enabled in both the plot config and the updated plot config
	if not plots_enabled(args['dicts']['plot_cfg']):
		# Check the update dict
		if (not args['dicts']['update_plot_cfg']
		    or not plots_enabled(args['dicts']['update_plot_cfg'])):
			log.caution("No plots enabled. Not continuing ...")
			return
		# else: there were some enabled ... continue.

	# Backup the configuration files
	backup_cfg_files(args=args, cfg_dir=dm.dirs['cfg'])

	# Load data
	log.highlight("\n\n%s", tty_centred_msg(" Loading data ... "))
	try:
		# Call the load method. This will use the load config passed at initialisation or, if none was passed, the DataManager defaults
		dm.load_data(update_load_cfg=args['dicts']['update_load_cfg'],
		             log_data=args['show_tree'])

	except deeeval.exc.RequiredDataMissingError:
		log.error("Not carrying out this evaluation ...\n\n\n")
		return
	# Data is now available in the DVLDataManager object

	# Call the plotting method with the desired plot configuration.
	log.highlight("\n\n%s", tty_centred_msg(" Performing plots ... "))

	dm.perform_plots(plot_cfg=args['dicts']['plot_cfg'],
	                 update_plot_cfg=args['dicts']['update_plot_cfg'])

# -----------------------------------------------------------------------------
# Main code -------------------------------------------------------------------
log.debug("Setting up ArgumentParser ...")

# Setup argument parser and command line arguments
parser = argparse.ArgumentParser(description="Use deeeval to perform "
                                             "evaluation and plots of "
                                             "deeevoLab simulations.",
                                 epilog="Happy evaluations! :)",
                                 fromfile_prefix_chars='@',
                                 prog='deeeval')
parser.add_argument('data_dir',
                    action='store',
                    nargs='?',
                    help="The data directory to be loaded and plotted.")
parser.add_argument('-o', '--out-dir',
                    nargs='?',
                    dest='out_dir',
                    default=None,
                    help="Where the output directory should be created. By "
                         "default, it is created in a sub-directory inside "
                         "the data directory.")
parser.add_argument('-p', '--plot-cfg',
                    nargs='?',
                    dest='plot_cfg',
                    default=None,
                    help="From which *.yml file to read the list of plots to "
                         "perform. This arugment is required if not using the "
                         "--from-file or --batch flags.")
parser.add_argument('--update-plot-cfg',
                    nargs='?',
                    dest='update_plot_cfg',
                    default=None,
                    help="If supplied, the content of this *.yml file will be "
                         "used to recursively update the previously "
                         "determined plot configuration. This can be used to "
                         "extend the plot configuration or make minor "
                         "changes, while baseing this on another file or the "
                         "defaults.")
parser.add_argument('--plot-only',
                    nargs='*',
                    dest='plot_only',
                    default=None,
                    help="The plot names specified here will be the only "
                         "ones plotted, even if they are disabled in the plot "
                         "configuration. All other plots will be disabled.")
parser.add_argument('-l', '--load-cfg',
                    nargs='?',
                    dest='load_cfg',
                    default=None,
                    help="From which *.yml file to read the load "
                         "configuration, i.e., which data should be imported "
                         "and how. If none is given, the default load "
                         "configuration of the DataManager will be used.")
parser.add_argument('--update-load-cfg',
                    nargs='?',
                    dest='update_load_cfg',
                    default=None,
                    help="If supplied, the content of this *.yml file will "
                         "be used to recursively update the previously "
                         "determined load configuration during (!) the "
                         ".load_data() call. This can be used to extend the "
                         "load configuration or make minor changes, while "
                         "baseing this on a given file or the defaults.")
parser.add_argument('--dm-name',
                    nargs='?',
                    dest='dm_name',
                    default=None,
                    help="The name of the data manager.")
parser.add_argument('--raise', '--debug',
                    action='store_true',
                    dest='debug',
                    default=None,
                    help="In debug mode, more (but not all) caught "
                         "exceptions are raised instead of just logging an "
                         "error. Use this to get a traceback to the error.")
parser.add_argument('--show-tree',
                    action='store_true',
                    default=None,
                    help="Enables the logging of the data tree. This flag "
                         "should not be supplied if a lot of data is "
                         "imported; the tree output will significantly slow "
                         "down the script execution.")
parser.add_argument('-f', '--from-file',
                    nargs='?',
                    default=None,
                    help="Uses the content of the given file for the "
                         "arguments or (if applicable) for the actual "
                         "dictionary values that would otherwise be read from "
                         "the file referenced to in the argument. The file "
                         "has to be in YAML format and have as root level "
                         "keys the names of this CLI arguments (with "
                         "underscores instead of dashes). Any argument given "
                         "in the command line still takes precedence over "
                         "the ones given in this file.")
parser.add_argument('-b', '--batch',
                    nargs='?',
                    dest='batch',
                    default=None,
                    help="Do a batch of evaluations. If given, the passed "
                         "*.yml file is read and the contents of the key "
                         "`batch` (a list of dictionaries where each "
                         "dictionary has the information for one run of this "
                         "script) determines which arguments to use. The "
                         "other command line arguments update the arguments "
                         "provided in that file.")
parser.add_argument('--version',
                    action='version',
                    version="%(prog)s " + deeeval.__version__)

# Parse these arguments and move them over into a dictionary
cliargs = parser.parse_args()
args = vars(cliargs)
log.debug("Command line arguments parsed. Got:\n  %s", deeeval.PP(args))

# Process args
log.info("Processing arguments ...")
multi_args = process_args(args)
# This is now a list of args dictionaries, that can be iterated over

# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
# Perform the evaluation runs.
for n, kv in enumerate(multi_args.items()):
	# Unpack the kv pair
	name, args = kv

	# Extract the output directory
	out_dir = args['out_dir']

	# Distinguish between a single run and a batch evaluation
	if args['batch']:
		_msg = " Evaluation {} / {} : {name:} ".format(n+1,len(multi_args),
		                                               name=name)
		log.highlight("%s%s", "\n"*6, tty_centred_msg(_msg, fill_char="-"))

		# If it is set, out directory gets an additional sub directory with the name of the current batch iteration
		if out_dir:
			out_dir = os.path.join(out_dir, "{name:}")
		# This is resolved inside the DataManager

	else:
		# A single run; not having a reasonable name for DataManager defined.
		# Choose the basename instead ...
		if not args['data_dir']:
			raise ValueError("Need argument `data_dir` specified!")
		name = os.path.basename(args['data_dir']) + "_Manager"

	# Set the debug status for this iteration
	if args['debug']:
		deeeval.DEBUG = deeeval.deval.DEBUG = True
		log.debugv("deeeval.DEBUG: %s", deeeval.DEBUG)
		log.debugv("deeeval.deval.DEBUG: %s", deeeval.deval.DEBUG)

	# Initialise DataManager object from the given data directory
	dm = deeeval.DVLDataManager(name=name, data_dir=args['data_dir'],
	                            out_dir=out_dir,
	                            load_cfg=args['dicts']['load_cfg'])

	try:
		# Call the load and plot methods -- most time is spent in here
		load_and_plot(dm, name=name, args=args)

	except KeyboardInterrupt:
		log.warning("Received KeyboardInterrupt. Aborting this evaluation.\n\nPerform KeyboardInterrupt again (within %ss) to stop evaluation altogether.\n", SLEEP_TIME)

		try:
			sleep(SLEEP_TIME)
		except KeyboardInterrupt:
			log.warning("Aborting all remaining evaluations ...\n\n\n")
			break
		
		# Did not break
		log.highlight("Continuing with other evaluations ...")
	# Done with this iteration

# Done with everything.
log.highlight("\n\n%s\n\n", tty_centred_msg(" All done. ", fill_char="-"))
