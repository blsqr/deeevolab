#!/usr/bin/env python3

from setuptools import setup

# Dependency lists
install_deps = [
    'colorlog>=2.7.0',
    'dill>=0.2.7.1',
    'easydict>=1.6',
    'h5py>=2.6.0',
    'matplotlib>=2.2',
    'numpy>=1.14',
    'scipy>=1.0',
    'PyPDF2>=1.26.0',
    'PyYAML>=3.12',
    'seaborn>=0.8',
    # NOTE the below deps need to have been installed prior to setup
    'easy_mp>=0.1',
    'paramspace>=1.1',
    'deval>=0.3.7'            # TODO migrate to dantro
    ]
test_deps = ['pytest>=3.4.0', 'pytest-cov>=2.5.1']

# -----------------------------------------------------------------------------
setup(name='deeevolab',
      # Version specification
      version='0.7.0-pre.0',
      # NOTE also adjust in deeevolab/__init__.py and deeeval/__init__.py
      description="A framework for investigating evolutionary systems",
      # long_description=(""),
      author="Yunus Sevinchan",
      author_email='Yunus.Sevinchan@iup.uni.heidelberg.de',
      licence='MIT',
      url='https://ts-gitlab.iup.uni-heidelberg.de/yunus/deeevolab',
      classifiers=[
          'Intended Audience :: Science/Research',
          'Programming Language :: Python :: 3.6',
          'Topic :: Utilities'
      ],
      packages=['deeevolab', 'deeeval'],
      package_data=dict(deeevolab=["*.yml"], deeeval=["*.yml"]),
      scripts=['cli/dvl-run', 'cli/deeeval'],
      install_requires=install_deps,
      tests_require=test_deps,
      test_suite='py.test',
      extras_require=dict(test_deps=test_deps)
)
