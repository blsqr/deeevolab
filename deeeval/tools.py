'''A collection of general tools.

Everything in here should stand on its own and not depend on anything else in the deval package.

NOTE: this file is made available via the deeeval.__init__ module, and can thus not depend on any other modules. Furthermore, it does not have access to a config file.
'''
import logging

import numpy as np

from deval.tools import tty_centred_msg, rec_update

# Setup logging for this file
log	= logging.getLogger(__name__)
log.debug("Loaded logger.")

# Local constants


# -----------------------------------------------------------------------------
# Working on data

def squeeze_first(a: np.ndarray, last_axis: int=None):
	'''Squeezes the axes of the array a one by one until an axis with size > 1 is encountered.'''

	log.debug("squeeze_first called")
	log.debugv("shape: %s,  last_axis: %s", a.shape, last_axis)

	# Determine the axes to squeeze
	to_squeeze 	= () # needs to be a tuple to be recognized by np.squeeze
	for axis, size in enumerate(a.shape):
		if size > 1:
			log.debugv("size > 1 encountered")
			break
		elif last_axis is not None and axis > last_axis:
			log.debugv("last_axis passed")
			break
		# Append to tuple
		to_squeeze += (axis,)

	log.debugv("squeezing: %s", to_squeeze)

	# Now go over a in reverse and squeeze those
	return np.squeeze(a, axis=to_squeeze)


def ensure_num_dims(a, *, num_dims: int, add_axes: str='post') -> np.ndarray:
	'''Ensure a specific number of dimensions for the arrays by adding size-one dimensions in back or front'''

	log.debug("ensure_num_dims called")
	log.debugv("data shape: %s, num_dims: %d", a.shape, num_dims)

	ndims 	= len(a.shape)

	if ndims > num_dims:
		raise ValueError("Too many dimensions for data of shape {} and desired num_dims=={}".format(a.shape, num_dims))

	elif ndims < num_dims:
		if add_axes in ['post', 'in_back']:
			new_shape 	= (a.shape
			               + tuple([1 for _ in range(num_dims - ndims)]))
		elif add_axes in ['pre', 'in_front']:
			new_shape 	= (tuple([1 for _ in range(num_dims - ndims)])
			               + a.shape)
		elif add_axes in ['raise']:
			raise ValueError("The given array (shape {}) does not have the required number of dimensions ({}).".format(a.shape, num_dims))
		else:
			raise ValueError("Invalid argument value for argument `add_axes`: "+str(add_axes))

		log.debugv("Ensuring %d dims by reshaping to: %s", num_dims, new_shape)
		a 	= a.reshape(new_shape)

	else:
		# Nothing to do
		pass

	return a
