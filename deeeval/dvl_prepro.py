''' This module holds plot preprocessing functions.

Guidelines:
	- The preprocessing functions work on single universes, which they get passed with the `uni` argument.
	- The return value of them will automatically be saved to the universe under the name `pp_results`->`{pp_func_name}`.
	- While having access to the wrapper, this should not be abused to change things in the wrapper or even access the data manager and write data there!
	- It should be taken care that the return values of the functions can be processed within a numpy array.
	- Simple mean and std operations should not be implemented here; they are taken care of by the DVLUniverseMap object ...
'''

import copy
from typing import Union, Sequence

import numpy as np

from paramspace import ParamSpace, ParamSpan

import deeeval
from deeeval.dvl_conts import DVLContainer

# Setup logging for this file
log	= deeeval.get_logger(__name__)
cfg = deeeval.get_config(__name__)
log.debug("Loaded module, config, and logger.")

# Local constants

# -----------------------------------------------------------------------------
# General methods

def last_row(wrpr, *, uni, monitor_name: str) -> np.ndarray:
	'''Returns the last available row (i.e.: axis 0) for the monitor with the given name.'''
	return uni['monitors'][monitor_name][-1]
last_row.dim_out = 1

def mask_value_if(wrpr, *, uni, monitor_name: str, threshold: float, operator: str) -> np.ndarray:
	'''Without changing the shape of the monitor, masks a value if it is above or below a threshold value.

	`mode` can be comparison strings: `==`, `<=`, ...
	'''
	# Get the monitor
	mon 			= uni['monitors'][monitor_name]
	mon_masked 		= mon.copy()

	# Get the comparison function
	comp_func 		= get_comparison_func(operator)

	# Create the mask
	mask 			= comp_func(mon, threshold)
	# Has True in it everywhere the condition is fulfilled

	# At the positions where the mask is True, set the values to nan
	# TODO need to convert to float first, if it is integer. Cannot set nan otherwise
	mon_masked[mask]= np.nan

	print(mon_masked)

	return mon_masked
mask_value_if.dim_out = None # doesn't change the dimension of the input

def correlation(wrpr, *, uni, array_keys: Sequence[list], mode: str='full', normalize: bool=False, cut_symmetric: bool=False, allow_shape_mismatch: bool=True, dtype=float) -> np.ndarray:
	'''Cross-correlates the 1D- or 2D-array(s) specified by the `array_keys` argument.'''

	if len(array_keys) == 2:
		arr1_key, arr2_key 	= array_keys
	elif len(array_keys) == 1:
		arr1_key = arr2_key = array_keys[0]
	else:
		raise ValueError("Invalid `array_keys` argument. Expected list of length 1 or 2, got: "+str(array_keys))

	# Check if it will be an acf or a cross correlation
	is_acf 	= bool(arr1_key == arr2_key)

	# Get the arrays. Make sure they are of consistent data type
	arr1 	= uni[arr1_key].copy().astype(dtype)
	arr2 	= uni[arr2_key].copy().astype(dtype)

	# Check for shape mismatch
	if allow_shape_mismatch and arr1.shape != arr2.shape:
		raise NotImplementedError("handling of shape mismatch in correlation")

	# Make sure it is two dimensional -> consistent handling
	if len(arr1.shape) == 1:
		arr1 	= arr1[:, np.newaxis]
	if len(arr2.shape) == 1:
		arr2 	= arr2[:, np.newaxis]

	# List of correlations
	corrs 	= []

	# Find out the number of columns in the second array
	num_arr2_cols 	= arr2.shape[-1]

	# Loop over columns and calculate correlation to the corresponding column in the other array
	# The first array will determine the columns to loop over. If there are more in the first array than in the second array, the second array's columns will be iterated over
	for col_idx in range(arr1.shape[-1]):
		# Calculate the correlation
		corr	= np.correlate(arr1[:, col_idx],
		                       arr2[:, col_idx%num_arr2_cols],
		                       mode=mode)

		# This is now a 1D-array

		# Cut off the symmetric branch, if it is an ACF or the argument was given
		if is_acf or cut_symmetric:
			corr 	= corr[int(len(corr)/2)+1:]

		# Check if it should and can be normalised
		if normalize:
			if (np.nanmax(corr) == 0. or np.isnan(np.nanmax(corr))):
				# Cannot reasonably normalize. Warn.
				log.caution("Cannot normalize the correlated arrays, because the maximum was 0 or NaN.")
			else:
				# Can normalize. Do so.
				corr 	= corr/np.nanmax(corr)

		# Expand to 2D array
		corr 	= corr[:, np.newaxis]

		# Append to list of correlations
		corrs.append(corr)

	# Build a new array
	corrs 	= np.hstack(corrs)

	# Done. Return.
	return corrs
correlation.dim_out = None # doesn't change the dimension of the input

# -----------------------------------------------------------------------------
# Population-related

def final_population_size(wrpr, *, uni, mean_steps: int=None, nan_if_zero: bool=False) -> np.ndarray:
	'''Returns the final sizes of all available populations.'''
	if mean_steps is None or mean_steps <= 1:
		fps = last_row(wrpr, uni=uni, monitor_name='population_sizes')
	else:
		# Get the population size data
		pop_sizes 	= uni['monitors']['population_sizes'][-mean_steps:]
		fps = np.nanmean(pop_sizes, axis=0)

	if nan_if_zero:
		# Conver to float
		fps = fps.astype(float)
		# Change the zeros to NaNs
		fps[fps == 0.] = np.nan

	return fps
final_population_size.dim_out = 1

def sum_individual_rounds(wrpr, *, uni) -> np.ndarray:
	'''For each population, calculates the number of individuals that ever lived in it, by summing up all sizes at all available times.'''
	pop_sizes 	= uni['monitors']['population_sizes']
	return np.sum(pop_sizes, axis=0)
sum_individual_rounds.dim_out = 1

def population_age(wrpr, *, uni) -> np.ndarray:
	'''Calculates the age of each population by summing up the time steps with a population size > 0.'''
	pop_sizes 	= uni['monitors']['population_sizes']

	# Find the values > 0 and then sum all rows together, which results in the number of time steps that the population was alive.
	ages 		= np.sum(pop_sizes > 0, axis=0)

	# Return those values
	return ages
population_age.dim_out = 1

def population_demise_time(wrpr, *, uni) -> np.ndarray:
	'''Returns a vector of population demise times.'''
	pop_sizes 	= uni['monitors']['population_sizes']

	raise NotImplementedError('population_demise_time')
population_demise_time.dim_out = 1

def energy_input_per_individual(wrpr, *, uni) -> np.ndarray:
	'''Calculates the energy input per individual over time. As the energy input at time t is caused by the population that is stored in time (t-1) in the data, there is no value for the initialisation time.'''

	# Get the data, already slicing it such that the arrays align
	pop_E_in 	= uni['monitors']['population_energy_input'][1:]
	pop_sizes 	= uni['monitors']['population_sizes'][:-1]

	# Perform the calculation
	epi 		= pop_E_in/pop_sizes

	# Add one row on top, filled with zeros.
	row 		= np.zeros((1, epi.shape[1]))
	epi 		= np.concatenate((row, epi), axis=0)

	# Make the first row nans
	epi[0, :] 	= np.nan

	# Done. Return.
	return epi
energy_input_per_individual.dim_out = 2

def calc_carrying_capacity(wrpr, *, uni) -> np.ndarray:
	'''Calculates the carrying capacity of a population using the normalised data and multilpying it with the population size.'''
	pop_sizes 	= uni['monitors']['population_sizes']
	ccfrac 		= uni['monitors']['population_carrying_capacity_fraction']

	return ccfrac * pop_sizes
calc_carrying_capacity.dim_out = 2

def mask_N_over_K(wrpr, *, uni) -> np.ndarray:
	'''Masks the first row of the carrying capacity fraction monitor, as the value is not well defined for time step 0.'''
	ccfrac 		= uni['monitors']['population_carrying_capacity_fraction']
	ccfrac[0, :]= np.nan
	return ccfrac
mask_N_over_K.dim_out = 2

def domination_time(wrpr, *, uni, return_val: str=None) -> int:
	'''Returns the time step at which a single population started to dominate; if that is not the case, returns np.inf

	NOTE: A population is NOT seen as dominating if it is extinct at the end of the run. This algorithm requires for domination that at the end of the simulation only one population is alive.

	The algorithm is as follows:
		1) More than one population in the last step -> no domination time
		2) Start at half the simulation time
		3) Check repeatedly the number of populations alive
			a) > 1: look later in time (move half the remaining steps forward)
			b) ==1: look earlier in time (move half the steps backward)
			Break condition: resolution of 1 step -> return the most recent step with one population alive.

	Complexity: log(T), with T being the number of simulation steps
	'''
	# Get the data
	pop_sizes 	= uni['monitors']['population_sizes']

	# Find out if there is a dominating population at the last time step
	if np.sum(pop_sizes[-1] > 0) != 1:
		# More than one alive -> no domination
		return np.inf

	# Else: Only one alive in the end -> dominating

	# Start in the middle of the time steps
	max_n	= pop_sizes.shape[0]
	divs 	= 2 # divisions; splitting the population in this ratio.
	n 		= max_n//divs # start in the middle
	dn 		= max_n//4 # resolution 1/4

	# Go through the times and try to find the transition from >1 populations alive to ==1 populations alive by jumping through the times and checking how many populations are alive at that time. See docstring for the algorithm.
	while dn >= 1:
		if np.sum(pop_sizes[n] > 0) == 1:
			# already in domination regime -> move backward by dn
			n -= dn

		else:
			# not yet in domination regime -> move forward by dn
			n += dn

		# Increase number of divisions (i.e.: higher resolution) and calculate the step to move forward or backward
		divs 	*= 2
		dn 		= max_n//divs

	# while-loop ended, i.e.: dn was == 0 -> last step n is the domination time
	return n
domination_time.dim_out = 0

def dominating_pop_property(wrpr, *, uni, return_val: str='col_no') -> Union[int, float]:
	'''Returns a property of the dominating population or np.nan if no domination was present at the last time step.

	NOTE: A population is NOT seen as dominating if it is extinct at the end of the run. This algorithm requires for domination that at the end of the simulation only one population is alive.
	'''
	# Get the data: the population sizes in the last time step
	pop_sizes 	= uni['monitors']['population_sizes'][-1]

	# Find out the indices of the ones with > 0 population sizes
	alive_idcs	= np.where(pop_sizes > 0)[0]
	if len(alive_idcs) != 1:
		# More than one or none alive -> no domination
		return np.nan
	else:
		# Get the index
		alive_idx	= alive_idcs[0]

	if return_val in ['col_no', 'pop_idx', None]:
		return alive_idx
	else:
		raise NotImplementedError("Other `return_val` arguments apart from 'col_no' not implemented yet.")
dominating_pop_property.dim_out = 0

def num_alive_final(wrpr, *, uni) -> int:
	'''Returns the number of populations that are still alive in the last time step'''
	final_pop_sizes	= uni['monitors']['population_sizes'][-1]
	return np.sum(final_pop_sizes > 0)
num_alive_final.dim_out = 0

def ratio_alive(wrpr, *, uni) -> float:
	'''Returns the ratio of populations that are still alive in the last time step'''
	final_pop_sizes	= uni['monitors']['population_sizes'][-1]
	num_alive 		= np.sum(final_pop_sizes > 0)
	return float(num_alive)/len(final_pop_sizes)
ratio_alive.dim_out = 0

# -----------------------------------------------------------------------------
# Resources
# General

def final_resource_energy(wrpr, *, uni, res_type: str, only_if_pop_alive: str=None) -> np.ndarray:
	'''Returns the primary or secondary resource's final energy. With the `only_if_pop_alive` argument, it can be controlled whether to mask values if populations are no longer alive at the last time step.'''
	res_energy = last_row(wrpr, uni=uni,
	                      monitor_name=res_type+'_resource_energies')

	if only_if_pop_alive is None:
		return res_energy

	elif only_if_pop_alive in ['same_col']:
		# Look at population size in the same column as well; if the population in the same column is not alive, mask the value
		pop_sizes 	= final_population_size(wrpr, uni=uni)

		res_energy 	= [Er if N > 0 else np.nan
		               for Er, N in zip(res_energy, pop_sizes)]

		res_energy 	= np.array(res_energy)

	else:
		raise NotImplementedError("only_if_pop_alive=="+str(only_if_pop_alive))

	return res_energy
final_resource_energy.dim_out = 1

# Primary .....................................................................

def primary_resource_depletion_time(wrpr, *, uni, threshold: float) -> np.ndarray:
	'''Returns a vector of the times a '''
	raise NotImplementedError
primary_resource_depletion_time.dim_out = 1

# Secondary ...................................................................

# -----------------------------------------------------------------------------
# Interaction


# -----------------------------------------------------------------------------
# Helpers

def get_comparison_func(operator: str):
	'''Returns a lambda function for the given operator'''
	if operator not in ['<', '>', '<=', '>=', '==', '!=']:
		raise ValueError("Invalid operator string: "+str(operator))

	return lambda x, y: eval("x {} y".format(operator))
