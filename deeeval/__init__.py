"""The deeeval package extends `deval` to use for analysis of deeevoLab"""

import os
from functools import partial
from pkg_resources import resource_filename

import deval

# The deeeval version
__version__ = '0.7.0-pre.0'
# NOTE this need always match the version in setup.py

# Make formatters available
PP = deval.PP
tree_printer = deval.tree_printer

# The package-wide debug flag
DEBUG = deval.DEBUG

# Default package and log configuration
DEFAULT_CFG	= resource_filename("deeeval", "deeeval_cfg.yml")
DEFAULT_LOG_CFG = resource_filename("deeeval", "log_cfg.yml")

# Setup logging
deval.setup_logger(cfg_path=DEFAULT_LOG_CFG)
get_logger = deval.get_logger
log	= get_logger(__name__)

log.highlight("Initialising deeeval package ...")

# Load additional consructors to
import deeeval.config as config

# Supply a custom config method based on the one in deval
get_config = partial(deval.get_config,
                     cfg_fpath=DEFAULT_CFG, base_module='deeeval',
                     add_constructors=[config.sweep_constructor,
                                       config.coupled_sweep_constructor])

# Load the configuration
cfg	= get_config(__name__)


# Import from other modules such that one can do `import kneval` somewhere and
# be able to access the most important methods.
log.debug("Supplying basic classes and methods ...")

from deval import exc
from deval import load_plt_config, load_cfg_file
from deval import DataContainer, DataGroup, NumpyDC, DataManager, PlotHandler
from deval.data_cont import recursive_convert
# TODO clean these up; the containers should not be available here!

import deeeval.tools as tools
from deeeval.dvl_mngr import DVLDataManager

log.info("deeeval initialised.\n")
