'''Config-related extensions of the deval package, made for deeevoLab.'''

import logging
import yaml

import numpy as np

# NOTE to avoid circular imports, this file may not access any other parts of the deeeval package.

# Setup logging for this file
log	= logging.getLogger(__name__)
log.debug("Loaded module and logger.")

# Local constants

# -----------------------------------------------------------------------------
# Target classes of yaml constructors

class BaseSweep:
	'''A Sweep object is just a place holder. It is initialised with just an order number, which later signifies the order in which a sweep is performed. At that point, the Sweep object is replaced with a new object.'''

	def __init__(self, order: int, values: list=None, info: list=None, name: str=None):
		# Catch infinity cases
		if order == np.inf:
			order 		= 99999999
		elif order == -np.inf:
			order 		= -99999999

		# Save the attributes
		self.order 	= int(order)
		self.values = values
		self.info 	= info
		self.name 	= name

		if values and info and len(values) != len(info):
			log.warning("Arguments `values` and `list` did not have the same length.")

		log.debug("Initialised Sweep object with order: %d", order)

class Sweep(BaseSweep):
	pass

class CoupledSweep(BaseSweep):
	'''A CoupledSweep object can be used to couple to another sweep; with this it is possible to encode information that belongs to another sweep object at another place in the config file.

	Here, the order is used to identify the Sweep that this is coupled to.'''

	@property
	def coupling_order(self):
		'''Get the coupling order.'''
		return self.order


# -----------------------------------------------------------------------------
# Custom yaml constructors

def sweep_constructor(loader, node):
	'''pyyaml constructor for Sweep objects'''

	log.debug("Encountered !sweep tag.")

	if isinstance(node, yaml.nodes.MappingNode):
		# Assuming kwargs
		return Sweep(**loader.construct_mapping(node, deep=True))
	else:
		# Assuming scalar
		return Sweep(order=loader.construct_scalar(node))
sweep_constructor.tag 	= u'!sweep'

def coupled_sweep_constructor(loader, node):
	'''pyyaml constructor for CoupledSweep objects'''

	log.debug("Encountered !coupled-sweep tag.")

	if isinstance(node, yaml.nodes.MappingNode):
		# Assuming kwargs
		return CoupledSweep(**loader.construct_mapping(node, deep=True))
	else:
		# Assuming scalar
		return CoupledSweep(order=loader.construct_scalar(node))

coupled_sweep_constructor.tag 	= u'!coupled-sweep'
