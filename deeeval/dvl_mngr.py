''' This module holds the deeevoLab-specific DataManager child.'''

import dill as pkl

import deeeval
import deeeval.dvl_conts as dcs
from deeeval.dvl_phdlr import DVLPlotHandler

# Setup logging for this file
log	= deeeval.get_logger(__name__)
cfg = deeeval.get_config(__name__)
log.debug("Loaded module, config, and logger.")

# Local constants

# -----------------------------------------------------------------------------

class DVLDataManager(deeeval.DataManager):
	'''The DVLDataManager specialises on managing deeevoLab data.'''

	# Class constants
	IO_CFG 				= cfg.io
	DEFAULT_LOAD_CFG 	= cfg.default_load_cfg
	PLOT_HANDLER_CLS 	= DVLPlotHandler

	def __init__(self, *args, **kwargs):
		'''Initialises a DVLDataManager.'''

		super().__init__(*args, **kwargs)

		# Initialise additional class attributes
		# ...

	# Properties ..............................................................

	# Loading data ............................................................
	# Extend the load function with KnoFu-specific postprocessing.
	def load_data(self, update_load_cfg: dict=None, postprocess: bool=True, **kwargs) -> None:
		''' Load the data using the class-level defaults. By passing the `update_load_cfg` argument, the defaults can be updated.

		If the `postprocess` flag is set, the loaded data will undergo some postprocessing like adjusting the loaded data's classes to resemble deeevoLab-related classes.'''

		super().load_data(update_load_cfg=update_load_cfg, **kwargs)

		if not postprocess:
			# Finish here
			return

		# Else: Do some postprocessing
		log.info("Postprocessing loaded data...")

		if 'universes' in self:
			log.progress("Identifying deeevoLab universes ...")

			# Check if a ParamSpace was also loaded
			_dvlu_kws = dict(pspace=self['pspace']) if 'pspace' in self else {}

			# Convert the entry to a DVLUniverses object, which can make use of the additional pspace passed ...
			self['universes'] = self['universes'].convert_to(dcs.DVLUniverses,
			                                                 **_dvlu_kws)

			# Print some information
			log.state("%s", self['universes'].pspace.get_info_str())
			log.state("State Map shape: %s", self['universes'].state_map.shape)
			log.debug("State Map:\n%s", self['universes'].state_map.state_map)


			# Convert the universes inside that group to DVLUniverse objects
			for uni_no, uni in self['universes'].items():
				self['universes'][uni_no] 	= uni.convert_to(dcs.DVLUniverse)

		log.note("Finished postprocessing.")
		return

	# Data loaders specific to KnoFu . . . . . . . . . . . . . . . . . . . . .

	def _load_hdf5(self, file: str, *, name: str, load_on_demand: bool=False, lower_case_keys: bool=True) -> dcs.DVLGroup:
		'''Loads the specified hdf5 file into KnoFu-specific DataGroup and DataContainer-derived objects; this completely loads all data into memory and recreates the hierarchic structure of the hdf5 file.

		This overloads the DataManager method with the same name.

		All attributes are carried over and are accessible under the `attrs` attribute.

		Args:
			file: 			hdf5 file to load
			name: 			the group this is loaded into
			load_on_demand:	if True, the leaf datasets are only loaded when their .data attribute is accessed the first time. To do so, a reference to the hdf5 file is saved in a H5DataProxy
			lower_case_keys: whether to cast all keys to lower case
		'''
		return super()._load_hdf5(file, name=name,
		                          load_on_demand=load_on_demand,
		                          lower_case_keys=lower_case_keys,
		                          GroupCls=dcs.DVLGroup,
		                          DsetCls=dcs.DVLContainer)


	def _load_pspace(self, file: str, *, name: str):
		'''Loads the pickled ParamSpace object.'''

		log.debug("Loading pspace from pickled file:  %s ...", file)

		pspace 	= pkl.load(open(file, 'rb'))

		# TODO can do stuff here

		return pspace
