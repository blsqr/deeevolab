''' PlotWrapper-derived classes that help automating some plotting. The classes here are deeeval-specific.'''

import os
import copy
import itertools
from functools import reduce, partial
from pkg_resources import resource_filename
from typing import Union, Tuple, Sequence

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from paramspace import ParamSpace, ParamDim, CoupledParamDim

import deval
from deval.plot_wrapr import get_cnorm

import deeeval
from deeeval.config import Sweep
import deeeval.dvl_conts as dcs

# Setup logging for this file
log	= deeeval.get_logger(__name__)
cfg = deeeval.get_config(__name__)
log.debug("Loaded module, config, and logger.")

# Try to import seaborn
try:
	import seaborn as sns

except ImportError:
	log.warning("Seaborn could not be imported; some plotting functionality will not be available.")
else:
	log.debugv("seaborn imported successfully")

# Local constants


# -----------------------------------------------------------------------------
# The classes from deval that are also desired here are defined here by inheriting from deval and changing the configuration to match deeeval.

class PlotWrapper(deval.wrappers.PlotWrapper):
	'''The base PlotWrapper class.
	TODO write docstring
	'''
	# Set the module to look for plot funcs
	PLOT_FUNC_MODSTR = cfg.plot_func_modstr

	# Update the base plot style (using the one defined in deval as default)
	UPDATE_BASE_STYLE = cfg.base_style
	DEFAULT_RC_FILE = resource_filename("deeeval", "default_rc.yml")

	def postprocess_single_plot(self):
		'''Extends the parent method by adding another set of helpers to perform beside the ones configured in the `helpers` entry.'''
		super().postprocess_single_plot()

		# Call the additional helpers
		self.call_helpers(*self.this_style['additional_helpers'])

# For all other wrappers that are to be provided here, multiple inheritance can be used. This uses the class constants defined in this scope and all the rest (as second parent) from the wrappers defined in deval

class SinglePlot(PlotWrapper, deval.wrappers.SinglePlot):
	pass

# -----------------------------------------------------------------------------
# Wrapper extensions

class SelectorSweep(SinglePlot):

	# Update the base plot style (using the one defined in deval as default)
	UPDATE_BASE_STYLE = cfg.SelectorSweep.base_style

	# A module to look for the preprocessing methods
	PP_FUNC_MODSTR = cfg.SelectorSweep.pp_func_modstr

	def __init__(self, *args, plot_func_dims: int, handle_pf_dims: bool=True, max_subplot_dims: int=2, num_pp_dims: int=0, pp_funcs: Sequence[dict]=None, fname_cfg: dict=None, **kwargs):
		'''The SelectorSweep class allows sweeping over the selector of the multiverse data by identifying where a Sweep object was defined in the data selector and associating them with different representation dimensions.
		There are four iterated stages, which are filled from low sweep order to high with the number of specified dimensions:

		plot function < preprocessing < subplots x < subplots y < loop

		Args:
			plot_func_dims:
				Number of dimensions handled by the plot function

			handle_pf_dims: {bool, int, default: True}
				Whether the wrapper should handle the plot func dimensions or the plotting function should take care of that itself. If an integer, that many higher order sweeps of the plot function iteration are handled, while passing the others on to the plotting function

			max_subplot_dims:
				Number of dimensions the subplot grid has (0,1,2)

			pp_funcs:
				List of parameter dictionaries for the definition of preprocessing methods

			fname_cfg:
				A dict of how the loop iterator creates filenames
		'''
		super().__init__(*args, **kwargs)

		# Filename configuration
		self.fname_cfg = fname_cfg
		if not self.fname_cfg:
			log.debug("Using default filename config...")
			self.fname_cfg = cfg.SelectorSweep.fname_cfg

		# Look for Sweep objects in the selector and create a ParamSpace from them; that is then stored in the selector attribute
		self.sweep_info = [] # a list with info about the sweeps
		self._identify_sweeps()

		# Loop and subplot iterators for the selector
		# These iterators determine the selector; the data retrieval is done elsewhere.
		# Loop iterator related
		self.loop_iterator 		= None # is determined once in the beginning
		self.num_loop_dims 		= None
		self.loop_iter_shape	= None
		self.loop_iter_state	= None
		# NOTE uses the _loop_rv attribute to interface with PlotHandler

		# Subplot iterator related
		self.sp_iterator 		= None # is determined in each loop iteration
		self.max_subplot_dims	= max_subplot_dims
		self.num_sp_dims 		= None
		self.sp_shape 			= None
		self.sp_iter_state 		= None

		# Preprocessing function and dimensions
		self.pp_funcs 			= None
		self.num_pp_dims		= num_pp_dims
		if num_pp_dims > 0:
			raise NotImplementedError("num_pp_dims > 0")
		self._init_pp_funcs(*(pp_funcs if pp_funcs else []))

		# Plot func iterator related
		self.pf_iterator		= None
		self.num_pf_dims 		= plot_func_dims
		self.handle_pf_dims 	= (plot_func_dims if handle_pf_dims is True
		                           else int(handle_pf_dims))
		self.pf_iter_shape 		= None
		self.pf_iter_state 		= None

		# Done.

	# .........................................................................
	# Additional properties

	# Access to selector
	# On two levels:
	# 	- selector: the global func_kwargs (base truth)
	# 	- this_selector: the func_kwargs for one loop iteration (use this!)

	@property
	def selector(self):
		return self.func_kwargs['selector']

	@selector.setter
	def selector(self, val):
		log.debug("Setting selector in func_kwargs to:\n%s", deeeval.PP(val))
		self.func_kwargs['selector'] = val

	@property
	def this_selector(self):
		return self.this_func_kwargs['selector']

	@this_selector.setter
	def this_selector(self, val):
		log.debug("Setting selector in func_kwargs (for single loop iteration) to:\n%s", deeeval.PP(val))
		self.this_func_kwargs['selector'] = val

	# .........................................................................
	# Sweep identification

	def _identify_sweeps(self):
		'''Searches in the entries of the selector for Sweep objects and from that information creates a ParamSpace for the .sweeps attribute of this PlotWrapper.'''

		# Counters
		n, m, k 	= 0, 0, 0

		# Distinguish between the `select` and the `data_keyseq` entry
		# First the `select` key, which selects a multiverse subspace
		# Iterate over the list of dicts
		for sd in self.selector['select']:
			key 	= sd['key']

			# Look at the `idx` entry
			if isinstance(sd.get('idx'), Sweep):
				log.debug("Found Sweep object in `select` -> %s.", key)
				# Resolve the sweep object to a ParamDim
				ps, cps	= self._identify_multiverse_dim(sd.get('idx'), key)

				# Save it into the selector and increment the counter
				sd['idx'] = ps 	# holds the index to access
				sd['val'] = cps 	# holds the value of that index
				n += 1

			else:
				log.debug("No Sweep object found in `select` -> %s.", key)
		else:
			log.progress("Found %d Sweep objects in `select` and converted to ParamDim objects.", n)


		# Now (almost) the same for the `data_keyseq` . . . . . . . . . . . . .
		# Get the key sequence and create a list of the same length as information on the keysequence (if not already present)
		dks = self.selector['data_keyseq']

		if 'data_keyseq_info' not in self.selector:
			self.selector['data_keyseq_info'] = [None for _ in dks]
		dks_info = self.selector['data_keyseq_info']

		# Go over the data_keyseq and identify corresponding data dimensions
		for i, key in enumerate(dks):
			# See if the object is a Sweep
			if isinstance(key, Sweep):
				log.debug("Found Sweep object in element %d of `data_keyseq`.", i)
				# Resolve the sweep object to a ParamDim
				ps, cps	= self._identify_data_dim(key, dks[:i+1], idx=i)

				# Save back to the selector and increment the counter
				dks[i] = ps 	# holds the index to access
				dks_info[i] = cps 	# holds information

				m += 1

			else:
				log.debug("No Sweep object found in element %d of `data_keyseq`.", i)
		else:
			log.progress("Found %d Sweep objects in `data_keyseq` and converted to ParamDim objects.", m)

		# There can also be dummy sweeps . . . . . . . . . . . . . . . . . . .
		# They can be used to change the order in that sweeps are iterated through

		dummies = self.selector.get('dummy_sweeps', [])
		if 'dummy_sweeps_info' not in self.selector:
			self.selector['dummy_sweeps_info'] = [None for _ in dummies]
		dummy_info = self.selector['dummy_sweeps_info']

		# Loop over the list, expecting a sweep object at every element
		for j, dummy in enumerate(dummies):
			if isinstance(dummy, Sweep):
				log.debug("Found Sweep object in element %d of "
				          "`dummy_sweeps`.", j)

				# Convert to a dummy ParamDim and CoupledParamDim
				ps, cps = self._identify_dummy_dim(dummy)

				# Save back
				dummies[j] = ps
				dummy_info[j] = cps

				k += 1
			else:
				raise ValueError("Expected entry {} of `dummy_sweeps` to be "
				                 "Sweep object, was: {}, {}"
				                 "".format(j, dummy, type(dummy)))
		else:
			if k > 0:
				log.progress("Found %d dummy Sweep objects.", k)


		# Converted all Sweeps in the dictionary to ParamDim objects now.
		if n + m + k == 0:
			# No sweeps resolved; do not create a ParamSpace
			log.caution("No !Sweep objects were found in the given selector.")
			return

		# Create a ParamSpace from the whole selector and set that as the value of the selector
		log.debug("Creating ParamSpace from selector ...")
		self.selector = ParamSpace(self.selector)
		# NOTE this will automatically collect the ParamDim objects and resolve their order (which was defined in the Sweep objects). The spans can then be accessed via

		# Create sweep information list by going over the sorted list of CoupledParamDims which were created above and extracting name, idx, and value information
		for cps in self.selector.get_coupled_spans():
			# Have a CoupledParamDim object now
			self.sweep_info.append(dict(key=cps.name, values=cps.span))

			# TODO add more info by expanding Sweep object and the creation of the CoupledParamDim?

		else:
			log.progress("Created sweep information from %d Sweep objects.",
			             len(self.sweep_info))
			log.state("Sweep Info:\n  %s",
			          "\n  ".join(["{:<18s} {}".format(si['key'], si['values'])        for si in self.sweep_info]))

	def _identify_multiverse_dim(self, swp_obj: Sweep, dim_name: str) -> tuple:
		'''Identifies the multiverse dimension belonging to the given sweep object and then gathers the required ParamDim objects. Returns a tuple (idcs, values) with the indices and the values.'''

		# Copy the ParamDim object from the handler's sweepspace (ParamSpace object) to use as a base for the selector
		sweepspace = self.handler.sweepspace
		ps = copy.deepcopy(sweepspace.get_span_by_name(dim_name))

		# Determine the values of the ParamDim and CoupledParamDim
		# See if the sweep object provided values or not
		if swp_obj.values is not None:
			log.debugv("Sweep object provided values: %s", swp_obj.values)
			ps_vals	= list(swp_obj.values)
		else:
			# Use the span values
			log.debugv("Generating indices corresponding to whole ParamDim: %s", ps.span)
			ps_vals	= list(range(len(ps)))

		# See if the sweep object provided info or not
		if swp_obj.info is not None:
			log.debugv("Using provided Sweep object info: %s", swp_obj.info)
			cps_vals = swp_obj.info
		else:
			# Use the span info
			log.debugv("Using ParamDim values for info: %s", ps.span)
			cps_vals = [ps.span[idx] for idx in ps_vals]

		# Adjust the ParamDim attributes. For the selector, the main span needs to have indices and not the actual values of the parameter span; those are stored in the CoupledParamDim below
		ps.span = ps_vals
		ps.name = dim_name if not swp_obj.name else swp_obj.name
		ps.default = 0

		# Set the order of the ParamDim object
		ps.order = swp_obj.order

		# Now need a CoupledParamDim with the actual values
		cps = CoupledParamDim(dict(name=ps.name,
		                           span=cps_vals,
		                           default=cps_vals[ps.default],
		                           coupled_to=ps.name,
		                           order=ps.order))
		# NOTE its not an issue that both the CoupledParamDim and the regular ParamDim have the same name; in ParamSpace, the only ones accessed by name are the ParamDims, not the CoupledParamDims!

		log.debugv("Converted Sweep (order: %d) to ParamDim of multiverse "
		           "dimension '%s'.", swp_obj.order, dim_name)

		# And return it
		return (ps, cps)

	def _identify_data_dim(self, swp_obj: Sweep, keyseq: Sequence[str], idx: int) -> tuple:
		'''Identifies possible values in the data selector. If the sweep object already supplies these values, those are used. The information given in that'''

		# Determine names for both
		if swp_obj.name:
			ps_name = cps_name = swp_obj.name
		else:
			ps_name = "keyseq_idx_"+str(idx)
			cps_name = ps_name + "_info"

		# Generate ParamDim
		if swp_obj.values is not None:
			ps = ParamDim(dict(name=ps_name,
			                   span=swp_obj.values,
			                   default=swp_obj.values[0],
			                   order=swp_obj.order))

		else:
			raise NotImplementedError("cannot automatically determine valid data dimension yet.")


		# Generate CoupledParamDim
		if swp_obj.info is not None:
			cps_span = swp_obj.info
			log.debugv("Using provided Sweep object info: %s", cps_span)
		else:
			cps_span = swp_obj.values
			log.debugv("Using Sweep object values for info: %s", cps_span)

		cps	= CoupledParamDim(dict(name=cps_name,
		                           span=cps_span,
		                           default=cps_span[0],
		                           order=swp_obj.order,
		                           coupled_to=ps.name))

		return ps, cps

	def _identify_dummy_dim(self, swp_obj) -> tuple:
		'''These are meant to do nothing and can be used to change the ordering of iterations ...'''
		# Determine names for both
		ps_name = "order_{}_dummy".format(swp_obj.order)
		cps_name= ps_name + "_info"

		# Generate ParamDim
		ps = ParamDim(dict(name=ps_name,
		                   span=[ps_name],
		                   default=ps_name,
		                   order=swp_obj.order,
		                   enabled=False))

		# Generate CoupledParamDim
		cps	= CoupledParamDim(dict(name=cps_name,
		                           span=[cps_name],
		                           default=cps_name,
		                           order=swp_obj.order,
		                           coupled_to=ps.name,
		                           enabled=False))

		return ps, cps

	# .........................................................................
	# Sweep information

	def get_sweep_info(self, *, iter_name: str, dim: int=None) -> dict: # TODO simplify
		'''Returns information on the *current state* of the sweep.'''

		log.debug("Gathering sweep information for '%s' ...", iter_name)

		if iter_name in ['subplot', 'subplot_iter']:
			# Info about the current subplot state
			pf_dims = self.num_pf_dims # dimensions handled by plot function
			sp_info	= self.sweep_info[pf_dims: pf_dims + self.num_sp_dims]

			# Generate a dict with 'x' and 'y' entries.
			sp_dict = dict()
			for info, ax, idx in zip(sp_info, ['x', 'y'], self.sp_iter_state):
				# Add the basic information
				sp_dict[ax]			= copy.deepcopy(info)

				# Target dict
				td 					= sp_dict[ax]

				# Add information on the current value to the target dict
				td['idx'] 			= idx
				td['value']			= info['values'][idx]
				td['num_values']	= len(info['values'])

				if isinstance(info['key'], str):
					td['keystr']	= info['key']
				else:
					td['keystr']	= "-".join(info['key'])

			if not dim:
				log.debugv("Returning full subplot info dict:\n  %s", sp_dict)
				return sp_dict
			else:
				log.debugv("Returning subplot info for axis '%s':\n  %s", dim, sp_dict[dim])
				return sp_dict[dim]

		# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
		elif iter_name in ['loop_iter', 'pf_iter', 'pf_handled']:
			if iter_name in ['loop_iter']:
				# Info about the current loop iteration sate
				_skip	= self.num_pf_dims + self.num_sp_dims
				infos	= self.sweep_info[_skip: _skip+self.num_loop_dims]
				state 	= self.loop_iter_state
			elif iter_name in ['pf_iter']:
				# Info about the plot function iteration (not handled, therefore without state!)
				infos	= self.sweep_info[:self.num_pf_dims]
				state 	= None
			else:
				# Info about the current handled plot function iteration
				if self.handle_pf_dims > 0:
					i1, i2 	= self.num_pf_dims, self.handle_pf_dims
					infos	= self.sweep_info[:i1][-i2:]
					state 	= self.pf_iter_state
				else:
					infos 	= []

			# Generate a list of dictionaries where each list entry belongs to one dimension of the iterator. Add some more information ...
			info_list = []

			for idx, info in enumerate(infos):
				_info = copy.deepcopy(info)

				# Add information on the current value
				if state is not None:
					# Can only do this, if a state is available
					_state 				= state[idx]
					_info['idx'] 		= _state
					_info['value'] 		= _info['values'][_state]
				_info['num_values']	= len(_info['values'])

				if isinstance(_info['key'], str):
					_info['keystr']		= _info['key']
				else:
					_info['keystr']		= "-".join(_info['key'])

				# And append to list
				info_list.append(_info)

			if dim is None:
				# Return the whole list
				log.debugv("Returning full %s info list:\n%s", iter_name, info_list)
				return info_list
			else:
				# Just one entry
				log.debugv("Returning dimension %d of %s info list:\n%s", dim, iter_name, info_list[dim])
				return info_list[dim]

		else:
			raise NotImplementedError("get_sweep_info iter_name: "+str(iter_name))

	def format_sweep_info(self, *, iter_name: str, dim: int, fstr: str='{keystr:}'):
		'''Extracts infromation from the sweep info dictionary.'''
		info = self.get_sweep_info(iter_name=iter_name, dim=dim)

		if fstr:
			try:
				return fstr.format(**info)
			except KeyError as err:
				msg 	= "Failed to resolve format string '{}' for sweep info (iter_name '{}', dim {}): {}. KeyError: {}. This might be due to the wrong sweep name.".format(fstr, iter_name, dim, info, err)
				if deeeval.DEBUG:
					raise ValueError(msg) from err
				else:
					log.warning( + "\nUsing `keystr` instead.")

		return info['keystr']
	# .........................................................................
	# Plotting

	def prepare_plotting(self):
		'''Prepares the plotting.'''

		super().prepare_plotting()

		# Dimension calculation . . . . . . . . . . . . . . . . . . . . . . . .
		# Calculate the dimensions and shapes for the subplot iteration and the loop iteration from the selector

		# Catch cases where there is no ParamSpace defined for the selector
		if not isinstance(self.selector, ParamSpace):
			log.caution("Selector is not a ParamSpace object; did you define any sweeps?")

			self.sp_shape = super()._determine_subplot_shape()

		log.note("Associating subplots and loops with sweep dimensions ...")

		# Can assume the selector to be a ParamSpace object now
		# Create a shortcut
		sel = self.selector

		# Calculate the number of dimensions not represented by a single plot
		log.progress("Dimension calculations:")
		log.state("  # multiverse dimensions:        %d",
		          (self.handler.dm['universes'].pspace.num_dimensions))
		num_swp_dims 	= sel.num_dimensions
		log.state("  # sweep dimensions:             %d", num_swp_dims)
		log.state("  # plot func dimensions:         %d", self.num_pf_dims)
		log.state("  #              handled:         %d", self.handle_pf_dims)
		log.state("  # preprocessing dimensions:     %d", self.num_pp_dims)

		# From that, calculate the number of subplot dimensions and the number of dimensions the loop needs to take care of
		pf_pp_dims 		= self.num_pf_dims + self.num_pp_dims
		num_sp_dims 	= min(num_swp_dims - pf_pp_dims, self.max_subplot_dims)
		log.state("  # subplot dimensions:           %d", num_sp_dims)

		num_loop_dims 	= num_swp_dims - pf_pp_dims - num_sp_dims
		log.state("  # loop dimensions:              %d", num_loop_dims)

		# Store as attribute
		self.num_sp_dims	= num_sp_dims
		self.num_loop_dims	= num_loop_dims

		# From the number of dimensions, determine the shapes
		log.progress("Determining iterator shapes:")
		log.state("  total sweep shape:              %s", sel.shape)
		log.state("  plot func iterator shape:       %s -> %s handled",
		          sel.shape[:self.num_pf_dims],
		          sel.shape[:self.num_pf_dims][-self.handle_pf_dims:] if self.handle_pf_dims else "none")
		log.state("  preprocessing iterator shape:   %s",
		          sel.shape[self.num_pf_dims:][:self.num_pp_dims])
		# NOTE this is just for information; the iterators are created elsewhere and that is where the shape is determined.

		# Subplot shape
		sp_shape 		= sel.shape[pf_pp_dims:][:num_sp_dims]

		# Correct this if necessary to always have a 2-tuple
		if len(sp_shape) == 0:
			self.sp_shape 	= (1, 1)
		elif len(sp_shape) == 1:
			self.sp_shape 	= sp_shape + (1,)
		else:
			self.sp_shape 	= sp_shape

		log.state("  subplot shape:                  %s -> %s",
		          sp_shape, self.sp_shape)

		# And also for the loop iteration, if there is one
		if self.num_loop_dims > 0:
			self.loop_iter_shape 	= sel.shape[-num_loop_dims:]
			log.state("  loop iteration shape:           %s",
			          self.loop_iter_shape)

		# Dimension mismatch errors between plotting function and plot function dimensions
		allowed_dims = getattr(self.plot_func, 'allowed_num_sweep_dims', None)
		if allowed_dims is not None:
			if (self.num_pf_dims - self.handle_pf_dims) not in allowed_dims:
				msg = "Dimension mismatch error: the plot function '{}' specified that it would allow {} sweep dimensions, but the free dimensions available to the plot function (i.e. not handled by the wrapper) are:  {}.  Either add or remove low order Sweep objects to the selector or adjust the `plot_func_dims`, `handle_pf_dims` and the other dimension-related arguments to fix this.".format(self.plot_func_name, allowed_dims, self.num_pf_dims - self.handle_pf_dims)
				raise ValueError(msg)

		# If there will be subplots, adjust some regular helpers as they might interfere with the subplot helpers
		if self.sp_shape > (1, 1):
			# To remove them only if they are present, check against the names in the subplot_helpers list.
			for hlpr_name in self.style.get('subplot_helpers', []):
				if hlpr_name in self.style['helpers'] and hlpr_name in self.style['subplot_helpers']:
					# There is a subplot helper doing the corresponding job; remove the other one.
					self.style['helpers'].remove(hlpr_name)

		# Create loop iterator, if needed
		if self.num_loop_dims > 0:
			log.progress("Creating loop iterator ...")
			# Basically, the loop iterator needs to iterate over the subspace of the ParamSpace belonging to the last num_loop_dims ParamDims while leaving everything else open.
			# The subplot iterator needs to supply at every subplot a subspace of size <= max_subplot_dims. This is done in the prepare_single_plot file.
			# Due to limited iterator functionality in paramspace package (v0.9.9), the iterator is created by, at each point of the pspace

			self.loop_iterator 	= pspace_iter(self.selector,
			                                  num_dims=num_loop_dims,
			                                  mode='last',
			                                  iter_name='loop')
			# NOTE that even with num_dims==0 a generator object is returned; therefore, the loop iterator will not evaluate to None and cannot be used as a test of whether there will be subplot iterations or not

			# Create the initial return value dictionary
			n_max 				= reduce(lambda x,y: x*y, self.loop_iter_shape)
			# NOTE This needs to be calculated manually here as there is no other access to the content of the iterator, where this information would also be available.

			self._loop_rv 		= dict(n_max=n_max,
			                           n_digits=len(str(n_max)),
			                           fname_fstrs={})
			log.debug("Return value dictionary initialised.")

		else:
			log.debug("Loop iterator is not needed.")

		# If preprocessing is needed, do it now. This will calculate the preprocessing results on all universes selected in the plot selector's select list.
		if self.pp_funcs:
			self._perform_pp(selector=self.selector)

		return

	def prepare_single_plot(self, *, old_rv: dict=None):
		'''Extends the parent method by adding the creation of the subplot iterator.'''
		super().prepare_single_plot(old_rv=old_rv)

		# Can do something with the old return value dictionary here

		# Initialise some variables
		self['pxmap_info'] 	= dict(num_registered=0, patch_collections={},
		                           data_vlims={}, data={}, by_ax={})

		# Distinguish according to whether loop iterations are performed
		if not self.loop_iterator:
			# No loop iterator -> the subplot iterator consists of the whole selector, i.e. there is no subspace
			subspace 				= self.selector

		else:
			# Create the subplot iterator for this specific loop iteration.
			state, subspace			= self.loop_iterator.__next__()
			# This might raise StopIteration, meaning that the loop iterator is finished and no further subplots are needed.

			self.loop_iter_state	= state
			self._loop_iter_selector= subspace

			# Adjust the return value dictionary to e.g. hold filename information
			self._update_rv()

		# Create a (new) subplot iterator using the subspace. This will yield the selector subspace for each subplot.
		self.sp_iterator 		= pspace_iter(subspace,
		                                      num_dims=self.num_sp_dims,
		                                      iter_name='subplot',
		                                      num_state_dims=2)

		return

	def perform_plot(self):
		'''This not only calls the plot function, but wraps around it the handling of the return value such that looped plots can be generated and the subplot handling.'''

		log.info("Performing plot ...")

		# Go over the subplot iterator
		for sp_state, selector in self.sp_iterator:
			# Save the subplot state
			self.sp_iter_state 	= sp_state

			# Some preparations
			self._prepare_subplot()

			# Select the correct plot axis
			log.debug("Selecting subplot axarr object at %s ...", sp_state)
			self.ax 	= self.axarr[sp_state]

			# Perform the plot function iteration, which handles the calls to the plot function
			self._perform_pf_iteration(selector=selector)

			# Call the subplot helpers
			self.call_helpers(*self.this_style['subplot_helpers'])

			log.note("Subplot %s finished.", sp_state)

		else:
			log.note("Subplot iteration finished.")
			self.sp_iter_state 	= None # NOTE that this makes some helpers not work; this is intended, as helpers that should operate on subplots should not be called after all subplot iterations are finished, but as part of the subplot helpers

		# Done with plotting.
		log.info("Finished plot.")

	# .........................................................................
	# Preprocessing

	def _init_pp_funcs(self, *pp_funcs):
		'''Initialises the list of preprocessing methods.'''

		def init_pp_func(*, func_name: str=None, modstr: str=None, target_dset: Union[str, Sequence[str]]=None, recalculate: bool=False, **func_kwargs):
			'''Initialises the preprocessing method.'''
			if not modstr:
				modstr 	= self.PP_FUNC_MODSTR

			# Import the module
			log.debug("Loading module '%s' ...", modstr)
			try:
				mod  	= __import__(modstr, fromlist=[func_name])
			except ImportError:
				log.error("Failed to import preprocessing method with name '%s' from module '%s'.", func_name, modstr)
				raise
			else:
				# Get the plot function
				pp_func = getattr(mod, func_name)
				log.progress("Got preprocessing function '%s'.", func_name)
				docstr 	= getattr(pp_func, '__doc__', '(no docstring)')

			# Partially apply arguments and the kwargs
			pp_func 		= partial(pp_func, self, **func_kwargs)
			pp_func.__name__= func_name
			pp_func.__doc__ = docstr

			# Set the target dset defaults
			if not target_dset:
				target_dset 	= ['pp_results', func_name]

			# Save to dict that is then returned
			return dict(name=func_name, func=pp_func, target_dset=target_dset, modstr=modstr, recalculate=recalculate)

		# Go over the supplied list and add the process the parameters for each preprocessing function; save to attribute
		self.pp_funcs 	= [init_pp_func(**p) for p in pp_funcs]

		# Perform a check
		if not self.pp_funcs and self.num_pp_dims > 0:
			if deeeval.DEBUG:
				raise ValueError("Requested {} preprocessing dimensions but did not supply a preprocessing `func_name`.".format(num_pp_dims))
			else:
				log.error("No preprocessing methods defined, but `num_pp_dims` == %d > 0 was set. Setting `num_pp_dims` to zero.", self.num_pp_dims)
				self.num_pp_dims 	= 0

		return

	def _perform_pp(self, *, selector: dict, selector_state: int=None, selector_volume: int=None):
		'''Performs the preprocessing function on the given selector. It ignores sweeps that occur outside of the `select` list, as the preprocessing methods handle whole universe data and would otherwise perform multiple operations on one universe.'''

		# The format string to inform about the current iteration
		info_fstr 	= " {n:>5d} : {uni:>6s} -- {ppf:<30s}"
		pp_fstr 	= "  - {}:{}{}"

		# Have to do something differently if it is a ParamSpace ...
		if isinstance(selector, ParamSpace):
			# Have to call it on each point, i.e. iterate through the selector. Then call this method again to call and apply the preprocessing function ...
			log.progress("Going over selected universes and performing %d preprocessing function(s) ...",
			             len(self.pp_funcs))
			log.state("\n%s",
			          "\n\n".join([pp_fstr.format(pp['func'].__name__,
			                                      "\n"+" "*6,
			                                      pp['func'].__doc__)
			                       for pp in self.pp_funcs]))

			n 				= 0 # how many universes were operated upon
			select_lists 	= []
			sel_vol 		= selector.volume

			for sel_state, _selector in selector.get_points():
				# Check if preprocessing was already done with this `select` list. If yes, skip this iteration to avoid multiple preprocessing calls on a single universe.
				if _selector['select'] in select_lists:
					continue
				else:
					select_lists.append(_selector['select'])

				# Call the function again
				n += self._perform_pp(selector=_selector,
				                      selector_state=sel_state,
				                      selector_volume=sel_vol)
			else:
				log.progress("Performed preprocessing on %d universes.", n+1)

		else:
			# Loop over the universes in the select list and perform the preprocessing on each one of them separately
			if selector_state is None:
				log.progress("Going over selected universes and performing %d preprocessing function(s) ...",
				             len(self.pp_funcs))
				log.state("\n%s",
				          "\n\n".join([pp_fstr.format(pp['func'].__name__,
			                                          "\n"+" "*6,
				                                      pp['func'].__doc__)
				                       for pp in self.pp_funcs]))

			else:
				# Write the state information to the format string
				state_fstr= "  {state:>{vol_digits:}d} / {vol:d} -- {info:}"
				info_fstr = state_fstr.format(state=selector_state+1,
				                              vol=selector_volume,
				                              vol_digits=len(str(selector_volume)),
				                              info=info_fstr)

			select = selector['select']
			it 	   = self.handler.dm['universes'].iter_universes(select=select)

			for n, uni in enumerate(it):
				# Call the preprocessing function, which returns a result for a single universe. Then save the result to the pp_results group of the universe

				# Loop over all preprocessing methods
				for pp in self.pp_funcs:
					# Inform
					print(info_fstr.format(n=n, uni=str(uni.name),
					                       ppf=pp['name']),
					      end="\r")

					# If its not to be recalculated, check if this was already calculated. If yes, need not calculate it again.
					if (not pp['recalculate']
					    and uni.hasitem(pp['target_dset'])):
						# The entry is already present
						log.debugv("Entry already present; continuing.")
						continue

					log.debugv("Calling preprocessing function '%s' ...",
					           pp['name'])

					# Get the pp_func
					pp_func = pp['func']

					# Call the postprocessing function
					try:
						res 	= pp_func(uni=uni)
					except Exception as err:
						log.error("Failed applying postprocessing function '%s'!\n\n%s: %s\n", pp_func.__name__, err.__class__.__name__, err)
						if deeeval.DEBUG:
							raise
					else:
						# And save the return value
						self._save_pp_result(res, uni=uni,
						                     target_dset=pp['target_dset'],
						                     recalculate=pp['recalculate'])

			else:
				msg = "Performed preprocessing on %d universes."
				if selector_state is None:
					log.progress(msg, n+1)
				else:
					log.debug(msg, n+1)

			return n

	def _save_pp_result(self, res, *, uni, target_dset: Sequence[str], recalculate: bool):
		'''Saves the given result of a preprocessing function to the handlers's data manager's `pp_results` group.'''

		# Start at the universe and traverse down, creating the keys on the groups on the way, if neecessary
		dset 		= uni
		walk_dsets 	= target_dset[:-1] # names of the dsets on the way
		leaf_dset 	= target_dset[-1]  # name of the dset on the leaf

		for key in walk_dsets:
			if key not in dset:
				# Create a group for the preprocessing results
				dset[key] 	= dcs.DVLPPResults(name=key)
			# Set the traversal variable to the new leaf
			dset 	= dset[key]
		# Now have the target dataset's parent

		# Check if the leaf dset name is already present; if yes, warn.
		if not recalculate and leaf_dset in dset:
			# Overwriting something ... warn.
			log.caution("There already is an entry '%s' in %s '%s' for universe %s, possibly due to earlier preprocessing on the same DataManager. Overwriting it with the new preprocessing result ...", leaf_dset, dset.classname, dset.name, uni.name)

		# Save the result
		dset[leaf_dset] = res

		log.debugv("Preprocessing result saved to universe %s.", uni.name)
		return

	# .........................................................................
	# Plot function iteration

	def _perform_pf_iteration(self, *, selector):
		'''Handler at the lowest sweep order level: performing plot function iteration, if necesary.'''
		if not self.handle_pf_dims or self.num_pf_dims < 1:
			# Adjust the func kwargs of this loop iteration so that they are passed to the plot function
			self.this_selector 	= selector

			# Call the plotting function
			self._call_plot_func()
			# The return value is ignored, as this is completely handled by this wrapper

		else:
			# Given a selector with nonzero free dimension, now need to create an iterator over that selector that returns at each point a flat selector.
			log.progress("Creating plot function iterator over %d handled dimension(s) ...", self.handle_pf_dims)

			self.pf_iterator 	= pspace_iter(selector,
			                                  num_dims=self.handle_pf_dims,
			                                  mode='last',
			                                  iter_name='plot func')
			self.pf_iter_shape 	= selector.shape[-self.handle_pf_dims:]
			# NOTE Careful! This is the _handled_ plot function iterator's shape, not the total

			# Start a loop over that iterator and use the returned selector as argument for the plotting function
			for pf_state, pf_selector in self.pf_iterator:
				self.pf_iter_state 	= pf_state
				self.this_selector 	= pf_selector

				self._call_plot_func()

			else:
				# Done with the plot func iteration
				self.pf_iter_state 	= None

	# .........................................................................
	# Subplots and looping

	def _determine_subplot_shape(self) -> tuple:
		'''Determines the subplot shape.

		This is done by looking at the number of sweeps defined for the selector and the number of dimensions that can be represented within a single subplot (i.e. `dims_per_subplot`).'''

		# Return the previously calculated result (see prepare_plotting)
		log.debug("Subplot shape: %s", self.sp_shape)
		return self.sp_shape

	def _update_rv(self):
		'''Updates the return value dictionary with e.g. filename information.'''
		# Generate a format string to represent the loop iteration state
		loop_info 	= self.get_sweep_info(iter_name='loop_iter')

		log.debug("Got current loop information:\n  %s", deeeval.PP(loop_info))

		# Get the format string to generate the state string
		kv_fstr 	= self.fname_cfg['kv_fstr']
		join_str	= self.fname_cfg['kv_join_str']
		log.debugv("Using format string '%s' and join string '%s' to generate state string.", kv_fstr, join_str)

		# Generate the state string
		state_str 	= join_str.join([kv_fstr.format(**d) for d in loop_info])

		# Make lowercase and replace spaces by underscores
		state_str 	= state_str.replace(" ", "_").lower()
		log.debug("Generated state string:\n  %s", state_str)

		# Apply a format operation to the prefix and suffix
		fname_fstrs = self.fname_cfg['fname_fstrs']
		prefix 		= fname_fstrs['prefix'].format(state_str=state_str)
		suffix 		= fname_fstrs['suffix'].format(state_str=state_str)

		# And add them to the return value dictionary
		self._loop_rv['fname_fstrs']['prefix'] 	= prefix
		self._loop_rv['fname_fstrs']['suffix'] 	= suffix

	def _prepare_subplot(self):
		'''As these might change in each subplot, make a copy from the originals.'''
		self['this_func_kwargs'] 	= copy.deepcopy(self.func_kwargs)
		self['this_style']  		= copy.deepcopy(self.style)

	# .........................................................................
	# Other public methods

	def register_pxmap_info(self, *, pc, data_vlims, data):
		'''Registers a pxmap_patch_collection and its corresponding data.'''
		self['pxmap_info']['patch_collections'][self.ax]= pc
		self['pxmap_info']['data_vlims'][self.ax]		= data_vlims
		self['pxmap_info']['data'][self.ax]				= data
		self['pxmap_info']['by_ax'][self.ax]			= (data, pc)
		self['pxmap_info']['num_registered']			+= 1

		log.debug("Registered pxmap info ...")
		return

	# .........................................................................
	# Official helper methods
	# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	# For adding information
	# TODO use generalised helper for legend!

	def _hlpr_set_labels(self, *, xlabel: str=None, ylabel: str=None, **label_kwargs):
		'''Extends the set_labels method to be aware of the position within the subplot iteration. This still works only on a single axis; it assumes to be called as a helper after each subplot.'''

		# Depending on the position in the subplot, remove the label.
		if self.sp_shape > (1, 1) and self.sp_iter_state:
			# To add the labels only if in a valid subplot position (leftmost column, last row), get the current subplot position and shape
			x_idx, y_idx 	= self.sp_iter_state
			x_max, y_max 	= (self.sp_shape[0] - 1, self.sp_shape[1] - 1)

			if not (y_idx == y_max):
				# Not in the last row -> remove x-axis label
				xlabel 			= None

			if not (x_idx == 0):
				# Not in first column -> remove y-axis label
				ylabel 			= None

		elif self.sp_shape > (1, 1) and not self.sp_iter_state:
			log.caution("Called 'set_labels' helper outside the subplot iteration. As this plot has subplots, make sure to add the 'set_labels' helper to the list of subplot_helpers, not any other list.")
			# Cannot do anything else here.
			return

		# If they are still set, check if they are dictionaries with further parameters and might need to be resolved from the sweep info
		if isinstance(xlabel, dict):
			xlabel 	= self.format_sweep_info(**xlabel)
		if isinstance(ylabel, dict):
			ylabel 	= self.format_sweep_info(**ylabel)

		# Call the parent method
		super()._hlpr_set_labels(xlabel=xlabel, ylabel=ylabel, **label_kwargs)

	def _hlpr_set_label_coords(self, xlabel_coords: tuple=None, ylabel_coords: tuple=None, **kwargs):
		'''Extends the set_label_coords method to be aware of the position within the subplot iteration. The coordinates will not be adjusted if not in the first column or last row'''

		# Depending on the position in the subplot, remove the argument.
		if self.sp_shape > (1, 1):
			# To add the labels only if in a valid subplot position (leftmost column, last row), get the current subplot position and shape
			x_idx, y_idx 	= self.sp_iter_state
			x_max, y_max 	= (self.sp_shape[0] - 1, self.sp_shape[1] - 1)

			if not (y_idx == y_max):
				# Not in the last row -> remove x-axis label
				xlabel_coords	= None

			if not (x_idx == 0):
				# Not in first column -> remove y-axis label
				ylabel_coords	= None

		# Call the parent method
		super()._hlpr_set_label_coords(xlabel_coords=xlabel_coords,
		                               ylabel_coords=ylabel_coords, **kwargs)

	def _hlpr_set_legend(self, *args, on_axarr_pos: Sequence[Sequence[int]]=None, enforce_title: bool=False, **kwargs):
		'''Sets the current axis legend.

		If the `axarr_pos` argument is given, the legend is only set if the current subplot position matches that specific position.'''

		# FIXME enforce_title is confusing ... and not useful

		if on_axarr_pos:
			# Convert to a list of tuples and check if the subplot position is in that list
			if self.sp_iter_state not in [tuple(pos) for pos in on_axarr_pos]:
				log.debug("Current subplot position %s was not in `on_axarr_pos` list (%s).", self.sp_iter_state, on_axarr_pos)

				if enforce_title: # needed for adding of subplot info
					# Still add the legend, but only with the title
					log.debugv("Still adding legend, but only with title.")
					return super()._hlpr_set_legend(*args, title_only=True,
				                                    **kwargs)
				else:
					log.debugv("Not adding a legend at this position.")
					return
			else:
				log.debugv("Current subplot position %s was in `on_axarr_pos` list.", self.sp_iter_state)

		# Call the parent method unchanged
		return super()._hlpr_set_legend(*args, **kwargs)

	def _hlpr_add_subplot_info(self, *, x_label_fstr: str, y_label_fstr: str, legend_title_mode: str='before', capitalize: bool=False, replace_ops: Sequence[Sequence[str]]=None, keep_title_on_pos: Sequence[int]='all', only_on: Sequence[str]=None):
		'''Adds the subplot information by looking at the current selector and reading those entries that belong to the dimensions associated with the subplot axes'''

		def add_to_lgd_title(s: str, *, mode: str='before'):
			log.debugv("Adding text: %s (mode: %s)", s, mode)
			log.debugv("Current title: %s", self.this_style['set_legend'].get('title', ""))

			# Check whether to drop the title
			if (keep_title_on_pos
			    and (keep_title_on_pos == 'all'
			         or tuple(keep_title_on_pos) == self.sp_iter_state)
			    ):
				# keep the title, i.e. read the already existing one as basis
				title 	= self.this_style['set_legend'].get('title', "")
				log.debugv("Keeping current title.")
			else:
				# empty title
				title 	= ""
				log.debugv("Dropping title.")

			if not title:
				# Can just set it, dropping the previous title
				title 	= s
			else:
				# Distinguish modes
				if mode == 'before':
					title = s + "\n" + title
				elif mode == 'after':
					title = title + "\n" + s
				else:
					raise ValueError("Invalid mode "+str(mode))

			# Now set it to the style
			self.this_style['set_legend']['title'] 	= title

			# And enforce it being printed by set_legend
			self.this_style['set_legend']['enforce_title'] = True

			return

		def format_label(*, fstr: str, capitalize: bool, replace_ops: Sequence[Sequence[str]], **fstr_kwargs) -> str:
			if not fstr:
				# Nothing to format
				return None

			# Pass information to the label formatstring
			s 	= fstr.format(**fstr_kwargs)

			if capitalize:
				s 	= s.capitalize()

			if replace_ops:
				for repl_args in replace_ops:
					s 	= s.replace(*repl_args)

			return s

		# Get the current axis and subplot iteration
		ax 				= self.ax
		sp_state 		= self.sp_iter_state

		# Can have info only on one axis
		enabled 		= only_on if only_on is not None else ['x', 'y']

		# Extract the selectors for x and y
		sp_info 		= self.get_sweep_info(iter_name='subplot')
		x_info 			= sp_info.get('x')
		y_info 			= sp_info.get('y')

		log.debugv("Got sweep info for subplot %s:\n  x:  %s\n  y:  %s", sp_state, x_info, y_info)

		# Get subplot indices and complete subplot shape in order to know the position in the subplot array
		x_idx, y_idx 	= sp_state
		x_max, y_max 	= (self.sp_shape[0] - 1, self.sp_shape[1] - 1)

		# Decide whether to add the info to the axis label or the legend
		add_to_legend 	= bool(x_max == 0 or y_max == 0)

		# Basically, the information on the sweep that goes over the subplots in one row go on the top x-axis labels; for the sweep over a column the info goes to the right y-axis labels.
		if y_info and y_max > 0 and x_idx == x_max and 'y' in enabled:
			# Last column. Add y-subplot selector information onto right label
			y_info_text 	= format_label(fstr=y_label_fstr,
			                               capitalize=capitalize,
			                               replace_ops=replace_ops,
			                               **y_info)

			# Only add if it is not an empty string
			if y_info_text:
				# Distinguish whether to add to axis label or to legend
				if not add_to_legend:
					log.debugv("Adding right axis label: %s", y_info_text)
					ax.yaxis.set_label_position('right')
					ax.yaxis.set_label_text(y_info_text)
				elif y_info:
					add_to_lgd_title(y_info_text, mode=legend_title_mode)

		if x_info and x_max > 0 and y_idx == 0 and 'x' in enabled:
			# First row. Add x-subplot selector information
			x_info_text 	= format_label(fstr=x_label_fstr,
			                               capitalize=capitalize,
			                               replace_ops=replace_ops,
			                               **x_info)

			# Only add if it is not an empty string
			if x_info_text:
				# Distinguish whether to add to axis label or to legend
				if not add_to_legend:
					log.debugv("Adding top axis label: %s", x_info_text)
					ax.xaxis.set_label_position('top')
					ax.xaxis.set_label_text(x_info_text)
				else:
					add_to_lgd_title(x_info_text, mode=legend_title_mode)

	def _hlpr_add_loop_info(self): # TODO
		'''Add some information on the fixed sweep values to the plot'''
		raise NotImplementedError("add_loop_info")

	# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	# Extending functionality

	def _hlpr_add_cbar(self, *, redraw: bool=False, intv: tuple=None, **cbar_kwargs):
		'''Not only adds a colorbar, but redraws all registered patch collections to fit the same normalisation.'''
		if redraw:
			pxmap_info 	= self['pxmap_info']
			if not pxmap_info['num_registered']:
				log.caution("No registered PatchCollections to use for redrawing.")

			else:
				# Can redraw.
				# Find out the min and max data interval
				data_vmin 	= [l[0] for l in pxmap_info['data_vlims'].values()]
				data_vmax 	= [l[1] for l in pxmap_info['data_vlims'].values()]

				# Use these values to fill the None's in the intv
				intv 		= tuple(intv) if intv else (None, None)

				if intv[0] is None:
					intv 		= (np.nanmin(data_vmin),) + (intv[1],)
				if intv[1] is None:
					intv 		= (intv[0],) + (np.nanmax(data_vmax),)

				log.debug("Redrawing registered pxmap plots and setting normalisation interval to %s ...", intv)

				# Have the interval now. Create the new norm using the other kwargs that are defined in the style.
				_kws 		= copy.deepcopy(self.this_style.get('cnorm_kwargs', {}))
				_kws.pop('intv', None)

				cnorm 		= get_cnorm(intv=intv, cmap=self['cmap'], **_kws)

				# Set the attribute cnorm, that is used for drawing the cbar
				self['cnorm'] 	= cnorm

				# Now loop over all axes, and apply new facecolors to the PatchCollection
				for ax, (data, pc) in pxmap_info['by_ax'].items():
					# Disassociate from the axis
					pc.remove()

					# Set the facecolor anew
					pc.set_facecolor(self['cmap'](cnorm(data.flatten())))

					# And add it back to the axis
					ax.add_collection(pc)
				else:
					log.debug("Redrew %d PatchCollections, changing facecolor.", len(pxmap_info['by_ax']))

		super()._hlpr_add_cbar(**cbar_kwargs)

	def _hlpr_set_scales(self, xscale: str=None, yscale: str=None, xscale_kwargs: dict=None, yscale_kwargs: dict=None):
		'''Extends the parent method with filling the argument dictionaries with iterator values ... does so, if the `iter_name` key is given in the dictionaries.'''
		# Valid categorical scale identifiers; # TODO load from deval!
		CAT_SCALES= ['cat', 'categorical', 'index', 'at_value', 'cat_at_value']

		def cat_kwargs(*, mode, iter_name: str=None, dim: int=None, val_fstr: str='{:}', factor: float=None, **regular_kwargs):
			if iter_name is None:
				# The kwargs consist just of the mode and the regular kwargs
				return dict(mode=mode, **regular_kwargs)

			else:
				# Assume that the information from the iterator should be used
				# Warn if regular kwargs were given, as these will not be used
				if regular_kwargs:
					log.caution("Passed `iter_name` argument as well as other keys (presumably) for manually setting the categorical scale values: %s. These keys will be ignored.", regular_kwargs)

				# Get the info dict
				info 	= self.get_sweep_info(iter_name=iter_name, dim=dim)

				# Get the values
				values 	= info['values']

				if mode in ['at_value', 'cat_at_value']:
					# Ensure the values are numeric
					try:
						values 	= [float(v) for v in values]
					except Exception as err:
						# They were not; change the mode
						log.warning("Got categorical scale mode '%s', but encountered values (%s) that were not convertible to floats: %s\nChanging mode to 'index' ...", mode, values, err)
						mode 	= 'index'

				# Create labels from the values using the format string; if given, multiply with a factor first
				if factor is None:
					# Regular case
					labels 	= [val_fstr.format(v) for v in values]
				else:
					# Values multiplied with factor
					labels 	= [val_fstr.format(v*factor) for v in values]

				# Bundle in dict and return together with (potentially updated) mode
				return mode, dict(labels=labels, locations=values)

		# Adjust the kwargs
		if xscale in CAT_SCALES:
			xscale, xscale_kwargs 	= cat_kwargs(mode=xscale, **xscale_kwargs)
		if yscale in CAT_SCALES:
			yscale, yscale_kwargs 	= cat_kwargs(mode=yscale, **yscale_kwargs)

		# Call the parent method with the adjusted kwargs
		super()._hlpr_set_scales(xscale=xscale, xscale_kwargs=xscale_kwargs,
		                         yscale=yscale, yscale_kwargs=yscale_kwargs)

		# Done.

	# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	# Visuals -- mainly extending the parent methods to be subplot-aware

	def _hlpr_set_limits(self, *args, on_axarr_pos: list=None, **kwargs): # TODO add `expand_only` argument to allow for autoscaling
		'''Extension to the parent method to work on specific subplots.'''
		helper_on_axarr_pos(parent_method=super()._hlpr_set_limits,
		                    args=args, kwargs=kwargs,
		                    sp_state=self.sp_iter_state,
		                    on_axarr_pos=on_axarr_pos)

	def _hlpr_set_tick_locators(self, *args, on_axarr_pos: list=None, **kwargs):
		'''Extension to the parent method to work on specific subplots.'''
		helper_on_axarr_pos(parent_method=super()._hlpr_set_tick_locators,
		                    args=args, kwargs=kwargs,
		                    sp_state=self.sp_iter_state,
		                    on_axarr_pos=on_axarr_pos)

	def _hlpr_set_tick_formatters(self, *args, on_axarr_pos: list=None, **kwargs):
		'''Extension to the parent method to work on specific subplots.'''
		helper_on_axarr_pos(parent_method=super()._hlpr_set_tick_formatters,
		                    args=args, kwargs=kwargs,
		                    sp_state=self.sp_iter_state,
		                    on_axarr_pos=on_axarr_pos)

	def _hlpr_set_hv_lines(self, *args, on_axarr_pos: list=None, **kwargs):
		'''Extension to the parent method to work on specific subplots.'''
		helper_on_axarr_pos(parent_method=super()._hlpr_set_hv_lines,
		                    args=args, kwargs=kwargs,
		                    sp_state=self.sp_iter_state,
		                    on_axarr_pos=on_axarr_pos)

# .............................................................................

class GeneralSweep(SelectorSweep): # TODO
	pass

# -----------------------------------------------------------------------------
# Iterator helper
# TODO include into paramspace package?
# TODO remove dummy dimensions that are created in here?!
# FIXME determining iter_shape for a subplot iteration with a dummy dimension does not work (takes the ParamSpace.shape) ...

def pspace_iter(pspace, *, num_dims: int, iter_name: str, mode: str='last', num_state_dims: int=None):
	'''An implementation of an iterator for ParamSpace subspaces.

	To do so, for every loop iteration, a subspace is generated, where the dimensions that need to be fixed are just ParamDims of size 1 -- this has to be done like this, because the paramspace package does not supply iterators to do this.
	'''

	# Determine number of state dimensions
	num_state_dims = (num_dims if not num_state_dims
	                  else max(num_dims, num_state_dims))

	log.note("Starting %s iteration...", iter_name)
	log.debug("... with parameters:  %d dimensions,  %d state dimensions,  mode %s", num_dims, num_state_dims, mode)

	# Check if there are enough dimensions requested to carry on
	if num_dims == 0:
		# Simplest case -> just return a single point which is that of the current pspace.

		# Find out number of dimensions needed for the state
		state = tuple([0 for _ in range(num_state_dims)])

		log.debug("Zero dimensional iterator; yielding single point.")

		yield (state, pspace)
		return

	elif not isinstance(pspace, ParamSpace) or pspace.num_dimensions == 0:
		# A helpful error message
		log.warning("Did not receive a ParamSpace or a zero-dimensional ParamSpace in %s iterator, but a %s object. This might be due to a dummy sweep dimension, i.e. a Sweep object with one value only, and potentially leads to errors in a derived iterator. Check your plot configuration for sweeps with too little values.\n\nYielding single point ...", iter_name, type(pspace))
		yield (tuple([0 for _ in range(num_state_dims)]), pspace)
		# TODO check if this behaviour is desired
		return

	log.debug("... from ParamSpace object of shape:  %s", pspace.shape)

	# Not the case. Determine the shape of the 'loop' iterator
	if mode == 'last':
		iter_shape 	= pspace.shape[-num_dims:]
	elif mode == 'first':
		iter_shape 	= pspace.shape[:num_dims]
	else:
		raise ValueError("unsupported mode: "+str(mode))

	# If required, expand by adding dummy dimensions
	if num_state_dims:
		if num_state_dims < len(iter_shape):
			raise ValueError("iterator has more dimensions that argument `num_state_dims`: {} < {}".format(len(iter_shape), num_state_dims))

		else:
			# FIXME can this be removed?!
			# Add >= 0 dummy dimensions of size one to have a state shape of the required number of dimensions
			# Calculate number of dummy dimensions
			num_dd 		= num_state_dims - len(iter_shape)
			log.debug("Adding %d dummy dimension(s) to get a state of dimensionality %d.", num_dd, num_state_dims)

			# Adjust the iterator shape accordingly
			add_dims 	= tuple([1 for _ in range(num_dd)])
			if mode == 'last':
				iter_shape += add_dims
			elif mode == 'first':
				iter_shape = add_dims + iter_shape

	else:
		# No dummy dimensions
		num_dd 	= 0

	log.state("%s", "  {:<32s}{:}".format(iter_name+" iterator shape:", iter_shape))

	# Use the product iterator to go over the slices to make
	for state in itertools.product(*[range(s) for s in iter_shape]):
		# The state variable is now a tuple of which indices to select for the subspace
		log.state("%s", "  {:<32s}{:} / {:}".format(iter_name+" iterator state:", state, tuple([i-1 for i in iter_shape])))

		# Create slices according to the state (without dummy dimensions)
		if num_dd > 0:
			slices 	= [slice(idx, idx+1) for idx in state[:-num_dd]]
		else:
			slices 	= [slice(idx, idx+1) for idx in state]

		if num_dims < pspace.num_dimensions:
			if mode == 'last':
				# Add Ellipsis in front to select the last axes
				slices 	= [Ellipsis] + slices
			elif mode == 'first':
				# Add Ellipsis in back to select first axes (also default behaviour, but better to make it explicit here)
				slices 	+= [Ellipsis]

		log.debug("Creating subspace and returning iterator ...")
		log.debug("%s pspace # dims:  %s", iter_name, pspace.num_dimensions)
		log.debug("Slices to apply:   %s", slices)

		# Get the subspace
		subspace 	= pspace.get_subspace(*slices, squeeze=True,
		                                  as_dict_if_0d=True)

		# Yield a tuple of the current state and the subspace
		yield (state, subspace)

	log.note("%s iterator finished.", iter_name.capitalize())
	return

# -----------------------------------------------------------------------------
# Other helpers

def helper_on_axarr_pos(*, parent_method, args: tuple, kwargs: dict, on_axarr_pos: list, sp_state: tuple):

	if not on_axarr_pos:
		parent_method(*args, **kwargs)

	else:
		log.debug("Calling helper only on specified axarr positions ...")

		for params in on_axarr_pos:
			_kwargs	= copy.deepcopy(params)
			pos 	= _kwargs.pop('pos')
			_kwargs = deeeval.tools.rec_update(copy.deepcopy(kwargs), _kwargs)

			# Can also compare partially, if there is a None in the tuple
			if pos[0] is None or pos[0] == sp_state[0]:
				# Matches in x-direction -> check y.
				if pos[1] is None or pos[1] == sp_state[1]:
					# Matches in y-direction -> execute helper.
					parent_method(*args, **_kwargs)

