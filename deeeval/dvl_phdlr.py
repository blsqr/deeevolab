''' This module holds a PlotHandler child for working with deeevoLab.'''

import deeeval
import deeeval.dvl_wrapr

# Setup logging for this file
log	= deeeval.get_logger(__name__)
cfg = deeeval.get_config(__name__)
log.debug("Loaded module, config, and logger.")

# Local constants

# -----------------------------------------------------------------------------

class DVLPlotHandler(deeeval.PlotHandler):

	# Overwrite class constants of parent class
	# NOTE this is only used to make the class constants accessible from cfg

	# Set the module in which to look for plot wrappers and a default wrapper
	PLOT_WRAPPERS 		= deeeval.dvl_wrapr
	DEFAULT_WRAPPER_NAME= cfg.default_wrapper_name

	# Set the defaults for the filename format strings
	FNAME_FSTRS 		= cfg.fname_fstrs

	@property
	def sweepspace(self):
		'''Returns the ParamSpace object that is used in the SelectorSweep plot wrapper.'''
		return self.dm['pspace']
