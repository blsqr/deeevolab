''' This module holds DataContainer-derived classes that extend their functionality specific to the deeevoLab data.'''

import copy
from typing import Union, Sequence, Tuple

import numpy as np
from paramspace import ParamSpace

import deeeval

# Setup logging for this file
log	= deeeval.get_logger(__name__)
cfg = deeeval.get_config(__name__)
log.debug("Loaded module, config, and logger.")

# Local constants

# -----------------------------------------------------------------------------
# Base Groups and Containers

class DVLContainer(deeeval.NumpyDC):
	pass

class DVLGroup(deeeval.DataGroup):

	# Public methods ..........................................................
	# TODO migrate these to deval?

	def hasitem(self, key: Union[str, Sequence]) -> bool:
		'''Checks if the key or keysequence is part of this DataManager.'''
		if isinstance(key, (tuple, list)):
			grp 	= self
			for _key in key:
				if _key not in grp:
					return False
				grp 	= grp[_key]
			else:
				return True

			# TODO alternatively: maybe via __getitem__ and try-except?!

		else:
			return bool(key in self)

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Container extensions

class DVLStateMap(DVLContainer):
	'''This is a specific form of an n-dimensional numpy array that is made to work with the state map of a parameter sweep. It will always hold data of the same shape as the state map.'''

	def __init__(self, *args, state_map: np.ndarray, pspace, dim_info: dict=None, data=None, **kwargs):

		# No data should be passed
		if data is not None:
			log.warning("No data should be passed to DVLStateMap. Ignoring ...")
			data 		= None

		# If a corresponding pspace is given and no dim_info is available, construct the dim_info
		if not dim_info:
			spans 		= {k: v for k, v in enumerate(pspace.get_spans())}
			names 		= {k: v for k, v in enumerate(pspace.get_span_names())}

			# Bundle into dim_info dict
			dim_info 	= _bundle_dim_info(spans=spans, axis_names=names)

		# Pass to parent method, saving the dim info in the attributes
		super().__init__(*args, data=None, attrs=dim_info, **kwargs)

		# Additional attributes
		self._map 	= state_map
		self._pspace= pspace

	@property
	def shape(self) -> tuple:
		'''Returns the shape of the state map.'''
		return self.state_map.shape

	@property
	def state_map(self) -> np.ndarray:
		'''Returns the state map data as a np.ndarray.'''
		return self._map

	@property
	def dim_info(self):
		'''Returns the dimension information corresponding to the current map. As this is saved in the attributes, the features of the DataAttributes class can be used ...'''
		return self.attrs

	@property
	def pspace(self) -> ParamSpace:
		'''Returns the pspace corresponding to the current map'''
		return self._pspace

	# .........................................................................
	# Item access

	def get_axis_no(self, *, axis_name: str) -> int:
		'''Tries to find the axis with the specified name from the axis information and returns the corresponding axis number.

		Args:
			axis_name: 	Name of the axis to get the axis number from
		Returns:
			int: 		axis number
		Raises:
			KeyError: 	if the specified name is not available
		'''
		def is_subseq_from_end(l1: Union[Sequence[str], tuple], l2: Union[Sequence[str], tuple]):
			check_length 	= min(len(l1), len(l2))
			if not check_length:
				raise ValueError("One of the key lists was of length zero.")

			return bool(tuple(l1[-check_length:]) == tuple(l2[-check_length:]))

		# Make sure it is an iterable; makes comparisons easier
		if not isinstance(axis_name, (tuple, list)):
			axis_name 	= (axis_name,)

		# Get the dict that maps axis names to axis numbers
		abn 	= self.dim_info['axis_by_name']
		for keys, axis_no in abn.items():
			# Make sure the key is an iterable
			if not isinstance(keys, (tuple, list)):
				keys 		= (keys,)

			if is_subseq_from_end(keys, axis_name):
				return axis_no

		else:
			raise KeyError("No such axis_name '"+str(axis_name)+"' in dimension information: "+str(self.dim_info.data))

	def build_select_tuple(self, select: list) -> tuple:
		'''Returns a tuple that can be applied to the state map to get a subset of the map.'''
		if len(select) != len(self.shape):
			raise ValueError("Need a `select` list of the same length as the number of dimensions of the state map ({}).\nGot: {}".format(len(self.shape), select))

		# The selector to be filled
		sel 	= [None for _ in self.shape]

		# Go over and fill the selector
		for dim in select:
			axis_no 	= self.get_axis_no(axis_name=dim['key'])
			sel[axis_no]= dim['idx']

		return tuple(sel)

	def get_state_id(self, *, select: list) -> int:
		'''Selects a single point of the state map and returns that points id. This does not perform any reshaping, axis swapping, etc. but only tries to identify a point from the list of selections. It will only work if the `select` argument fully specifies the point.'''

		sel 	= self.build_select_tuple(select)

		if any([bool(not isinstance(i, int)) for i in sel]):
			raise ValueError("Required a fully specified select tuple, only containing integers. Got: "+str(sel))

		pt 		= self.state_map[sel]

		log.debug("Got point with state id: %d", pt)
		return pt

	def iter_state_id(self, *, select: list) -> int:
		'''Iterates over the state ids specified by the select list.'''
		sel 	= self.build_select_tuple(select)

		id_arr 	= self.state_map[sel] # will be either ndarray or numpy.int64

		log.debug("Iterating over %s subarray of shape %s ...", self.classname, id_arr.shape)

		if not id_arr.shape:
			# Only one value to return
			yield id_arr
			return

		# Iterate over flat array
		for _id in id_arr.flat:
			yield _id
		return

	# .........................................................................

	def select(self, select: list, *, default_slice=slice(None), TargetCls=None, **umap_kwargs):
		'''Given the names of spans to select and span values to fix, this method returns a new DVLUniverseMap or DVLStateMap object with the the selected state map.

		Args:
			select: 	the selector list
			default_slice: if given, it is used to slice the freely floating dimensions. Default is to select the whole dimension
			TargetCls:  The class of the target
			**umap_kwargs: Passed to DVLStateMap.__init__

		Returns:
			TargetCls, if given, else self.__class__
			Holds the selected state map and corresponding information
		'''

		if not self.pspace:
			log.error("Cannot select from %s '%s' without the corresponding ParamSpace object being present.", self.classname, self.name)
			if deeeval.DEBUG:
				raise ValueError("No pspace; cannot perform select")
			return

		log.debug("Selecting from %s '%s'...", self.classname, self.name)

		# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
		# Helper methods
		# NOTE some of these rely on the namespace of this (outer) scope

		def remove_dim_from_avail_dims(dim_no):
			if dim_no in avail_dims:
				avail_dims.remove(dim_no)
				return True
			else:
				# variables are available from outer scope
				msg 	= "No dimension with name '%s' available. Check whether the dimension is available in the parameter space and check your plot configuration whether the dimension was already used otherwise." % dim['key']
				log.error(msg)
				print("Parameter Space keys:\n{:span_names}\n".format(pspace))
				print("Remaining dimension numbers:\n  %s" % avail_dims)
				print("Remaining dimension names:\n  " + "\n  ".join([" -> ".join(pspace.get_span_names()[no]) for no in avail_dims]))

				raise ValueError(msg)

		def parse_dim_key(key) -> Union[Tuple[str], str]:
			if isinstance(key, str):
				return key
			elif isinstance(key, (tuple, list)):
				# Make sure it is a tuple
				return tuple(key)
			else:
				raise TypeError("Invalid span name type {} (value: {}) encountered.".format(type(key), key))

		def str_of_dim_key(key) -> str:
			'''Creates a well readable string of the key or key sequence'''
			if isinstance(key, (tuple, list)):
				# Make sure they are all strings
				key 	= [str(k) for k in key]

				if len(key) > 4:
					return "… -> " + " -> ".join(key[-4:])
				else:
					return " -> ".join(key)
			else:
				# Regular string cast
				return str(key)

		def calc_swaps(l, sort_desc=False) -> tuple:
			''' Sorts list l only by swapping operations and returns a list of the swaps as 2-tuples of the swap indices.

			FIXME: Might fail with duplicate items?!
			'''
			log.progress("Calculating axis swaps ...")
			log.debug("Received list:  %s", l)

			l 		= copy.deepcopy(list(l))	# work on a copy and on a list
			i 		= 0 						# the current index
			aim_l 	= sorted(l)					# the order the items _should_ be in
			if sort_desc:
				aim_l = aim_l[::-1]
			swaps	= [] 						# the list of swaps

			while i < len(l):
				if l[i] != aim_l[i]:
					swap 	= (i, aim_l.index(l[i]))
					l[swap[0]], l[swap[1]] 	= l[swap[1]], l[swap[0]]
					swaps.append(swap)
				else:
					i += 1

			log.debug("Swaps: %s", swaps)

			return swaps

		def parse_selection(select: list, missing_slice_warn: bool=False):
			'''Parses the entries of the given list into the (outer scope) dicts and lists ...'''
			# Loop over the entries
			for dim in select:
				# Parse the dimensions key and get the dimension number in parameter space. If the key is given, do it in that way. If not, use the dim_no key and determine the name of the span by
				if dim.get('key'):
					dim_key 	= parse_dim_key(dim['key'])
					dim_no 		= pspace.get_span_dim_no(dim_key)

					if dim_no is None:
						log.warning("Dimension '%s' is not part of ParamSpace -- will be skipped.", dim_key)
						# TODO this should raise KeyError (even in pspace!)
						continue

				else:
					dim_no 		= dim['dim_no']
					dim_key 	= pspace.get_span_names()[dim_no]

				# Got key and dimension number now
				log.debug("dim_no: %s, key: %s",
				          dim_no, str_of_dim_key(dim_key))

				# Mark no longer available
				remove_dim_from_avail_dims(dim_no)

				# Determine the new axis number
				new_dim_no			= len(slices)

				# Add to the dimensions that should be swapped
				swp_dims.append(dim_no)

				# Get the slice object from the ìdx` to select entry;
				idx 				= dim.get('idx')
				if isinstance(idx, int):
					# An index was given; use that to construct a slice
					slc 				= slice(idx, idx+1)

				elif isinstance(idx, slice):
					# Use as it is
					slc 				= idx

				elif dim.get('val') is not None:
					# Select by the given value
					# TODO
					raise NotImplementedError("selection by value")

				elif idx is None:
					if missing_slice_warn:
						log.caution("Could not find slice definition for '%s'! A slice should be defined by passing either of these keys to the selector:  %s\nUsing default slice instead ...", dim_key, ", ".join(['idx', 'slice_args', 'slice']))
					else:
						log.debug("Using default slice for dim '%s' ...", dim_key)
					slc 				= default_slice

				slices[new_dim_no] 	= slc

				# Add information on the span and the name
				spans[new_dim_no] 	= pspace.get_span(dim_no)
				names[new_dim_no] 	= dim_key
				info[new_dim_no] 	= dim

				log.state("New axis %d:  %s",
				          new_dim_no, str_of_dim_key(dim_key))
				log.debug("…using slice: %s", slices[new_dim_no])

		# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
		# Prepare variables
		select 		= select if select is not None else []
		state_map 	= self.state_map
		pspace 		= self.pspace
		num_dims 	= pspace.num_dimensions
		avail_dims 	= list(range(num_dims)) # available dimension numbers
		swp_dims 	= [] 	# the order of swaps to perform
		slices 		= {} 	# to record which states to use, key: dim_no
		spans 		= {} 	# to record the corresponding spans, key: dim_no
		names 		= {} 	# to record the corresponding names, key: dim_no
		info 		= {} 	# other info from to_select dict, key: dim_no

		# Loop over the dimensions in the selector list and populate the above list and dicts
		log.progress("Parsing selection with %d entries ...", len(select))
		parse_selection(select, missing_slice_warn=True)
		log.progress("Selection parsed.")

		# Check if there are still floating dimensions
		if avail_dims:
			# Generate a helpful log message
			span_names	= pspace.get_span_names()
			float_dims 	= ["  {:}:  {:}".format(n,
			                                    str_of_dim_key(span_names[n]))
			               for n in avail_dims]

			log.caution("There are %d floating dimensions remaining:\n%s\n"\
			            "These are still part of the returned state map!",
			            len(avail_dims),
			            "\n".join(float_dims))

			# Handling of "free" (i.e. not manually fixed) dimensions: they should also be part of the dim_info ... -> Generate a list of dictionaries with the `dim_no` key; this can then be fed to parse_selection again and will generate dim_info entries.
			log.progress("Handling floating dimensions ...")
			parse_selection([dict(dim_no=n) for n in avail_dims])

		else:
			log.debug("No freely floating dimensions remaining.")

		dim_info 	= _bundle_dim_info(spans=spans, axis_names=names,info=info)
		log.debug("Bundled dimension information:\n%s", deeeval.PP(dim_info))

		# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
		# Swap the axes of the inverse state map to have the axes in the same order as the dimensions are to be plotted

		# From the swp_dims list, the list of necessary axes swaps is calculated, that is needed to bring the inverse mapping into the correct axes order.
		swaps 		= calc_swaps(swp_dims)
		log.debug("Swap operations: %s", swaps)

		# Create the slicing tuple
		selector 	= [None for _ in range(num_dims)]
		for idx, slc in slices.items():
			selector[idx] 	= slc
		selector 	= tuple(selector)
		log.debug("Selector:\n  %s", selector)

		# Need to apply these to the new pspace as well # TODO
		ps 			= self._apply_swaps_and_selection_to_pspace(swaps,
		                                                        selector)

		# Apply the swaps and the selection to the state map and create a new object
		TargetCls 	= TargetCls if TargetCls else self.__class__
		dvlmap 		= self._swap_select_and_init(swaps, selector,
		                                         TargetCls=TargetCls,
		                                         pspace=None, # TODO
		                                         dim_info=dim_info,
		                                         name='')
		# TODO find an informative name for the target object?

		# Done. Return.
		return dvlmap

	def _apply_swaps_and_selection_to_pspace(self, swaps, selector): # TODO
		'''As the name says ...'''
		# TODO Applying swaps and selection to pspace is not yet implemented ... Right now, it is not possible to select from the created object!

		return None

	def _swap_select_and_init(self, swaps, selector, *, TargetCls, **init_kwargs):
		'''Applies the given swaps and the selector to the state map and passes that and the other information to TargetCls.__init__'''
		log.debug("Performing %d swap operations on state map ...", len(swaps))


		state_map 	= self.state_map
		for swp in reversed(swaps): # TODO why reversed?
			state_map 	= np.swapaxes(state_map, *swp)

		log.debug("Finished swap operations.")
		# The state_map now holds the simulations numbers in the right axis order, i.e. corresponding to the order given in the selector

		log.debug("Applying selector ...")
		log.debugv("Selector:  %s", selector)

		state_map 	= state_map[selector]

		if 0 in state_map.shape:
			log.warning("Application of selector\n  %s\nresulted in one or more state map dimensions being of size 0, effectively reducing the state map volume to 0. Check your select list configuration and compare it with the shape of the multiverse: %s.", selector, self.state_map.shape)

		log.debug("Initialising %s ...", TargetCls.__name__)
		return TargetCls(state_map=state_map, **init_kwargs)

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class DVLUniverseMap(DVLStateMap):
	'''Extension of a StateMap that also holds the data ...'''

	def __init__(self, *args, data=None, dtype='object', **kwargs):

		super().__init__(*args, data=None, **kwargs)

		# Now create a data array with the shape of the state map
		if data is None:
			log.debug("Initialised %s '%s' without data; fully masking array.", self.classname, self.name)
			data 		= np.zeros(self.state_map.shape, dtype=dtype)
			ma_kwargs 	= dict(mask=True)
		else:
			ma_kwargs 	= dict(dtype=dtype)

		# Create the masked data array; if the input data was a masked array already, that mask will be preserved.
		self.data 	= np.ma.MaskedArray(data, **ma_kwargs)

		# Additional attributes
		# For caching of properties
		self._item_shapes 		= None
		self._max_item_shape	= None
		self._same_item_shapes	= None
		self._item_dtypes 		= None
		self._same_item_dtypes	= None

		# For cache of ndarray version of the data
		self._data_ndarray 		= None
		self._data_ndarray_params=None

	def _swap_select_and_init(self, swaps, selector, *, TargetCls, **init_kwargs):
		'''Applies the given swaps and the selector to the state map the data, then passes that and the other information to TargetCls.__init__'''
		log.progress("Performing %d swap operations on state map and data...",
		             len(swaps))

		state_map 	= self.state_map
		data 		= self.data

		for swp in reversed(swaps): # TODO why reversed?
			state_map 	= np.swapaxes(state_map, *swp)
			data 		= np.swapaxes(data,      *swp)

		log.debug("Finished swap operations.")
		# The state_map now holds the simulations numbers in the right axis order, i.e. corresponding to the order given in the selector

		log.progress("Applying selector to state map and data ...")
		state_map 	= state_map[selector]
		data 		= data[selector]

		log.progress("Initialising %s ...", TargetCls.__name__)
		return TargetCls(state_map=state_map, data=data, **init_kwargs)

	# .........................................................................
	# Item-related data extraction
	# NOTE an "item" is the entry that stems from a single universe, corresponding to each entry in the state map

	@property
	def item_shapes(self) -> np.ndarray:
		'''Returns an np.ndarray with the shapes of each item.'''
		# Calculate only if not already done
		if self._item_shapes is None:
			shapes 	= np.zeros(self.shape, dtype='object')

			# Iterate over the newly created array and at each point store the data array's shape
			it 	= np.nditer(shapes, flags=['multi_index', 'refs_ok'])
			while not it.finished:
				midx 		= it.multi_index
				# See if a shape attribute is available (might not be np data)
				try:
					shape 		= self[midx].shape
				except AttributeError:
					if isinstance(self[midx], (int, float, str)):
						shape 		= tuple()
					else:
						# Really could not determine the shape
						shape 		= None
				# Set it and continue
				shapes[midx]= shape
				it.iternext()

			# Store as attribute
			self._item_shapes 	= shapes
		return self._item_shapes

	@property
	def max_item_shape(self) -> tuple:
		'''Returns the highest item
		shape'''
		if self._max_item_shape is None:
			try:
				self._max_item_shape 	= np.nanmax(self.item_shapes)
			except Exception as err:
				# Data was probably without shape
				log.debug("Could not determine max item shape. %s: %s\nAssuming scalar.", err.__class__.__name__, err)

				# Set to empty tuple -> interpreted as scalar
				self._max_item_shape = tuple()

		return self._max_item_shape

	@property
	def same_item_shapes(self) -> bool:
		'''Returns True if all items have the same shape'''
		# Need to create a test array, because a tuple cannot be compared with an array ...
		if self._same_item_shapes is None:
			test_arr 	= np.zeros(self.item_shapes.shape, dtype='object')
			test_arr.fill(self.max_item_shape)

			self._same_item_shapes 	= np.all(test_arr == self.item_shapes)
		return self._same_item_shapes

	@property
	def item_dtypes(self) -> np.ndarray:
		'''Returns an np.ndarray with the dtypes of each item.'''
		# Calculate only if not already done
		if self._item_dtypes is None:
			dtypes 	= np.zeros(self.shape, dtype='object')

			# Iterate over the newly created array and at each point store the data array's dtype
			it 	= np.nditer(dtypes, flags=['multi_index', 'refs_ok'])
			while not it.finished:
				midx 		= it.multi_index
				# See if a dtype is defined:
				try:
					dtype 		= self[midx].dtype
				except AttributeError:
					dtype 		= type(self[midx])
				# Set it and continue
				dtypes[midx] = dtype
				it.iternext()

			# Store in attribute
			self._item_dtypes 	= dtypes
		return self._item_dtypes

	@property
	def same_item_dtypes(self) -> bool:
		'''True, if all dtypes are the same'''
		if self._same_item_dtypes is None:
			test_arr 	= np.zeros(self.item_dtypes.shape, dtype='object')
			test_arr.fill(self.item_dtypes.flat[0])

			self._same_item_dtypes 	= np.all(test_arr == self.item_dtypes)
		return self._same_item_dtypes

	# .........................................................................
	# Retrieving and working with data

	def as_ndarray(self, squeeze: bool=False, ensure_dims: int=0, pad_kwargs=None, dtype=float) -> np.ndarray:
		'''Returns a copy of the whole universe data as a (numeric) numpy array, i.e. not with dtype 'object' but with a numeric dtype.

		To do so, the shape is extended to hold both the axes that belong to the parameter sweep and the axes that belong to each universe's data.

		If there was a shape mismatch, the smaller arrays are padded (according to pad_kwargs or by default with np.nan) to get to the correct size.

		Uses python float as generic numeric dtype.
		'''

		_params 	= dict(squeeze=squeeze,
		                   ensure_dims=ensure_dims,
		                   pad_kwargs=pad_kwargs)

		# Check if a new array needs to be created or not
		if self._data_ndarray is None or _params != self._data_ndarray_params:
			log.debugv("Converting arrays of the following shapes and dtypes into a new ndarray ...\n\n%s\n\n%s\n", self.item_shapes, self.item_dtypes)

			# Save parameters to attribute to check for next time
			self._data_ndarray_params 	= _params

			# Determine target shape
			if not self.same_item_shapes:
				log.caution("There was an item shape mismatch. Using largest item shape and padding smaller arrays ...")
			shape 	= self.shape + self.max_item_shape

			# Determine pad args and kwargs
			if pad_kwargs:
				pad_kwargs 	= copy.deepcopy(pad_kwargs)
				pad_mode 	= pad_kwargs.pop('mode', 'constant')
			else:
				# Pad with NaNs
				pad_mode 	= 'constant'
				pad_kwargs 	= dict(constant_values=np.nan)

			# Create a new array that has the shape of this UniverseMap plus that of the data
			log.progress("Creating target array ... (shape %s, dtype %s)\n",
			             shape, dtype)
			arr 	= np.ma.MaskedArray(np.zeros(shape, dtype=dtype))

			# Iterate over the first dimensions and fill with the data from the UniverseMap
			it 	= np.nditer(self.state_map, flags=['multi_index', 'refs_ok'])
			while not it.finished:
				midx 		= it.multi_index
				log.debugv(" %s -> uni %s", midx, it[0])

				# Get the entry
				entry 		= self[midx]

				# Check if it needs to be adjusted to match the target shape
				if self.max_item_shape and entry.shape < self.max_item_shape:
					log.debugv("Need to pad to get to shape %s. mode: %s, kwargs: %s", self.max_item_shape, pad_mode, pad_kwargs)

					# Save the old shape
					entry_shape = entry.shape

					# Pad the entry to match the target array size
					pads 		= [(0, mis-es)
					               for es, mis in zip(entry.shape,
					                                  self.max_item_shape)]
					entry 		= np.pad(entry, tuple(pads), pad_mode,
					                     **pad_kwargs)

					# Convert it to a fully masked array
					entry 		= np.ma.MaskedArray(data=entry, mask=True,
					                                dtype=dtype)

					# Unmask the values using the old shape
					entry.mask[tuple([slice(s) for s in entry_shape])] = False

				else:
					# no shape defined -> scalar value -> no padding necessary
					pass

				# Set the entry
				arr[midx] 	= entry
				# NOTE this indexation leaves open the last axes, which are the ones that correspond to each item's shape

				# Done with this point, continue
				it.iternext()

			# Save to attribute
			self._data_ndarray 	= arr

		# Work on a copy
		arr = np.copy(self._data_ndarray)

		if squeeze:
			lsa	= len(self.shape) - (ensure_dims - 1)
			log.debug("Squeezing the first axes (maximum up to %d) ...", lsa)
			arr	= deeeval.tools.squeeze_first(arr, last_axis=lsa)

		if ensure_dims:
			arr	= deeeval.tools.ensure_num_dims(arr, num_dims=ensure_dims)

		log.debug("Returning copy of ndarray ... (squeeze: %s, ensure_dims: %s)", squeeze, ensure_dims)
		return arr

	def mean_and_std(self, *axes, allow_axis_idx: bool=False, squeeze: bool=False, ensure_dims: int=None, pad_kwargs: dict=None) -> tuple:
		'''Returns a tuple of (mean, std) calculated from this UniverseMap.


		If `ensure_dims` is given, the data array gets reshaped with dimensions of size one until the number of dimensions is reached. If there are more than this number of dimensions, an Exception is raised.'''

		# Make sure the axes argument is given and a list
		if not axes:
			raise ValueError("Need `axes` argument specified to calculate mean and standard deviation, got: "+str(axes))
		elif isinstance(axes, str):
			axes 	= [axes]

		log.progress("Calculating mean and std of %s '%s' ... (axes: %s)",
		             self.classname, self.name, axes)

		# Get the base data
		mean 	= self.as_ndarray(pad_kwargs=pad_kwargs)
		std 	= self.as_ndarray(pad_kwargs=pad_kwargs)

		# Loop over the specified axis names
		for axis in axes:
			# Try resolve the axis number by name
			try:
				axis_no	= self.get_axis_no(axis_name=axis)
			except KeyError:
				if not allow_axis_idx:
					raise
				else:
					log.debug("Could not resolve axis number by name; using it as axis index directly. Control this behaviour with the `allow_axis_idx` argument.")
					axis_no = None
			else:
				log.debug("Selected axis %d by name '%s'.", axis_no, axis)

			if not axis_no and allow_axis_idx:
				# Assume that it is a valid axis index and use that for the axis number ...
				axis_no 	= axis

			# Calculate mean and std, preserve the axes
			mean 	= np.nanmean(mean, axis=axis_no, keepdims=True)
			std 	= np.nanstd(std, axis=axis_no, keepdims=True)

		if squeeze:
			lsa 	= len(self.shape) - 1
			log.debug("Squeezing the first axes (maximum up to %d) ...", lsa)
			mean 	= deeeval.tools.squeeze_first(mean, last_axis=lsa)
			std		= deeeval.tools.squeeze_first(std, last_axis=lsa)

		if ensure_dims:
			mean 	= deeeval.tools.ensure_num_dims(mean, num_dims=ensure_dims)
			std		= deeeval.tools.ensure_num_dims(std, num_dims=ensure_dims)

		# Done
		log.debug("Returning mean and std of shape: %s ...", mean.shape)
		return mean, std

	def axis_operations(self, *axis_ops, squeeze: bool=False, ensure_dims: int=0, pad_kwargs: dict=None, otype=None) -> np.ndarray:
		'''Performs a number of axis operations on the UniverseMap.

		An axis operation dictionary has the following entries:
			axis: 			name or index of axis to work on
			operation: 		callable or string that will evaluate to callable
			**operation_kwargs

		Args:
			*axis_ops: 		a list of dictionaries where each dict specifies a single axis operation
			squeeze: 		whether to squeeze the final array
			ensure_dims: 	how many dimensions the final array should have
			pad_kwargs: 	if padding is needed, the corresponding kwargs
			otype: 			output dtype

		Returns:
			np.ndarray with the applied operations
		'''

		def apply_axis_operation(darr, *, axis, operation, bool_ix: str=None, invalid_val: float=np.nan, **op_kwargs):
			'''Applies the given axis operation arguments to the data array.'''
			# Get the desired axis number
			if isinstance(axis, int):
				# Directly use that value
				log.debug("Using given integer axis number for axis operation.")
				axis_no 	= axis
			else:
				axis_no 	= self.get_axis_no(axis_name=axis)

			# Resolve the operation function
			if callable(operation):
				# Can directly use the given operation
				op_func 	= operation
			elif isinstance(operation, str):
				# Evaluate the string to get the method
				log.debug("Eval'ing operation string ...\n  '%s'", operation)
				op_func 	= eval(operation)
			else:
				raise ValueError("Expected callable or string, got: "+str(operation))

			# If a string to create a boolean index access is given, apply that, making sure that the dimensionality remains the same
			if bool_ix:
				# Parse the string to a method
				log.debug("Creating boolean index array with: '%s' ...", bool_ix)
				bfunc  		= eval(bool_ix)

				# Now apply this, which might change the shape. Thus, need to keep track of the old shape
				old_shape 	= darr.shape
				darr 		= darr[bfunc(darr)]

				if darr.size == 0:
					# Create new array of old shape and fill with the given value
					darr 		= np.zeros(old_shape, dtype=float)

					# Now fill the array solely with that value
					darr.fill(invalid_val)

				elif len(darr.shape) < len(old_shape):
					# Expand the dimensions back
					while len(darr.shape) < len(old_shape):
						darr 		= np.expand_dims(darr, 0)

				elif len(darr.shape) > len(old_shape):
					raise ValueError("Array larger after applying boolean index -- this is fishy.")

				else:
					# Everything fine, continue ...
					pass

			# Apply the operation
			log.debug("Applying axis operation '%s' on axis %d of array of shape %s ...", operation, axis_no, darr.shape)

			darr 		= op_func(darr, axis=axis_no, **op_kwargs)

			return darr

		# Get the base data
		darr 	= self.as_ndarray(pad_kwargs=pad_kwargs)

		# Go over the defined axes operations
		for i, ax_op in enumerate(axis_ops):
			log.debug("Applying axis operation %d/%d ...", i+1, len(axis_ops))

			# Save previous shape
			old_shape 	= darr.shape

			# Apply the operation
			darr 	= apply_axis_operation(darr, **ax_op)

			# If more than one axis operation will be applied, check if there was a shape change that will complicate axis identification.
			# TODO keep track of reduced axes such that this is not a problem
			if len(axis_ops) > 1 and len(old_shape) != len(darr.shape):
				log.caution("The axis operation '%s' on axis '%s' resulted in a change in dimensions; this will make identifying axes by their names no longer possible!", operation, axis)
		else:
			log.progress("Applied %d axis operations.", i+1)

		# Squeeze and ensure number of dimensions
		if squeeze:
			lsa 	= len(self.shape) - 1
			log.debug("Squeezing the first axes (maximum up to %d) ...", lsa)
			darr 	= deeeval.tools.squeeze_first(darr, last_axis=lsa)

		if ensure_dims:
			darr 	= deeeval.tools.ensure_num_dims(darr, num_dims=ensure_dims)

		# Apply output dtype
		if otype is not None:
			darr 	= darr.astype(otype)

		return darr

	# .........................................................................
	# Helpers


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Group extensions

class DVLPPResults(DVLGroup):
	'''This group holds preprocessing results data.'''
	pass

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class DVLUniverse(DVLGroup):
	pass

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class DVLUniverses(DVLGroup):
	'''A group that has as its entries the data of all universes.'''

	def __init__(self, *args, pspace=None, **kwargs):

		super().__init__(*args, **kwargs)

		# Additional attributes
		self._pspace 		= pspace
		self._smap 			= None

	def return_tree(self, shorten: bool=cfg.DVLUniverses.shorten_tree):
		'''Adjusts the return_tree method to not show the whole number of universes but only the first one ...'''

		if not shorten and len(self) <= 42:
			return super().return_tree()

		tree 		= {}

		# Get only the first iterator item:
		for k, v in self.items():
			tree[k] 	= v.return_tree()
			break

		# Add an indicator how many more are available
		ind_key 	= "~ (and {n:} more)".format(n=len(self) - 1) # ~ for ordering
		tree[ind_key] = v

		return tree

	# .........................................................................
	# Properties

	@property
	def pspace(self) -> ParamSpace:
		return self._pspace

	@property
	def state_map(self) -> DVLStateMap:
		'''Return and cache the state map of the ParamSpace. This is done in a DVLStateMap object that is then accessible under this property.'''
		if self.pspace:
			if self._smap is None:
				log.progress("Getting and caching state map ...")
				smap 		= self.pspace.get_inverse_mapping()

				# Convert to DVLStateMap object, which saves
				self._smap 	= DVLStateMap(name='universe_state_map',
				                          pspace=self.pspace,
				                          state_map=smap)

				log.debug("Created %s '%s'", self._smap.classname, self._smap.name)
			return self._smap

		else:
			log.caution("There was no ParamSpace associated with this %s. Cannot get state mapping.", self.classname)
			return None

	# .........................................................................
	# Universe and data access

	def get_single_universe(self, *, select: Sequence[dict]) -> DVLUniverse:
		'''Returns the DVLUniverse object corresponding to the given universe select list.'''
		uni_id 	= self.state_map.get_state_id(select=select)
		return self[uni_id]

	def iter_universes(self, *, select: Sequence[dict]) -> DVLUniverse:
		'''Iterates over the subset of universes defined by the select list.'''
		for uni_id in self.state_map.iter_state_id(select=select):
			yield self[uni_id]
		return

	def select_data(self, *, select: Sequence[dict], data_keyseq: Sequence[str], data_keyseq_info: Sequence[str]=None, map_func=None, otype='object', dummy_sweeps: dict=None, dummy_sweeps_info: dict=None, **map_kwargs):
		'''Selects data (specified by `data_keyseq`) from a subset of universes (specified by `select`) and returns them in a np.ndarray with the selected data at the corresponding array entry.

		Args:
			select: 		The universe selector
			data_keyseq: 	The sequence of keys to select the data with
			data_keyseq_info: Information about each entry
			map_func:		A function that, if given, is applied to each selected data entry before writing it to the target array
			otype: 			The dtype of the output array. Default is `object`.
			dummy_: 		Used to catch further entries from the unpacking of the selector; are ignored.
			**map_kwargs: 	Keyword arguments passed to the pointwise function

		Returns:
			tuple (data_map, dim_info)
		'''

		# Ensure the sequence is a list
		if not isinstance(data_keyseq, tuple):
			data_keyseq 	= list(data_keyseq)

		# Log some information
		log.debug("Selecting data from %s with state map shape %s ...", self.classname, self.state_map.shape)
		log.debugv("Infos:\n  select list: %s\n  data_keyseq: %s", select, data_keyseq)

		# Set the mapping function
		if not map_func:
			# None given -> create a passthrough method
			log.debugv("Using passthrough map function ...")
			map_func 		= lambda x: x

		elif isinstance(map_func, (dict, list, tuple)):
			log.debugv("Using %s as mapping function ...", map_func.__class__.__name__)
			# Use the getitem call
			map_func_dict 	= copy.deepcopy(map_func) # To keep in memory
			map_func 		= lambda key: map_func_dict[key]

		elif isinstance(map_func, str):
			# Evaluate it to a function ...
			log.debugv("eval'ing string to generate map_func: %s", map_func)
			map_func 		= eval(map_func)

		# Get a new DVLStateMap object from the selection. This is the target object ...
		dmap 	= self.state_map.select(select, TargetCls=DVLUniverseMap,
		                                dtype=otype)

		# Create an ierator over the data; inform about a common error where the iterator cannot be created
		try:
			it 	= np.nditer(dmap.state_map, flags=['multi_index', 'refs_ok'])
		except ValueError as err:
			if str(err) == "Iteration of zero-sized operands is not enabled":
				msg 	= "{}\n\nThis is probably due to an overspecified selector or a selector that results in a zero-length slice in one or more multiverse dimensions.\nSelect list: \n  {}\n\n{}".format(err, "\n  ".join(["{key:>15s}:  {idx:}".format(**s) for s in select]), self.pspace.get_info_str())
				raise ValueError(msg) from err
			else:
				raise
		else:
			log.debug("Created iterator over selected universes, shape: %s", dmap.state_map.shape)

		log.debug("Iterating over universes, selecting data using keysequence, and applying mapping function ...")
		# Iterate over the target array's state map, read the corresponding data, and put it in the equivalent spot in its data map
		while not it.finished:
			midx 		= it.multi_index
			log.debugv(" %s -> %s", midx, it[0])

			# Build the selector
			sel 		= [int(it[0])] + data_keyseq

			# Get the data at that point and apply the mapping function
			try:
				point 		= map_func(self[sel], **map_kwargs)
			except Exception as err:
				if not deeeval.DEBUG:
					log.caution("Could not retrieve point %s.\n%s: %s", midx, err.__class__.__name__, err)
					point 		= np.nan
				else:
					print("Selector: ", sel)
					print("map_func: ", map_func)
					print("map_kwargs: ", map_kwargs)
					raise

			# Set the entry in the data map
			dmap[midx] 	= point

			# Done with this point, continue
			it.iternext()

		log.debug("Returning %s ...", dmap.classname)
		try:
			log.debug("Shape:  %s", dmap.shape)
			log.debug("dtype:  %s", dmap.dtype)
		except Exception:
			pass

		return dmap

# -----------------------------------------------------------------------------
# Helpers
def _bundle_dim_info(*, spans: list, axis_names: list, info: dict=None):
	'''Bundles the information for the dim_info dict (used in DVLStateMap) together ...'''

	# Shortcut for names
	names 		= axis_names

	# Create mapping from names to axes, spans, info
	axis_by_name= {v: k for k, v in names.items()}
	span_by_name= {names[k]: v for k, v in spans.items()}

	info_by_name= {names[k]: v for k, v in info.items()} if info else None

	# Bundle into dim_info dict
	return dict(spans=spans, span_by_name=span_by_name,
	            axis_names=names, axis_by_name=axis_by_name,
	            info=info, info_by_name=info_by_name)
