''' This module holds the plot functions.

Guidelines:
	- Plot functions should make use of the fact that they are being called by the plot wrapper. If a loop is to be made over a variable that can be configured in e.g. the SelectorSweep wrapper, that it should be implemented like that.
	- If a plot function needs to explicitly require a number of sweeps being open (e.g. to gather that data into a connected line), than that can be done with the function attribute `allowed_num_sweep_dims`, which should be a list that holds the number of open sweep dimensions in the selector. If that argument is given, the wrapper will make sure that the specified number of dimensions is available; it need not be checked again in the plot function.
'''

import copy
import math
from itertools import product
from typing import Union, Sequence

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

from paramspace import ParamSpace

import deval.plot_funcs as dpf
from deval.plot_wrapr import get_cnorm

import deeeval
from deeeval.tools import ensure_num_dims

# Setup logging for this file
log	= deeeval.get_logger(__name__)
cfg = deeeval.get_config(__name__)
log.debug("Loaded module, config, and logger.")

# Try to import seaborn
try:
	import seaborn as sns
except ImportError:
	log.warning("Seaborn could not be imported; some plotting functionality will not be available.")
	sns 	= None
else:
	log.debugv("seaborn imported successfully")

# Local constants


# -----------------------------------------------------------------------------
def plt_sweep_lineplot(wrpr, dm, *, fig, ax, name: str, style: dict, cmap, selector: dict, average_over: Sequence[str]=None, sum_over: Sequence[str]=None, pad_kwargs: dict=None, log_values: bool=False, **select_kwargs):
	'''Expects one sweep dimensions to remain that is to be used to generate a line over that sweep dimension on the x-axis.'''

	def extract_scalar(arr):
		if arr.shape == ():
			# Is already a scalar
			val 	= arr
		elif arr.shape == (1,):
			# Is an array with a single entry
			val 	= arr[0]
			# NOTE this might not occur at all, because squeeze was called
		else:
			raise ValueError("Need single points as result of the data selection, got: "+str(arr)+" of shape "+str(arr.shape))
		return val

	def get_pt_val_and_std(dmap, *, average_over, sum_over) -> tuple:
		if average_over:
			pt_val, pt_std	= dmap.mean_and_std(*average_over, squeeze=True,
			                                    pad_kwargs=pad_kwargs)
		elif sum_over:
			pt_val			= dmap.sum_over(*sum_over, squeeze=True,
			                                pad_kwargs=pad_kwargs)
			pt_std 			= None
		else:
			pt_val  		= dmap.as_ndarray(squeeze=True,
			                                  pad_kwargs=pad_kwargs)
			pt_std 			= None

		# Ensure they are single values
		pt_val	= extract_scalar(pt_val)
		pt_std	= extract_scalar(pt_std) if pt_std is not None else np.nan

		return pt_val, pt_std

	# Lists to fill with the point values extracted from the universe
	pt_vals = []
	pt_stds = []

	# The x values from the param span
	dim_info= wrpr.get_sweep_info(iter_name='pf_iter', dim=0)
	x_vals 	= dim_info['values']

	# It is assured that the selector is a ParamSpace; need not check that here
	# Go over the ParamSpace points and get the corresponding data
	# TODO there has to be a better way. select_data should take care of that!
	for _, _selector in selector.get_points():
		# This is now for a single point of the line
		# Select the correct universe subset
		dmap, _ 		= select_data(dm, _selector, **select_kwargs)

		# Get the point value, either by averaging or directly
		pt_val, pt_std 	= get_pt_val_and_std(dmap,
		                                     average_over=average_over,
		                                     sum_over=sum_over)

		pt_vals.append(pt_val)
		pt_stds.append(pt_std) # NOTE this can also be None

	else:
		log.debug("Collected %d points needed for '%s' line.", len(pt_vals), dim_info['keystr'])
	# Data collected now.

	if log_values:
		log.state("Acuqired lineplot values:\n"\
		          "mean:\n%s\n\nstd:\n%s\n\n"\
		          "%s values:\n%s\n",
		          pt_vals, pt_stds, dim_info['keystr'], x_vals)

	# Parse the plot parameters (returns dict with color, label, style ...)
	plot_params = parse_single_plot_params(wrpr, max_swp_dims=1, style=style,
	                                       plot_style=style.errorbar)

	# Plot the single line as errorbar plot
	plt.errorbar(x_vals, pt_vals, yerr=pt_stds, **plot_params)

	# Done
	return
plt_sweep_lineplot.style 					= cfg.plot_styles.sweep_lineplot
plt_sweep_lineplot.allowed_num_sweep_dims 	= [1]


# -----------------------------------------------------------------------------
def plt_sweep_pxmap(wrpr, dm, *, fig, ax, name: str, style: dict, cmap, selector: dict, cnorm_intv: tuple=None, **select_kwargs):
	'''Plots a 2D pixel map for a 2D ParamSpace selector.

	Args:
		cnorm_intv: 	A tuple of (lower, upper) limits of the normalisation function that is used for creating a custom colormap normalisation. The other initialsation kwargs are the `style`->`cnorm_kwargs`.
	'''

	# It is assured that the selector is a ParamSpace; need not check that here
	log.debug("Preparing sweep_pxmap plot with ParamSpace selector of shape %s ...", selector.shape)

	# The values from the two ParamSpan that correspond to x and y of the pxmap
	xdim_info 	= wrpr.get_sweep_info(iter_name='pf_iter', dim=0)
	ydim_info 	= wrpr.get_sweep_info(iter_name='pf_iter', dim=1)
	x_vals 		= xdim_info['values']
	y_vals 		= ydim_info['values']

	z_data 		= select_scalar_data(dm, selector=selector, **select_kwargs)

	# Prepare normalisation method . . . . . . . . . . . . . . . . . . . . . .
	cnorm_kwargs= copy.deepcopy(style.get('cnorm_kwargs', {}))

	# Have to know the data interval; there might be an argument given, if not, use the one from the style
	if not cnorm_intv:
		cnorm_intv 	= cnorm_kwargs.get('intv', (None, None))

	# Fill None entries with min and max values (ignoring NaN and +/- np.inf)
	intv 		= tuple(cnorm_intv)
	try:
		min_val 	= np.nanmin(z_data[z_data != -np.inf])
		max_val 	= np.nanmax(z_data[z_data != np.inf])
	except ValueError:
		# probably due to zero-size array after masking
		# in that case, all values are invalid anyway, so the normalisation scale does not matter -> set to None, such that mpl.colors.Normalize decides what to do
		min_val 	= max_val 	= None

	if intv[0] is None:
		intv 		= (min_val, intv[1])
	if intv[1] is None:
		intv 		= (intv[0], max_val)

	# Remove the original interval in the kwargs, if there was one set
	cnorm_kwargs.pop('intv', None)

	# Create the normalisation function
	cnorm 		= get_cnorm(intv=intv, cmap=cmap, **cnorm_kwargs)
	log.debug("Created a custom normalisation function with interval %s.",intv)

	# Prepare colors and recangles . . . . . . . . . . . . . . . . . . . . . .
	# Map data to colors
	colors 		= cmap(cnorm(z_data))

	# Create the rectangles
	# Get x and y scales, needed for them
	scales 		= style.get('set_scales', {})
	x_scale 	= scales.get('xscale', 'linear')
	y_scale 	= scales.get('yscale', 'linear')

	# TODO perform check if the scale definitions are consistent with the pxmap?
	# TODO Could set the xscale_ and yscale_kwargs here so that they don't have to be specified

	# Get rectangle sizes
	rects, borders 	= calc_pxmap_rects_and_borders(x_vals=x_vals,
	                                               y_vals=y_vals,
	                                               shape=z_data.shape,
	                                               x_scale=x_scale,
	                                               y_scale=y_scale,
	                                               **style.get('rect_sizes',
	                                                           {}))

	# TODO Extract some style kwargs here already?!

	# Plotting ................................................................
	# Iterate over data and generate the patches
	log.debug("Plotting pxmap ...")

	patches 	= []

	data_it 	= np.nditer(z_data, flags=['multi_index'])
	while not data_it.finished:
		# Individual recangle -> get multi-index
		midx 	= data_it.multi_index
		log.debugv("Plotting point %s", midx)

		# Get data, rectangle, color, state number, ...
		_x, _y 	= x_vals[midx[0]], y_vals[midx[1]]
		_rect 	= rects[midx]

		# Distinguish between masked and unmasked points
		if (hasattr(z_data, 'mask')
		    and ((isinstance(z_data.mask, bool) and z_data.mask is True)
		         or z_data.mask[midx])):
			# Point is masked
			_col 			= style.get('masked_color', 'w')
			rect_kwargs 	= style.get('masked_rect_kwargs', {})
		else:
			# No mask present or not masked
			_col 			= colors[midx]
			rect_kwargs 	= style.get('rect_kwargs', {})

		# Create the rectangle and gather it in the list
		rect 	= Rectangle((_rect[0], _rect[1]),	# Position
		                     _rect[2], _rect[3],	# Width, Height
		                     fc=_col, **rect_kwargs)# Other params
		patches.append(rect)

		if style.get('mark_point', False):
			# Plot a single point at the coordinates to see where the data is
			plt.scatter(_x, _y, **style.get('point_mark_kwargs', {}))

		# Go to next data point
		data_it.iternext()

	# Iteration finished. Add the patches to the axis as PatchCollection
	pc 	= mpl.collections.PatchCollection(patches, cmap=cmap)
	ax.add_collection(pc)

	# Register the patch collection in the wrapper
	if cnorm_intv:
		wrpr.register_pxmap_info(pc=pc, data_vlims=intv, data=z_data)

	# Set the limits to the borders
	log.debug("Setting limits of axis to pxmap borders ...\n  %s", borders)
	ax.set_xlim(borders[0])
	ax.set_ylim(borders[1])

	# Done
	return
plt_sweep_pxmap.style 					= cfg.plot_styles.sweep_pxmap
plt_sweep_pxmap.allowed_num_sweep_dims 	= [2] # TODO 1 dim

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Monitor specific plots ------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

def plt_monitor_lineplot(wrpr, dm, *, fig, ax, name: str, style: dict, cmap, selector: dict, columns: Sequence[int]=None, labels: Sequence[str]=None, **select_kwargs) -> None:
	'''Perform a lineplot on a number of columns of a monitor.

	Requires 2D-data and assumes that the x data consists of just the indices.

	The `columns` and `labels` argument can be used to plot only certain columns with the corresponding labels. The indices of the columns are with respect to the 2D array remaining after the selector was applied. Note that the selector might also reduce the number of columns.'''

	# Can perform the mean over some axes
	# Get the data map using the selector . . . . . . . . . . . . . . . . . . .
	dmap, lines	= select_data(dm, selector,
	                          squeeze=True, ensure_dims=2, # for ndarray
	                          **select_kwargs)

	# Plot column by column . . . . . . . . . . . . . . . . . . . . . . . . . .
	plot_params	= parse_plot_params(wrpr, columns=columns, labels=labels,
	                                max_swp_dims=1, data_shape=lines.shape,
	                                target_dims=1, style=style,
	                                plot_style=style.linestyle)

	# Perform the loop over all columns
	plot_as_lines(lines, plot_params=plot_params)

	return
plt_monitor_lineplot.style 	= cfg.plot_styles.monitor_lineplot

# .............................................................................

def plt_monitor_mean_and_std(wrpr, dm, *, fig, ax, name: str, style: dict, cmap, selector: dict, average_over: Sequence[str], columns: Sequence[int]=None, labels: Sequence[str]=None, allow_axis_idx: bool=False, pad_kwargs=None, **select_kwargs):
	'''Calculates and plots the mean and std over the specified `average_over` of the data resulting from the `selector`.

	Requires 2D-data and assumes that the x data consists of just the indices.

	The `columns` and `labels` argument can be used to plot only certain columns with the corresponding labels. The indices of the columns are with respect to the 2D array remaining after the selector was applied. Note that the selector might also reduce the number of columns.
	'''

	# Get the data map using the selector . . . . . . . . . . . . . . . . . . .
	dmap, _ 	= select_data(dm, selector, **select_kwargs)

	# Get the mean and std
	mean, std 	= dmap.mean_and_std(*average_over, squeeze=True, ensure_dims=2,
	                                allow_axis_idx=allow_axis_idx,
	                                pad_kwargs=pad_kwargs, )

	# Plot column by column . . . . . . . . . . . . . . . . . . . . . . . . . .
	plot_params	= parse_plot_params(wrpr, columns=columns, labels=labels,
	                                max_swp_dims=1, data_shape=mean.shape,
	                                target_dims=1, style=style,
	                                plot_style=style.mean_and_error_bands)
	# return value is (data_sel, labels, colors, styles)

	for col_idx, label, color, plot_style in zip(*plot_params):
		# Select the data
		_mean 	= mean[:, col_idx]
		_std 	= std[:, col_idx]

		# Plot mean and error bands using a helper function
		dpf.mean_and_error_bands(wrpr=wrpr, ax=ax,
		                         mean=_mean,
		                         low= _mean - _std,
		                         high=_mean + _std,
		                         color=color, label=label,
		                         **plot_style)

	return
plt_monitor_mean_and_std.style 		= cfg.plot_styles.monitor_mean_and_std

# .............................................................................

def plt_monitor_mean_and_ensemble(wrpr, dm, *, fig, ax, name: str, style: dict, cmap, selector: dict, ensemble_of: str, columns: Sequence[int]=None, labels: Sequence[str]=None, allow_axis_idx: bool=False, without_mean: bool=False, without_ens_label: bool=True, pad_kwargs=None, **select_kwargs):
	'''Calculates and plots the mean and std over the specified `average_over` of the data resulting from the `selector`.

	Requires 2D-data and assumes that the x data consists of just the indices.

	The `columns` and `labels` argument can be used to plot only certain columns with the corresponding labels. The indices of the columns are with respect to the 2D array remaining after the selector was applied. Note that the selector might also reduce the number of columns.
	'''

	# Get the basic universe data map
	dmap, darr 	= select_data(dm, selector, **select_kwargs)

	# Mean . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	if not without_mean:
		mean, _ 	= dmap.mean_and_std(ensemble_of,
		                                squeeze=True, ensure_dims=2,
		                                allow_axis_idx=allow_axis_idx,
		                                pad_kwargs=pad_kwargs)

		plot_params	= parse_plot_params(wrpr, columns=columns, labels=labels,
		                                max_swp_dims=1, data_shape=mean.shape,
		                                target_dims=1, style=style,
		                                plot_style=style.mean)

		# Perform the loop over all columns of the mean
		plot_as_lines(mean, plot_params=plot_params)
		log.debug("Plotted the mean.")

	else:
		log.debug("Skipped plotting the mean.")

	# Ensemble . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	# Find out the number of values on the ensemble axis
	ens_axis 	= dmap.dim_info['axis_by_name'][ensemble_of]
	ens_size 	= dmap.shape[ens_axis]
	log.debug("Found ensemble of size %d.", ens_size)

	# Loop over the dmap (not including item dimensions)
	for i, sel in enumerate(shape_iterator(dmap.shape, ens_axis)):
		log.debugv("- - - Plotting ensemble member %d ...", i)
		log.debugv("Selector:  %s", sel)

		# Select the data and ensure it is two-dimensional
		ens_data 	= darr[sel]
		ens_data 	= ensure_num_dims(ens_data, num_dims=2, add_axes='post')

		# Parse the plot parameters
		plot_params	= parse_plot_params(wrpr, columns=columns,
		                                labels=None,
		                                without_labels=bool(without_ens_label or (not without_ens_label and i != 0)),
	                                    max_swp_dims=1,
	                                    data_shape=ens_data.shape,
	                                    target_dims=1, style=style,
	                                    plot_style=style.ensemble)
		plot_as_lines(ens_data, plot_params=plot_params)

		log.debugv("- - - Finished ensemble member %d.", i)

	return
plt_monitor_mean_and_ensemble.style	= cfg.plot_styles.monitor_mean_and_ensemble



# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Specific plots --------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

def plt_logistic_funcs(wrpr, dm, *, fig, ax, name: str, style: dict, cmap, mode: str, intv, num_steps: Sequence[int], num_calcs: int, init_val: float=0.5, use_frac: float=0.5, bif_thresh: float=None, no_bif_most_common: int=4, as_collection: bool=True, cnorm_intv: bool=False):
	'''Plots the logistic growth model or the logistic map with a certain resolution...'''

	from collections import Counter

	# Determine some values
	if mode in ['logistic_map', 'logistic map', 'map']:
		log_func 	= lambda mu, old_rho: 	mu * old_rho *(1 - old_rho)

		if bif_thresh is None:
			bif_thresh 	= 3.0

	elif mode in ['logistic_growth', 'logistic growth', 'growth']:
		log_func 	= lambda mu, old_rho:	old_rho + mu*old_rho *(1 - old_rho)

		if bif_thresh is None:
			bif_thresh 	= 2.0

	elif mode in ['beverton-hold']:
		log_func 	= lambda mu, old_rho:	mu*old_rho / (1 + old_rho*(mu-1))

		if bif_thresh is None:
			bif_thresh 	= 2.0

	else:
		raise ValueError("Invalid mode: "+str(mode))

	# Ensure parameters are integer
	if isinstance(num_steps, int):
		num_steps 	= (num_steps//2, num_steps//2+1)
	num_steps 	= (int(num_steps[0]), int(num_steps[1]))
	num_steps_tot=num_steps[0] + num_steps[1]

	num_calcs 	= int(num_calcs)
	use_calcs 	= int(use_frac*num_calcs)

	# Some information
	log.state("Calculating %s ...\n"\
	          "  Steps:         %s -> %d total\n"\
	          "  Calculations:  %s  (last %.2f%% = %d values used)\n"\
	          "  Interval:      (%s, %s]\n"\
	          "  Bif. thresh.:  %s",
	          mode, num_steps, num_steps_tot,
	          num_calcs, use_frac*100, use_calcs,
	          intv[0], intv[1], bif_thresh)

	# Initialise steps for birth rate / growth rate, leaving out the 0 value
	if intv[0] == 0.:
		intv 		= (bif_thresh/num_steps[0], intv[1])

	mu_vals		= np.linspace(intv[0], bif_thresh,
	                          num_steps[0], endpoint=False)
	mu_vals 	= np.hstack((mu_vals,
	                         np.linspace(bif_thresh, intv[1],
	                                     num_steps[1], endpoint=True)))

	# Initialise result containers
	conv_vals 	= []
	num_cvs		= np.zeros((num_steps_tot,), dtype=int) # number of different conv vals

	log.progress("Calculating ...")

	# Perform the calculation at each step ...
	for n, mu in enumerate(mu_vals):
		# Create the calculations array and set initial value
		rho			= np.zeros(num_calcs+1, dtype=float)
		rho[0]		= init_val
		for t in range(num_calcs):
			rho[t+1] 	= log_func(mu, rho[t])

		# Only use the last part of the calculated rho values (first part is for initialisation)
		counts 		= Counter(rho[-use_calcs:]).most_common()
		# Now a sorted list of (value, count) tuples

		# Store data
		if mu < bif_thresh:
			# Before the bifurcation: only use the `no_bif_most_common`
			conv_vals.append(counts[:no_bif_most_common])
		else:
			# After the bifurcation: use all data
			conv_vals.append(counts)

		# Store number of converged values as well
		num_cvs[n] 	= len(counts)

		# Progress
		print("  {:>3.2f}% done.".format((100.*n)/num_steps_tot), end="\r")

	log.progress("Calculation finished.")



	# Plotting
	log.progress("Preparing plotting parameters ...")

	# Get point style and other parameters for aesthetics
	base_radius 	= style.get('base_radius', 0.01)
	min_radius 		= style.get('min_radius', 0.0025)
	ellipse_kwargs	= style.get('ellipse_kwargs', {})
	scatter_kwargs	= style.get('scatter_kwargs', {})


	# Set the radius and mapping function
	radius_func 	= style.get('radius_func')
	map_func 		= style.get('map_func')

	if radius_func in ['lin', 'linear', None]:
		radius_func 	= lambda r0, cfrac: r0 * cfrac

	elif radius_func in ['log', 'logarithmic']:
		radius_func 	= lambda r0, cfrac: r0 * math.log(1+cfrac, 2)

	else:
		log.state("Setting radius function:\n  %s", radius_func)
		radius_func 	= eval(radius_func)


	if map_func is None:
		map_func 		= lambda ncvs: ncvs
	elif map_func in ['log2']:
		map_func 		= lambda ncvs: math.log(ncvs, 2)
	elif map_func in ['log', 'log10']:
		map_func 		= lambda ncvs: math.log(ncvs, 10)
	else:
		log.state("Setting mapping function:\n  %s", map_func)
		map_func		= eval(map_func)

	# Create the normalisation function
	log.debug("Creating cnorm from `cnorm_kwargs` ...")
	if not cnorm_intv or cnorm_intv in ['within_cmap', 'do_not_extend']:
		cnorm_intv 		= (map_func(1), map_func(use_calcs))
	elif cnorm_intv in ['extend']:
		cnorm_intv 		= (map_func(2), map_func(use_calcs-1))
	else:
		log.state("Given cnorm_intv:  %s", cnorm_intv)
		cnorm_intv 		= (map_func(cnorm_intv[0]),
		                   map_func(cnorm_intv[1]))
	log.state("cnorm_intv after mapping:  %s", cnorm_intv)

	cnorm 			= get_cnorm(intv=cnorm_intv,
	                            cmap=cmap, **style.get('cnorm_kwargs', {}))
	wrpr['cnorm'] 	= cnorm # so that the colorbar can be added

	# Generate the lists that will be used for the Ellipse collection
	colors 			= []
	radii 			= []
	x_vals 			= []
	y_vals 			= []

	log.progress("Preparing points ...")

	# Fill the lists
	for n, (mu, cvs, ncvs) in enumerate(zip(mu_vals, conv_vals, num_cvs)):
		# Go over the value and the corresponding counts for each entry
		for val, count in cvs:
			# Calculate the fraction of counts that ended in this value
			cfrac 		= count/use_calcs

			# Calculate normalised color value and radius
			radius 		= radius_func(base_radius, cfrac)
			cval 		= cnorm(map_func(ncvs))

			# Create the color, radius, and offset values
			x_vals.append(mu)
			y_vals.append(val)
			radii.append(max(min_radius, radius))
			colors.append(cmap(cval))

		# Progress
		print("  {:>3.2f}% done.".format((100.*n)/num_steps_tot), end="\r")

	if as_collection:
		log.progress("Creating collection ...")

		# Create and add the collection
		coll = mpl.collections.EllipseCollection(radii, radii, # width/height
		                                         np.zeros_like(radii), # angle
		                                         offsets=list(zip(x_vals,
		                                                          y_vals)),
		                                         facecolors=colors,
		                                         # to have circles:
		                                         transOffset=ax.transData,
		                                         units='x',
		                                         zorder=100,
		                                         **ellipse_kwargs)
		wrpr.ax.add_collection(coll)

	else:
		log.progress("Creating scatter plot ...")
		plt.scatter(x_vals, y_vals, s=radii, c=colors,
		            marker='.', zorder=100, **scatter_kwargs)

	log.progress("Plotting finished.")
plt_logistic_funcs.style 	= cfg.plot_styles.logistic_funcs

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Other plots -----------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

def plt_equation_plot(wrpr, dm, *, fig, ax, name: str, style: dict, cmap, eq_params: dict):
	'''Plots an equation defined in dm['equations']'''
	log.progress("Plotting '%s' equation ...", name)

	log.state("Equation:\n  %s", eq_params['eq'])

	# Initialise lambda equation and vectorise it
	eq_func	= np.vectorize(eval(eq_params['eq']))

	# Determine number of variables
	num_vars= eq_params['num_vars']
	if num_vars == 1:
		x_vals	= eval(eq_params['x_vals'])
	else:
		x_vals 	= [eval(xv) for xv in eq_params['x_vals']]

	consts 	= eq_params['constants']
	lines 	= eq_params['lines']
	labels 	= eq_params.get('labels', [None for _ in lines])

	log.state("Constants:  %s", consts)
	log.state("Lines and labels:\n  %s",
	          "\n  ".join(["{label:>15s}   {params:}".format(label=l, params=p)
	                       for l, p in zip(labels, lines)]))

	log.state("Cycler:     %s", style.get('cycler'))
	cycler 	= prepare_cycler(style.get('cycler'), n_colors=len(lines))

	log.progress("Plotting %d lines ...", len(lines))
	for _vars, _label, _color  in zip(lines, labels, cycler):
		_func_kwargs 	= copy.deepcopy(consts)
		_func_kwargs.update(_vars)

		plt.plot(x_vals, eq_func(x_vals, **_func_kwargs),
		         label=_label, color=_color, **style.linestyle)
plt_equation_plot.style 	= cfg.plot_styles.equation_plot

def plt_recurrence_relation(wrpr, dm, *, fig, ax, name: str, style: dict, cmap, func_str : str, num_steps: int, init_vals: list, constants: dict=None, lines: list=None, labels: list=None, linekwargs: list=None):
	'''Plots the recurrence relation given by the lambda function that is defined by the `func_str` argument.'''
	constants 	= constants if constants else {}
	lines 		= lines if lines else [{} for _ in init_vals]
	labels 		= labels if labels else [None for _ in init_vals]
	lkws 		= linekwargs if linekwargs else [{} for _ in init_vals]

	if len(lines) != len(init_vals) or len(labels) != len(init_vals) or len(lkws) != len(init_vals):
		raise ValueError("Need to supply list of the same length for arguments `init_vals`, `lines`, `labels`, and `linekwargs`.")

	# Create the function
	log.state("Creating recurrence relation from:\n  %s", func_str)
	rec_func 	= eval(func_str)

	log.state("Constants:  %s", constants)
	log.state("Lines and labels:\n  %s",
	          "\n  ".join(["{label:>15s}   {params:}".format(label=l, params=p)
	                       for l, p in zip(labels, lines)]))

	log.state("Cycler:     %s", style.get('cycler'))
	cycler 	= prepare_cycler(style.get('cycler'), n_colors=len(lines))

	for init_val, line, label, color, lkw in zip(init_vals, lines, labels, cycler, lkws):
		# Create the empty value array
		vals 		= np.zeros((num_steps,), dtype=float)
		vals[0] 	= init_val

		# Perform the iteration
		for i in range(1, num_steps):
			vals[i] 	= rec_func(vals[i-1], **line, **constants)

		# Plot the result
		plt.plot(vals, label=label, color=color, **lkw, **style.linestyle)
	else:
		log.debug("Finished plotting %d recurrence relation(s).",
		          len(init_vals))

plt_recurrence_relation.style 	= cfg.plot_styles.recurrence_relation

def plt_data_output(wrpr, dm, *, fig, ax, name: str, style: dict, cmap, selector: dict, to_stdout: bool=True):
	'''Applies the selector and, depending on the config, performs a data output. Is not actually a plotting function!'''
	dmap, darr 	= select_data(dm, selector)

	if to_stdout:
		deeeval.deval.clear_line()
		print(darr)
		print("shape: ", darr.shape)
	return

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Helpers ---------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Data selection helpers

def select_data(dm, selector: dict, map_func=None, map_kwargs=None, otype=None, **as_ndarray_kwargs) -> tuple:
	'''Returns the data necessary for a monitor plot as a (dmap, darr) tuple.'''
	log.debug("Selecting data ...")

	log.debugv("Selector type:  %s", type(selector))
	log.debugv("Selector:\n%s", deeeval.PP(selector))

	dmap 	= dm['universes'].select_data(**selector, otype=otype,
	                                      map_func=map_func,
	                                      **(map_kwargs if map_kwargs else {}))

	# Get the data array
	darr 	= dmap.as_ndarray(**as_ndarray_kwargs)

	return dmap, darr
# TODO This should take care of type ParamSpace selectors as well!

def select_scalar_data(dm, *, selector: dict, axis_operations: list=None, otype=float, ax_op_kwargs: dict=None, **select_kwargs) -> np.ndarray:
	'''Returns an ndarray of data from the given data manager. Note that the array will have the same shape as the selector; it is not possible to store non-scalars!'''

	def extract_scalar(arr):
		if arr.shape == ():
			# Is already a scalar
			val 	= arr
		elif arr.shape == (1,):
			# Is an array with a single entry
			val 	= arr[0]
			# NOTE this might not occur at all, because squeeze was called
		else:
			# Try again after squeezing
			try:
				val 	= extract_scalar(np.squeeze(arr))
			except:
				raise ValueError("Need single points as result of the data selection, got: "+str(arr)+" of shape "+str(arr.shape))
		return val

	if not isinstance(selector, ParamSpace):
		raise NotImplementedError("Need type ParamSpace selector for `select_scalar_data`.")

	if not axis_operations:
		axis_operations 	= []

	# Prepare the output array, same shape as pspace
	darr 	= np.ma.MaskedArray(data=np.zeros(selector.shape, dtype=otype),
	                            mask=True)

	# Loop over the points in the selector, extract and save each point
	for state, _selector in selector.get_points(with_span_states=True):
		# This is now for a single point - >Select the correct universe subset
		dmap, _		= select_data(dm, _selector, otype=otype, **select_kwargs)

		# Get the point value, applying the axes operations
		val 		= dmap.axis_operations(*axis_operations,
		                                   otype=otype,
		                                   squeeze=True,
		                                   **(ax_op_kwargs if ax_op_kwargs
		                                      else {}))

		# If the values are available, set them; unmasks the points
		if val is not None:
			darr[state] = extract_scalar(val)

	else:
		log.debug("Collected %d points as defined by ParamSpace-selector.",
		           darr.size)

	return darr

# .............................................................................

def is_within_handled_pf_sweep(wrpr, *, max_dims=None) -> bool:
	'''Returns true, if a certain plot function call is target of a call within a plot function sweep.'''
	if not hasattr(wrpr, 'get_sweep_info'):
		# Class does not support sweeps
		return False

	state 	= wrpr.pf_iter_state

	if max_dims is not None and state is not None:
		if len(state) > max_dims:
			raise ValueError("The plot function iterator state has more dimensions ({}) than allow by this plotting function ({}).".format(len(state), max_dims))

	return bool(state is not None)

def shape_iterator(shape, *axes) -> tuple:
	'''Returns an iterator over the shape along the specified axes; the resulting tuples can be used as selectors.'''
	spans 	= [range(shape[i]) for i in axes]
	shpit 	= product(*spans)

	for span_vals in shpit:
		# Put into a dict to be accessible by axis number
		sv 		= {k: v for k, v in zip(axes, span_vals)}
		# Put the span values into the original shape
		sel 	= [shape[i]-1 if i not in axes else sv[i]
		           for i,_ in enumerate(shape)]
		yield tuple(sel)
	return

# -----------------------------------------------------------------------------
# Parsing helpers
# TODO clean up the parsers

def parse_single_plot_params(wrpr, *, max_swp_dims: int, style: dict, plot_style: dict, pf_iter_dim: int=0, without_label: bool=False) -> dict:
	'''Parses the parameter necessary for a single plot operation inside a plot function sweep.'''
	# Take care of the style dictionaries
	style 		= copy.deepcopy(style)
	plot_style	= copy.deepcopy(plot_style)

	# Extract cycler and format string for labels
	cycler 		= style.pop('cycler', None)
	val2label_fstr = style.pop('val2label_fstr', None)

	# Check whether this is a plot function iteration
	# If yes, need to adjust labels and colors
	if is_within_handled_pf_sweep(wrpr, max_dims=max_swp_dims):
		# Get the info on the handled plot function iteration
		iter_info 	= wrpr.get_sweep_info(iter_name='pf_handled', dim=pf_iter_dim)
		log.debugv("Got plot func iterator info:\n%s", iter_info)

		# Use the supplied values to set the label and the color index
		log.debug("Extracting labels and color cycler index from plot function sweep iterator ...")

		# Use the given label
		if val2label_fstr:
			this_label 	= val2label_fstr.format(val=iter_info['value'])
		else:
			this_label 	= iter_info['value']

		# Prepare the cycler using the number of entries in the sweep
		cycler 		= prepare_cycler(cycler, n_colors=iter_info['num_values'])

		# All lines have to have the same color in orer to not clash with other plot function iterations
		this_color 	= cycler[iter_info['idx']]

	else:
		# Use the default label
		this_label 	= plot_style.pop('label', None)

		# Have no information on the index, thus cannot set a color either. By setting it to None, the defaults are being used
		this_color 	= None

	if without_label:
		this_label 	= None

	# Some information
	log.debug("Label:      %s", this_label)
	log.debug("Color:      %s", this_color)

	# Return them all as a tuple
	return dict(label=this_label, color=this_color, **plot_style)

def parse_plot_params(wrpr, *, columns: list, labels: list, max_swp_dims: int,  data_shape: tuple, target_dims: int, style: dict, plot_style: dict, pf_iter_dim: int=0, without_labels: Union[bool, str]=False) -> tuple:

	log.debug("Parsing plot parameters ...")

	if target_dims != 1:
		raise NotImplementedError("Only generalised for one dimension.")

	# Explicitly determine a list of column numbers
	if not columns:
		columns 	= list(range(data_shape[-1]))
		log.debugv("Deducing columns from data shape:  %s", columns)

	# Find out the number of columns by looking at the shape of mean
	num_cols  	= len(columns)

	# Take care of the style dictionaries
	style 		= copy.deepcopy(style)
	plot_style	= copy.deepcopy(plot_style)

	# Extract cycler and format string for labels
	cycler 		= style.pop('cycler', None)
	val2label_fstr = style.pop('val2label_fstr', None)

	# Remove the label from the plot_style
	def_label 	= plot_style.pop('label', None) # TODO should this be in style?

	# Make sure they are set
	if labels is None:
		labels 		= [def_label for _ in range(num_cols)]
	else:
		log.debugv("Using provided labels ...")

	# Check whether this is a plot function iteration
	# If yes, need to adjust labels and colors
	if is_within_handled_pf_sweep(wrpr, max_dims=max_swp_dims):

		if num_cols > 1:
			log.caution("Plotting sweep with two-dimensional data. All columns will have the same color and label.")
			# TODO could potentially have more dimensions (e.g. that of the multiple columns) by cycling through more properties ...

		# Get the info on the handled plot function iteration
		iter_info 	= wrpr.get_sweep_info(iter_name='pf_handled', dim=pf_iter_dim)
		log.debugv("Got plot func iterator info:\n%s", iter_info)

		# Use the supplied values to set the label and the color index
		log.debug("Extracting labels and color cycler index from plot function sweep iterator ...")

		# Use the given label, extending it to the number of columns
		if val2label_fstr:
			this_label 	= val2label_fstr.format(val=iter_info['value'])
		else:
			this_label 	= iter_info['value']
		labels 		= [this_label for _ in range(num_cols)]

		# Prepare the cycler using the number of entries in the sweep
		cycler 		= prepare_cycler(cycler, n_colors=iter_info['num_values'],
		                             wrpr=wrpr)
		log.debugv("Received cycler of length %d:\n  %s\nWill select the color at index %d.", len(cycler), cycler, iter_info['idx'])

		# All lines have to have the same color in orer to not clash with other plot function iterations
		this_color 	= cycler[iter_info['idx']]
		colors 		= [this_color for _ in range(num_cols)]

	else:
		# Labels stay as they are

		# Prepare the cycler using the number of columns
		cycler 		= prepare_cycler(cycler, n_colors=num_cols, wrpr=wrpr)

		# Resolve the colors in the trivial way
		colors 		= cycler[:num_cols]

	# Map to the desired return value names
	# Selector list is just the column indices
	data_sel 	= columns

	# Create the plot style list
	plot_styles	= [copy.deepcopy(plot_style) for _ in range(num_cols)]
	# TODO could potentially do something more with this

	if without_labels is True:
		log.debugv("Dropping labels.")
		labels 		= [None for _ in labels]
	elif without_labels in ['only_first', 'first_only']:
		log.debugv("Dropping all but the first label.")
		labels 		= labels[0:1] + [None for _ in labels[1:]]

	# Some information
	log.debug("Data shape:  %s", data_shape)
	log.debug("Selectors:   %s", data_sel)
	log.debug("Labels:      %s", labels)
	log.debugv("Colors:   %s", colors)

	# Return them all as a tuple
	return data_sel, labels, colors, plot_styles

# .............................................................................

def prepare_cycler(cycler: Union[str, Sequence[str]], *, n_colors: int=None, wrpr=None) -> list:
	'''Prepares the color cycle list.'''
	if isinstance(cycler, str):
		cycler 	= _get_sns_color_palette(cycler, n_colors=n_colors)

	elif isinstance(cycler, (list, tuple)):
		# Is already a list. Assume it to be a list of colours -> keep as it is
		pass

	elif isinstance(cycler, dict):
		name 		= cycler['name']

		# The shift rolls the array forward by that many entries.
		# The offset additionally extends the number of colours.
		offset 		= cycler.get('offset', 0)
		shift 		= cycler.get('shift', 0)
		add_idx_to_shift = cycler.get('add_idx_to_shift', False)

		# Get number of colour, defaulting to the calcluated value
		n_colors 	= cycler.get('n_colors', n_colors + offset)

		# The dict to extract information from
		use_swp_info= cycler.get('use_sweep_info')

		log.debug("Resolving cycler '%s' from given parameters.\n"\
		          "  n_colors:       %s\n"\
		          "  offset:         %s\n"\
		          "  shift:          %s\n"\
		          "  use_sweep_info: %s",
		          name, n_colors, offset, shift, use_swp_info)

		if use_swp_info:
			# There were parameters passed to determine the offset and n_colors

			# Some checks
			if not wrpr:
				raise ValueError("Need wrapper passed to `prepare_cycler` in order to determine the index...")
			elif 'dim' not in use_swp_info:
				raise ValueError("Need to choose `dim` key in `use_swp_info` dict; got: "+str(use_swp_info))

			# Get the corresponding info
			info 		= wrpr.get_sweep_info(**use_swp_info)
			log.debugv("Got sweep info:\n  %s", info)

			# Determine number of colours
			n_colors 	= info['num_values'] + offset

			if add_idx_to_shift:
				log.debugv("Adding index (%d) to current shift value (%d)...", info['idx'], shift)
				shift 		+= info['idx']

			log.debugv("Determined shift (%d) and n_colors (%d incl. %d offset) from sweep info.", shift, n_colors, offset)

		# Get cycler
		cycler 		= _get_sns_color_palette(name, n_colors=n_colors)

		# Apply shift and offset
		if shift or offset:
			roll_by 	= (shift + offset) % n_colors
			cycler 		= cycler[roll_by:] + cycler[:roll_by]
			log.debugv("Rolled cycler by %d = %d offset + %d shift.", roll_by, offset, shift)

	else:
		# Default tab 10 cycler
		cycler 	= ["C"+str(n%10) for n in range(n_colors)]

	return cycler

def _get_sns_color_palette(name: str, *, n_colors: int=None):
	'''Tries to import a color palette from seaborn.'''
	if sns:
		cycler 		= sns.color_palette(palette=name, n_colors=n_colors)
		log.debug("Using seaborn color palette '%s' with %d colors.",
		          name, n_colors)
		return cycler

	else:
		log.warning("Wanted to use seaborn color palette, but seaborn could not be imported")
		return None

# -----------------------------------------------------------------------------
# Plotting helpers

def plot_as_lines(data, *, plot_params): # TODO generalise this?
	'''Plots the given 1D or 2D data as lines.'''
	# Ensure that it is two dimensional
	data 	= ensure_num_dims(data, num_dims=2, add_axes='raise')

	# Perform the loop over all columns
	for col_idx, label, color, plot_style in zip(*plot_params):
		# Select the data
		_data 	= data[:, col_idx]

		# Plot the line
		plt.plot(_data, label=label, color=color, **plot_style)

def calc_pxmap_rects_and_borders(*, x_vals, y_vals, shape: tuple, x_scale: str, y_scale: str, size_factor=1.0, extend=False, margin=0):
	''' Calculates the coordinates and sizes of rectangles which lie at the points in sequences x_vals and y_vals and have the dimensions such that they span half the space between them and the next point, scaled by size_factor. Additionally, the borders of the whole rectangle grid are given, which can be used to set the axes limits...

	Note that they might not lie centered at the x_vals and y_vals positions.
	'''
	log.debug("Calculating rectangles ...")

	# Define some allowed linear and logarithmic scales
	LIN_SCALES 				= ['lin', 'linear']
	LOG_SCALES 				= ['log', 'symlog']
	CATEGORIAL_SCALES		= ['cat', 'categorical', 'cat_at_value']

	# Helper functions
	# Have four explicit methods here as it is very tedious to build a rectangle specifier ...
	def calc_linlin_rect(x, y, x_size, y_size):
		r = ( x			- x_size[0], 	# x pos. bottom l.h. corner
			  y			- y_size[0],	# y pos. bottom l.h. corner
			  x_size[0] + x_size[1],	# x length
			  y_size[0] + y_size[1]) 	# y length
		return r

	def calc_linlog_rect(x, y, x_size, y_size):
		r = ( x			- x_size[0],
			  np.exp(np.log(y) - y_size[0]),
			  x_size[0] + x_size[1],
			  np.exp(np.log(y) + y_size[1]) - np.exp(np.log(y) - y_size[0]))
		return r

	def calc_loglin_rect(x, y, x_size, y_size):
		r = ( np.exp(np.log(x) - x_size[0]),
			  y			- y_size[0],
			  np.exp(np.log(x) + x_size[1]) - np.exp(np.log(x) - x_size[0]),
			  y_size[0] + y_size[1])
		return r

	def calc_loglog_rect(x, y, x_size, y_size):
		r = ( np.exp(np.log(x) - x_size[0]),
			  np.exp(np.log(y) - y_size[0]),
			  np.exp(np.log(x) + x_size[1]) - np.exp(np.log(x) - x_size[0]),
			  np.exp(np.log(y) + y_size[1]) - np.exp(np.log(y) - y_size[0]))
		return r


	# Check if there was any data (<= because diffs need to be calculated)
	if len(x_vals) <= 1 or len(y_vals) <= 1:
		return None

	# Calculate distances, distinguishing between linear and logarithmic scale
	if x_scale in LIN_SCALES:
		x_diffs 	= np.diff(x_vals)
	elif x_scale in LOG_SCALES: # work in logarithmic space
		x_diffs 	= np.diff(np.log(x_vals))
	elif x_scale in CATEGORIAL_SCALES:
		if x_scale != 'cat_at_value':
			# Change the data to just be with evenly spaced integers
			x_vals 		= list(range(len(x_vals)))
		x_diffs 	= np.diff(x_vals)
		x_scale 	= 'lin'
	else:
		raise ValueError("Invalid x_scale: "+str(x_scale)+"\nExpected one of: ", LIN_SCALES, LOG_SCALES, CATEGORIAL_SCALES)

	if y_scale in LIN_SCALES:
		y_diffs 	= np.diff(y_vals)
	elif y_scale in LOG_SCALES: # work in logarithmic space
		y_diffs 	= np.diff(np.log(y_vals))
	elif y_scale in CATEGORIAL_SCALES:
		if y_scale != 'cat_at_value':
			# Change the data to just be with evenly spaced integers
			y_vals 		= list(range(len(y_vals)))
		y_diffs 	= np.diff(y_vals)
		y_scale 	= 'lin'
	else:
		raise ValueError("Invalid y_scale: "+str(y_scale)+"\nExpected one of: ", LIN_SCALES, LOG_SCALES, CATEGORIAL_SCALES)

	# Determine rectangle calculation method
	if x_scale in LIN_SCALES and y_scale in LIN_SCALES:
		calc_rect 	= calc_linlin_rect
	elif x_scale in LIN_SCALES and y_scale in LOG_SCALES:
		calc_rect 	= calc_linlog_rect
	elif x_scale in LOG_SCALES and y_scale in LIN_SCALES:
		calc_rect 	= calc_loglin_rect
	elif x_scale in LOG_SCALES and y_scale in LOG_SCALES:
		calc_rect 	= calc_loglog_rect

	# Calculate the distance between two points to use in the calculation of the rectangle sizes
	if extend:
		# Add the first item to the beginning and the last to the end of the diffs sequences to account for the borders
		x_diffs 	= [x_diffs[0]] + list(x_diffs) + [x_diffs[-1]]
		y_diffs 	= [y_diffs[0]] + list(y_diffs) + [y_diffs[-1]]

	else:
		# No extension of borders desired --> add zeros
		x_diffs 	= [0] + list(x_diffs) + [0]
		y_diffs 	= [0] + list(y_diffs) + [0]

	# Divide by two to have the sizes of the rectangle, not the distance between the points
	x_sizes 	= np.array(x_diffs) / 2
	y_sizes 	= np.array(y_diffs) / 2

	# TODO Apply a margin ...?
	if margin != 0:
		raise NotImplementedError("calculation of margins not supported yet.")

	# Scale these, if size_factor factor is different than 1
	if size_factor != 1.0:
		x_sizes *= size_factor
		y_sizes *= size_factor

	# Create rects variable, which has the same shape as the data
	dtype 	= [	('xpos', float), 	('ypos', float),
				('xlen', float), 	('ylen', float)]
	rects 	= np.zeros(shape, dtype=dtype)
	# TODO use the access via the structured array to write data?!

	# Now go through the data, find corresponding x and y sizes and write the rectangle specifications to the grid
	it 	= np.nditer(rects, flags=['multi_index'], op_flags=['readwrite'])
	while not it.finished:
		# Get the index of this point
		x_idx, y_idx 	= it.multi_index

		# Get the value of the corresponding points on the spans
		_x 		= float(x_vals[x_idx])
		_y 		= float(y_vals[y_idx])

		# Get the sizes calculated above
		x_size 	= (x_sizes[x_idx], x_sizes[x_idx + 1]) 	# (left, right)
		y_size 	= (y_sizes[y_idx], y_sizes[y_idx + 1]) 	# (bottom, top)

		# Create rect specifications and save to position in rects ndarray
		it[0] 	= calc_rect(_x, _y, x_size, y_size)

		# Go to the next point
		it.iternext()

	# Calculate borders of the whole grid
	if x_scale in LIN_SCALES:
		x_lims 	= (	float(x_vals[0])  - x_sizes[0],
		           	float(x_vals[-1]) + x_sizes[-1])
	elif x_scale in LOG_SCALES:
		x_lims 	= (	np.exp(np.log(float(x_vals[0])) - x_sizes[0]),
		           	np.exp(np.log(float(x_vals[-1])) + x_sizes[-1]))

	if y_scale in LIN_SCALES:
		y_lims 	= (	float(y_vals[0])  - y_sizes[0],
		           	float(y_vals[-1]) + y_sizes[-1])
	elif y_scale in LOG_SCALES:
		y_lims 	= (	np.exp(np.log(float(y_vals[0])) - y_sizes[0]),
		           	np.exp(np.log(float(y_vals[-1])) + y_sizes[-1]))

	borders = (x_lims, y_lims)

	# All done
	return rects, borders
