#! /bin/bash
# This script builds the base image for deeevoLab
# It assumes that docker daemon is running and logged into blsqr cloud account

IMG_TAG=deeevolab_base
IMG_REMOTE_TAG=blsqr/deeevolab_base
IMG_VERSION=1.0

# Build the image and add the local tags
docker build -t "${IMG_TAG}:v${IMG_VERSION}" -t "${IMG_TAG}:latest" .

if [ ! $? -eq 0 ]; then
	echo "Building failed!"
    exit 1
fi

# Tag it such that it can be pushed to the remote
docker tag "${IMG_TAG}:latest" "${IMG_REMOTE_TAG}:latest"
docker tag "${IMG_TAG}:v${IMG_VERSION}" "${IMG_REMOTE_TAG}:v${IMG_VERSION}"

# Push the remote tags
docker push "${IMG_REMOTE_TAG}:latest"
docker push "${IMG_REMOTE_TAG}:v${IMG_VERSION}"
