#! /bin/bash
# This script builds the base image for deeevoLab
# It assumes that docker daemon is running and logged into blsqr cloud account

IMG_TAG=deeevolab
IMG_REMOTE_TAG=blsqr/deeevolab
IMG_VERSION=0.6
DEPLOY_KEY_FILE=~/.ssh/deeevolab_deploy_key

# Get the deploy key
DEPLOY_KEY=`cat ${DEPLOY_KEY_FILE}`

# Build the image and add the local tags
docker build \
	-t "${IMG_TAG}:v${IMG_VERSION}" -t "${IMG_TAG}:latest" \
	--no-cache \
	--build-arg SSH_PRIVATE_KEY="${DEPLOY_KEY}" \
	.
# TODO remove the --no-cache flag once releases of the full version are stable

if [ ! $? -eq 0 ]; then
	echo "Building failed!"
    exit 1
fi

# Tag it such that it can be pushed to the remote
docker tag "${IMG_TAG}:latest" "${IMG_REMOTE_TAG}:latest"
docker tag "${IMG_TAG}:v${IMG_VERSION}" "${IMG_REMOTE_TAG}:v${IMG_VERSION}"

# Push the remote tags
docker push "${IMG_REMOTE_TAG}:latest"
docker push "${IMG_REMOTE_TAG}:v${IMG_VERSION}"
