"""For importing and parsing config files."""

import os
import re
import collections
import yaml
import pkg_resources

import numpy as np
from easydict import EasyDict as DotDict # Dot notation dictionary

from paramspace import ParamSpace, ParamDim, CoupledParamDim
import paramspace.yaml_constructors as psp_constr

import deeevolab
from deeevolab.logging import setup_logger
from deeevolab.rng import RNG, ListGen

# Setup logging for this file (inherits from root logger)
log	= setup_logger(__name__)
log.debug("Loaded module and logger.")

# NOTE that there is no config variable available in this module.

# Local constants

# -----------------------------------------------------------------------------
# yaml constructors

# Add custom YAML-constructors
# ...for simple math expressions
def _expr_constructor(loader, node):
	"""pyyaml constructor for evaluating strings with simple mathematical expressions.

	Supports: +, -, *, /, e-X, eX
	"""

	log.debug("Encountered !expr tag. Constructing scalar ...")

	# get expression string
	expr_str 	= loader.construct_scalar(node)

	log.debugv("expr_str:  %s", expr_str)

	# Remove spaces
	expr_str 	= expr_str.replace(" ", "")

	# Parse some special strings
	if expr_str in ['np.nan', 'nan', 'NaN']:
		log.debugv("Returning np.nan")
		return np.nan

	elif expr_str in ['np.inf', 'inf', 'INF']:
		log.debugv("Returning np.inf")
		return np.inf

	elif expr_str in ['-np.inf', '-inf', '-INF']:
		log.debugv("Returning -np.inf")
		return -np.inf

	# remove everything that might cause trouble -- only allow digits, dot, +, -, *, /, and eE to allow for writing exponentials
	expr_str 	= re.sub(r'[^0-9eE\-.+\*\/]', '', expr_str)

	# Try to eval
	log.debugv("Returning eval(%s)", expr_str)
	return eval(expr_str)
yaml.add_constructor(u'!expr', _expr_constructor)

# ...for ParamSpace and ParamDim
# TODO use the constructors defined in paramspace package
yaml.add_constructor(u'!pspace', psp_constr.pspace_sorted)
yaml.add_constructor(u'!pdim', psp_constr.pdim_enabled_only)
yaml.add_constructor(u'!pdim-default', psp_constr.pdim_get_default)
yaml.add_constructor(u'!coupled-pdim', psp_constr.coupled_pdim_enabled_only)


# ...for Random Number Generator
def _rng_constructor(loader, node):
	"""pyyaml constructor for loading a RNG object."""
	if isinstance(node, yaml.nodes.MappingNode):
		log.debug("Encountered !rng tag. Constructing mapping ...")
		rng = RNG(**loader.construct_mapping(node, deep=True))

	else:
		raise TypeError("!rng node needs to be a mapping, was of type '{}' with value:\n{}".format(type(node), node))

	if rng.enabled:
		return rng
	else:
		log.debug("RNG not enabled -- returning default value.")
		return rng.default
yaml.add_constructor(u'!RNG', _rng_constructor)

def _rng_default_constructor(loader, node):
	"""pyyaml constructor for loading a RNG object and then returning the default value; i.e.: always default."""
	if isinstance(node, yaml.nodes.MappingNode):
		log.debug("Encountered !RNG-default tag. Constructing mapping ...")
		rng = RNG(**loader.construct_mapping(node, deep=True))

	else:
		raise TypeError("!RNG-default node needs to be a mapping, was of type '{}' with value:\n{}".format(type(node), node))

	log.debug("Returning default value.")
	return rng.default
yaml.add_constructor(u'!RNG-default', _rng_default_constructor)


# ...for generating lists from parameters
def _listgen_constructor(loader, node):
	"""pyyaml constructor for creating a list in place of the tagged node using the paramters given by the mapping at that node."""
	if isinstance(node, yaml.nodes.MappingNode):
		log.debug("Encountered !listgen tag. Constructing mapping ...")
		params 	= loader.construct_mapping(node, deep=True)

		if params.pop('enabled', True):
			resolve 	= params.pop('resolve', False)

			log.debug("Generating list with params %s", params)
			lg = ListGen(**params)

			if resolve:
				return lg.resolve_completely()
			else:
				return lg
		else:
			return []

	else:
		raise TypeError("!listgen node needs to be a mapping, was of type '{}' with value:\n{}".format(type(node), node))
yaml.add_constructor(u'!listgen', _listgen_constructor)


# -----------------------------------------------------------------------------
# Loading config files

def get_config(modstr: str, ConfigCls: dict=DotDict) -> dict:
	"""Loads a yaml-formatted configuration file ... [parse] ... and returns the content as a dict-like object, corresponding to the given modstr.
	
	TODO finish writing docstring
	
	Args:
	    modstr (str): dot-separated module string for the current module for which the config should be loaded
	    ConfigCls (dict, optional): the dict-like class that should be used for the returned config
	
	Returns:
	    config (dict): the parsed config file as a dict-like object. More specifically, this will be a ConfigCls object
	"""
	log.info("Loading config for module '%s' ...", modstr)

	# Extract the package name and the module from the modstr
	pkg_name = modstr.split('.')[0]
	module = ".".join(modstr.split('.')[1:])

	# Determine the package resource corresponding to this package
	cfg_file = pkg_resources.resource_filename(pkg_name, pkg_name + "_cfg.yml")

	# Load the file
	cfg_dict = load_cfg_file(cfg_file)

	# TODO this could/should be cached!

	# Extract the corresponding entry and return as ConfigCls object
	log.debug("Returning config for module '%s' as %s ...",
	          modstr, ConfigCls.__name__)
	return ConfigCls(cfg_dict[module])

def load_cfg_file(fpath: str, pop_shortcuts: bool=True) -> dict:
	"""Loads and returns the config file at path fpath. It is intentional, that it will raise an error if the file is not present."""
	log.debug("Loading config file '%s' ...", fpath)

	# Warn, if the file is not present
	if not os.path.isfile(fpath):
		raise FileNotFoundError("Cannot find config file at {}!".format(fpath))

	with open(fpath, 'r', encoding='utf-8') as f:
		try:
			cfg = yaml.load(f.read())
		except Exception as err:
			log.error("Exception while loading config file %s: %s", fpath, err)
			raise
		
	log.debug("Successfully loaded '%s'.", fpath)

	if cfg is None:
		log.debug("Loaded file was empty; creating an empty dict.")
		cfg = {}

	if pop_shortcuts:
		log.debug("Deleting all entries starting with '.' ...")
		# Go over top level and pop all names that start with a dot
		cfg = {k:v for k,v in cfg.items() if k[0] != "."}

		log.debug("Remaining top level keys in config:\n  %s", cfg.keys())

	log.debug("Returning loaded config file ...")
	return cfg
