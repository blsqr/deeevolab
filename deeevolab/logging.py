"""This module supplies the logging functionality for deeevolab"""

import os
import sys
import yaml
import logging
import logging.config
import pkg_resources

import colorlog

# NOTE: importing deeevolab here would be a circular import

# Setup logging for this file (inherits from root logger)
log	= logging.getLogger(__name__)
log.debug("Loaded module and logger.")

# Local constants

# -----------------------------------------------------------------------------
def setup_root(pkg_name: str='deeevolab'):
	"""Sets up the root logger."""
	# Determine the logging configuration for the specified package
	cfg_file = pkg_resources.resource_filename(pkg_name, "log_cfg.yml")

	# Load the logging configuration
	try:
		with open(cfg_file, "r") as fd:
			log_cfg = yaml.safe_load(fd.read())

	except FileNotFoundError as err:
		raise FileNotFoundError("Could not find log_cfg.yaml file at {}!"
		                        "".format(cfg_file)) from err

	# Add additional log levels
	_add_additional_log_levels()

	# Configure the logger from the loaded config
	logging.config.dictConfig(log_cfg['main'])

	# Get the root logger
	root = logging.getLogger()

	# Try to set a colored logger
	try:
		from colorlog import ColoredFormatter

	except ImportError:
		# module not available -> cannot set coloured formatter
		pass

	else:
		# Configure colored logging
		clog_cfg 			= log_cfg['coloredFormatter']
		coloredFormatter 	= ColoredFormatter(clog_cfg['formatstr'],
		                                       **clog_cfg['kwargs'])

		# Try to set the formatter of the Stream Handlers
		for handler in root.handlers:
			if handler._name == 'consoleHandler':
				handler.setFormatter(coloredFormatter)
				root.debug("Changed formatter of StreamHandler to ColoredFormatter.")

	log.debug("Finished setting up the root logger.")
	return root

def setup_logger(name: str=None, level: str=None) -> logging.Logger:
	"""Sets up and returns a logger with the given name.
	
	Args:
	    name (str, optional): Name of the logger, should be __name__ variable or None, to use the root logger.
	    level (str, optional): The level to set the logger and its handlers to
	
	Returns:
	    logger (logging.Logger): logger object with the argument name.
	"""

	# Initialise the logger with the given name -- usually the module name
	_logger = logging.getLogger(name)

	# Add the custom log levels
	_add_additional_log_levels()

	# write to the local logger
	log.note("Initialised logger %s", name)

	# Adjust level, if argument passed
	if level:
		_logger.setLevel(level)
		for handler in _logger.handlers:
			handler.setLevel(level)

		log.debug("Set level of logger %s and its handlers to %s", name, level)

	return _logger


def _add_additional_log_levels():
	"""Helper function to add more log levels"""

	# Add additional log level for repetitively called log messages
	DEBUG_LEVELV_NUM = 5
	logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
	def debugv(self, message, *args, **kws):
		if self.isEnabledFor(DEBUG_LEVELV_NUM):
			# prefix "|- " for this level, because it is often repetitive stuff
			self._log(DEBUG_LEVELV_NUM, "|- " + message, args, **kws)
	logging.Logger.debugv = debugv

	# Add additional log level for info on progress
	PROGRSS_LEVEL_NUM = 13 	#  (between DEBUG and INFO)
	logging.addLevelName(PROGRSS_LEVEL_NUM, "PROGRSS")
	def progress(self, message, *args, **kws):
		if self.isEnabledFor(PROGRSS_LEVEL_NUM):
			self._log(PROGRSS_LEVEL_NUM, message, args, **kws)
	logging.Logger.progress = progress

	# Add additional log level for messages on system state
	STATE_LEVEL_NUM = 15 	#  (between DEBUG and INFO)
	logging.addLevelName(STATE_LEVEL_NUM, "STATE")
	def state(self, message, *args, **kws):
		if self.isEnabledFor(STATE_LEVEL_NUM):
			self._log(STATE_LEVEL_NUM, message, args, **kws)
	logging.Logger.state = state

	# Add additional log level for messages that are a bit less important than information messages
	NOTE_LEVEL_NUM = 17 	#  (between STATE and INFO)
	logging.addLevelName(NOTE_LEVEL_NUM, "NOTE")
	def note(self, message, *args, **kws):
		if self.isEnabledFor(NOTE_LEVEL_NUM):
			self._log(NOTE_LEVEL_NUM, message, args, **kws)
	logging.Logger.note = note

	# Add additional log level for highlighted messages (that are not warnings)
	HIGHLIGHT_LEVEL_NUM = 25 	#  (between STATE and INFO)
	logging.addLevelName(HIGHLIGHT_LEVEL_NUM, "HILIGHT")
	def highlight(self, message, *args, **kws):
		if self.isEnabledFor(HIGHLIGHT_LEVEL_NUM):
			self._log(HIGHLIGHT_LEVEL_NUM, message, args, **kws)
	logging.Logger.highlight = highlight

	# Add additional log level for warning messages that are just slightly below warnings
	CAUTION_LEVEL_NUM = 29 	#  (between STATE and INFO)
	logging.addLevelName(CAUTION_LEVEL_NUM, "CAUTION")
	def caution(self, message, *args, **kws):
		if self.isEnabledFor(CAUTION_LEVEL_NUM):
			self._log(CAUTION_LEVEL_NUM, message, args, **kws)
	logging.Logger.caution = caution
