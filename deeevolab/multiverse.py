"""This is the multiverse.

It consists of arbitrarily many universes, which have absolutely no way of interacting with each other. They run in parallel, without ever talking to each other.

This is the analogy for the implementation of multiple simulations with different parameters, which are executed in parallel.
"""

import os
import sys
import copy
import glob
import platform
import pkg_resources
from shutil import copyfile
from datetime import datetime as dt

import yaml
import dill as pkl
import h5py as h5

from paramspace import ParamSpace

import deeevolab as dvl
import deeevolab.tools
import deeevolab.universe
import deeevolab.environment

import easy_mp as mp

# Initialise loggers and config file
log = dvl.setup_logger(__name__, level=dvl.get_log_level(is_universe=True))
cfg = dvl.get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Local constants
DEBUG = dvl.get_debug_flag()
PKG_DIR = pkg_resources.resource_filename('deeevolab', '')

# -----------------------------------------------------------------------------

class Multiverse(dvl.environment.Environment):
	"""The Multiverse class aims to parallelise the execution of uncoupled Universes with different parameters."""

	def __init__(self, Universe, *env_args, out_basedir: str=None, pool_kwargs: dict=None, reporter_kwargs: dict=None, pspace: dict=None, **env_kwargs):
		"""Initialises a Multiverse, which handles running parallel simulations of the given Universe."""

		# Initialise parent Environment
		super().__init__(*env_args, **env_kwargs)

		# Handle default values
		pool_kwargs = pool_kwargs if pool_kwargs else {}
		reporter_kwargs = reporter_kwargs if reporter_kwargs else {}
		pspace = pspace if pspace else {}

		# For the pool and reporter, update the defaults from the cfg with the given values, recursively
		_pool_kwargs = copy.deepcopy(cfg.defaults.pool_kwargs)
		pool_kwargs = dvl.tools.rec_update(_pool_kwargs,
		                                   pool_kwargs)
		del _pool_kwargs

		_reporter_kwargs = copy.deepcopy(cfg.defaults.reporter_kwargs)
		reporter_kwargs = dvl.tools.rec_update(_reporter_kwargs,
		                                       reporter_kwargs)
		del _reporter_kwargs

		# Save the Universe class to use for creating the universes and the given parameter space
		self.Universe = Universe
		self.pspace = pspace

		# Initialise some attributes
		self.simulations = []  # For saving of simulation call arguments

		# Set up directories
		self._init_dirs(out_basedir=out_basedir)

		# Reporter and Pool . . . . . . . . . . . . . . . . . . . . . . . . .
		# Create the filepath at which to report to
		filepath = os.path.join(self.dirs['out'],
		                        reporter_kwargs.pop('filename', ''))

		# Initialise reporter
		self.reporter = mp.Reporter(filepath=filepath,
		                            obs_dir=self.dirs['out'],
		                            **reporter_kwargs)
		log.note("Reporter initialised.")

		# Initialise pool
		self.pool = None
		self.pool_kwargs = pool_kwargs
		self.init_pool(**pool_kwargs)

		log.info("Multiverse initialised with %d processes.",
		         self.pool.get_num_processes())

	def _init_dirs(self, *, out_basedir: str):
		"""Creates the simulation output directory and the relevant substructure. Also creates the symlinks to that directory and copies config files there...
		"""
		log.debug("Initialising Multiverse directories...")

		# Get the universe class name to use as project name
		prj_name = self.Universe.__name__.lower()
		log.debugv("Acquired project name from Universe:  %s", prj_name)

		# Determine the base directory for all output
		out_basedir = out_basedir if out_basedir else cfg.io.out_basedir
		out_basedir = os.path.expanduser(out_basedir)
		self.dirs['base'] = out_basedir

		log.state("Base output directory:")
		log.state("  %s", self.dirs['base'])

		# Create output directory (depending on hostname, runmode, and time) and subdirectories and save as attribute
		_date = dt.now()
		self.datestr = _date.strftime(cfg.io.datestrf) # as unique ident.
		_host = platform.node().split(".")[0]
		subdir_path	= cfg.io.dir_name.format(host=_host, prj=prj_name,
		                                     mv=self, date=self.datestr)

		self.dirs['out'] = os.path.join(out_basedir, subdir_path)
		# 'out' is the base directory for all the simulation results

		# Add the subdirectories, create the simulation directory and the subdirectories and also make the subdirectory entries in self.dirs absolute
		for key, path in dict(cfg.io.subdirs).items():
			if key in ['out', 'base']:
				raise ValueError("multiverse.io.subdirs should not have entries 'out' and 'base', as they are specified dynamically inside Multiverse._init_dirs!")

			# make the path absolute and add it to the dirs dict
			self.dirs[key] = os.path.join(self.dirs['out'], path)
			try:
				os.makedirs(self.dirs[key])
			except FileExistsError:
				pass

		log.state("Multiverse output directory created:")
		log.state("  %s", self._rel_to_base('out'))

		# Create corresponding relative symlink
		index_dir 	= os.path.join(out_basedir, cfg.io.index_dir)
		if not os.path.exists(index_dir):
			os.makedirs(index_dir)
		self.dirs['idx'] = index_dir

		self.dirs['ln']	= cfg.io.index_name.save.format(mv=self, prj=prj_name,
		                                            	date=self.datestr)
		os.symlink(os.path.relpath(self.dirs['out'],
		                           start=os.path.dirname(self.dirs['ln'])),
				   self.dirs['ln'])
		log.state("Output directory symlinked:")
		log.state("  %s", self._rel_to_base('ln'))

		# Create _last_foobar symlink
		ln_last = cfg.io.ln_last.save.format(mv=self, prj=prj_name)
		if os.path.islink(ln_last):
			os.unlink(ln_last)
		os.symlink(os.path.relpath(self.dirs['out'],
		                           start=os.path.dirname(ln_last)),
				   ln_last)

		log.debug("Symlinks created.")

		# Loop over all yaml files below the base directory and copy them to the same tree inside the cfg directory
		cfg_files = glob.glob(os.path.join(PKG_DIR, "**/*.yml"),
		                      recursive=True)

		for cfg_fpath in cfg_files:
			# Note: cfg_fpath are absolute paths
			# Find the relative path regarding the basedir, which whill then be the target directory
			_target	= self.dirs['config'] + os.path.relpath(cfg_fpath, PKG_DIR)

			# ...and copy it to the relative path inside the config directory; creating the directory, if it does not exist
			if not os.path.isdir(os.path.dirname(_target)):
				os.makedirs(os.path.dirname(_target))
			copyfile(cfg_fpath, _target)

		log.note("Saved %d config files.", len(cfg_files))

	def init_pool(self, **pool_kwargs): # TODO directory passing
		"""This method can be used to (re-)initialise the multiprocessing pool.
		"""
		self.pool = mp.EasyPool(reporter=self.reporter,
		                        stopdir=None, **pool_kwargs)
		# TODO add stop dir: self.dirs['out'] ?

	EXTD_FORMAT_KEYS 	= ['dirname', 'datestr']
	def _parse_extd_format_spec(self, part: list) -> str:
		"""Extend the regular __format__ method"""

		if len(part) == 1:
			key = part[0]

			if key in ['dirname', 'datestr']:
				return self.datestr
			else:
				raise ValueError("Invalid format string key '{}'".format(key))

	def write_mv_info(self, add_notes: bool=True):
		"""Writes information about the Multiverse to some files in the output directory."""

		mv_info = "\nM U L T I V E R S E   I N F O\n"+("="*29)+"\n"

		for n, sim in enumerate(self.simulations):

			mv_info += "\n\n- - -  Simulation  {}/{}  - - -\n\n".format(n+1, len(self.simulations))

			# Add ParamSpace info, if present
			if isinstance(sim.get('pspace'), ParamSpace):
				# Is a ParameterSweep
				mv_info += sim.get('pspace').get_info_str()
			else:
				mv_info += "Single run using default ParamSpace values."

			# Dump the skip list, if it is present
			skip_list = sim.get('skip_list', None)
			if skip_list:
				dvl.tools.dump_yaml(skip_list,
				          cfg.io.skip_list.save.format(mv=self, n=n))
				log.debug("Saved skip_list to yml.")

		if add_notes:
			# A place for taking notes
			mv_info += "\n\nN O T E S\n"+("-"*9)+"\n ...\n"

		# Write the information to a txtfile
		with open(cfg.io.mv_info.save.format(mv=self), 'w') as f:
			f.write(mv_info)
			log.note("Wrote Multiverse info to text file.")

	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	# Performing simulations

	@dvl.tools.try_and_except(KeyboardInterrupt)
	def run_single(self, params: dict=None) -> dvl.universe.Universe:
		"""Performs a single simulation with the given parameters and returns the result."""

		log.highlight(dvl.tools.log_centred_msg("Single Simulation"))

		if self.simulations:
			raise NotImplementedError("Cannot run more than one simulation in "
			                          "Multiverse yet.")

		# Use the pspace given at initialisation as parameters
		if not params:
			log.debug("Using parameters given at initialisation.")
			params = self.pspace

		# Generate universe name using the same pattern as for parameter_sweep
		name = cfg.io.uni_name.save.format(pt_no=0, max_no=0, digits=0)

		# Check if a params dict was given or a ParameterSpace
		if isinstance(params, ParamSpace):
			log.debug("run_single got ParamSpace objects, using defaults.")
			params = params.default

		# Save the parameters
		dvl.tools.dump_yaml(params,
		                    cfg.io.uni_params.save.format(mv=self, name=name))

		# Save the simulation call
		self.add_sim(params=params)

		# Prepare params -- some values need to be removed
		params.pop('niceness', None)
		params.pop('suppress_result', None)

		# Here we go...
		log.info("Performing a single Universe simulation...")

		res = self.init_and_run(Universe=self.Universe, name=name,
		                        base_dir=self.dirs['universes'], **params)

		log.note("Finished single Universe simulation.\n\n")

		return res
		
	@dvl.tools.try_and_except(KeyboardInterrupt)
	def parameter_sweep(self, *, pspace: ParamSpace=None, skip_list: list=None):
		"""Performs a parameter sweep on the given Universe class with the parameter space passed via argument params.
		"""

		log.highlight(dvl.tools.log_centred_msg("Parameter Sweep"))

		if self.simulations:
			raise NotImplementedError("Cannot run more than one simulation in "
			                          "Multiverse yet.")

		if not pspace:
			log.debug("Using `pspace` given at initialisation.")
			pspace = self.pspace

		if not isinstance(pspace, ParamSpace):
			raise TypeError("Need a ParamSpace object in order to perform a "
			                "parameter sweep, got '{}'.\nNote: If only a "
			                "single point should be simulated, use the "
			                "run_single method.".format(type(pspace)))

		# Save the simulation call
		self.add_sim(pspace=pspace, skip_list=skip_list)

		# Handle default value for skip list
		skip_list = [] if skip_list is None else skip_list

		# Get the number of points (i.e. volume) of the parameter space
		pspace_vol = pspace.volume
		if pspace.num_dims == 0:
			raise ValueError("The given ParamSpace has no dimensions or no "
			                 "volume; ParamSweep cannot be performed.")
			return

		# Pickle the ParamSpace object
		pkl.dump(pspace, open(cfg.io.pspace.save.format(mv=self), 'wb'))
		log.debug("Pickled pspace to %s", cfg.io.pspace.save.format(mv=self))

		# Some info and reporting...
		log.info("Initialising parameter sweep ...")
		log.note("Creating a Universe at each point in parameter space ...")
		log.state("Parameter Space Volume:  %5d  (%d dimensions)",
		          pspace_vol, pspace.num_dims)
		log.state("To be skipped:           %5d", len(skip_list))

		# Set up reporter
		self.reporter.set_max_n(pspace_vol)
		self.reporter.set_start(end="\n", term_rep=False)
		log.note("Reporter ready.")

		# Iterate over points in parameter space and add a Universe at each point. Count how many universes are actually added and how many are skipped.
		log.highlight("Creating Universes and running simulation ...\n")
		cnt	= dict(added=0, skipped=0)
		for params, point_no in pspace.all_points(with_info=('state_no',)):
			name = cfg.io.uni_name.save.format(pt_no=point_no,
			                                   max_no=pspace_vol,
			                                   digits=len(str(pspace_vol)))

			# Save the params
			dvl.tools.dump_yaml(params,
			                    cfg.io.uni_params.save.format(mv=self,
			                                                  name=name))

			if point_no in skip_list:
				# Skip this simulation. Tell the callback handler about it.
				log.debug("Skipping universe %s (point_no: %d)",
				          name, point_no)
				self.pool.send_callback(dict(name=name, status='skipped'))
				cnt['skipped'] += 1
				continue

			# Add to EasyPool's apply method
			self.pool.apply(self.init_and_run,
			                # ...with these pool settings...
			                suppress_result=params.pop('suppress_result',
			                                           self.pool.mp_active),
			                niceness=params.pop('niceness', None),
			                # ...with the following init arguments:
							Universe=self.Universe,
			                name=name,
			                base_dir=self.dirs['universes'],
			                # ...and at this point in param space
			                **params)
			cnt['added'] += 1

		log.debug("Created %d Universes, skipped %d.",
		         cnt['added'], cnt['skipped'])
		self.reporter.one_line_terminal_report(print_now=True)

		# Universes added ... wait for workers to finish.
		# NOTE the main execution time is spent in here.
		self.pool.close_and_run(ignore_kbint=True)

		log.info("Finished parameter sweep.\n\n")

	# TODO methods to initialise a bunch of Universes (even given a ParamSpace), but not actually starting them ... not useful for multiprocessing, but for interactive work?

	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	# Methods to keep track of simulations with this multiverse, i.e.: which parameters were used etc.

	def add_sim(self, *_, pspace: ParamSpace=None, params: dict=None, sim_dir: str=None, metadata: dict=None, binfiles: dict=None, **sim_kwargs):
		"""Saves the simulation arguments that were used for running a simulation."""

		if not sim_dir:
			# The simulation is from the current run, so the out dir can be used and does not need to be passed
			sim_dir	= self.dirs['out']

		if params and not isinstance(params, dict):
			raise TypeError("Argument params should be a dict, was "+str(type(params)))

		metadata = metadata if metadata else {}
		binfiles = binfiles if binfiles else {}

		self.simulations.append(dict(sim_dir=sim_dir,
		                             pspace=pspace,
		                             params=params,
		                             sim_kwargs=sim_kwargs,
		                             metadata=metadata,
		                             binfiles=binfiles,
		                             data_loaded=False))

		log.state("Tracking simulation from:")
		log.state("  %s", self._rel_to_base(sim_dir))

		# Write some information about it to the output directory
		self.write_mv_info()

	# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	# Helpers

	@staticmethod
	def init_and_run(*, Universe, init_kwargs: dict, name: str, base_dir: str, run_kwargs: dict):
		"""Initialises a Universe and calls the run_simulation method afterwards. Returns the result of the run_simulation method."""

		# Initialise the Universe
		Uni = Universe(max_num_steps=run_kwargs.get('num_steps', 0) + 1,
		               name=name, base_dir=base_dir, **init_kwargs)
		# max_num_steps is needed in initalisation of monitors, therefore it is extracted here to make it available during initialisation. The +1 is because the initial step is also recorded

		# Run the simulation and return its result
		return Uni.run_simulation(**run_kwargs)

	def _rel_to_base(self, path: str):
		"""Return the given path relative to the base directory."""
		if path in self.dirs:
			# handle it like a key and use the path given in self.dirs
			path = self.dirs[path]

		return os.path.relpath(path, start=self.dirs['base'])
