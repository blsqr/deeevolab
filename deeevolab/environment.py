"""Defines the Environment class"""

import copy
from pprint import pformat
from collections import OrderedDict

import deeevolab as dvl
from deeevolab.monitor import CompoundBinaryMonitor

# Initialise loggers and config file
log = dvl.setup_logger(__name__, level=dvl.get_log_level())
cfg = dvl.get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Local constants
DEBUG = dvl.get_debug_flag()

# -----------------------------------------------------------------------------

class Environment:
	"""Environment objects are where evolutionary systems interact. This class needs to be subclassed and cannot be instantiated itself.

	They are very abstract in the sense that they do not require any definition of what they actually are; they can be minimalistic, i.e., only being the spectator of any interactions inside; or they can be rather complex, taking part in the interactions and in the process also changing.

	The environment (or, more precisely: its child classes) can also be present at multiple levels of organisation. For example, one child of the Environment class could represent a single cell (lower level), while another child class could represent the interaction between these cells (higher level).

	Environment objects should initiate and manage the execution of any code of evolutionary entities that are inside that environment object.
	"""

	def __init__(self, *args, dirs: dict=None, name: str=None, data_saver=None, monitoring: bool=True, max_num_steps: int=0):
		"""Initialise an Environment object. Note that all the arguments are optional.

		Args:
			dirs (dict) 			: directories, where things can be saved to
			name (str)  			: used to generate a string via format()
			data_saver (DataSaver) 	: used to saved the data for the monitors
			monitoring (bool)		: whether to perform monitoring or not
			max_num_steps (int) 	: needed in monitors that allocate memory
		"""
		if len(args):
			raise ValueError("Environment class does not take position-only arguments, got {}.".format(args))

		# Environments have some directories associated with them. They are usually set in the Child method ...
		self.dirs 			= dirs if dirs else {}

		# Environments can have a name
		self.name 			= name

		# Environments can monitor what happens inside of them ...
		self.monitors		= OrderedDict()
		self._monitoring	= monitoring

		self.data_saver 	= data_saver

		# Environments keep track of the 'steps' they executed
		self.steps 			= 0 				# Current step count
		self.max_num_steps 	= max_num_steps		# (important for monitors)

		return

	# Formatting ..............................................................

	FORMAT_KEYS 		= ['name', 'step', 'steps', 'maxsteps', 'dir', 'dirs', 'str', 'pltdir', '_']
	EXTD_FORMAT_KEYS 	= []

	def __format__(self, spec: str):
		"""Returns a formatted string, which can e.g. be used for a filename.

		The spec argument is the part right of the colon in the '{:}' of a format string.
		"""
		if len(spec) == 0:
			return str(self.name if self.name else '')

		parts 		= []
		join_char	= ""

		for part in spec.split(","): # split by comma and build string
			part 	= part.split("=")

			if not len(part):
				raise ValueError("Empty format string part: {}".format(part))
			elif len(part) > 2:
				raise ValueError("Part '{}' had more than one '=' as separator.".format("=".join(part)))

			# Catch changes of the join character
			if len(part) == 2 and part[0] == 'join':
				join_char 	= part[1]
				continue

			# Determine parser
			if part[0] in self.EXTD_FORMAT_KEYS: # has priority
				parser  = self._parse_extd_format_spec
			elif part[0] in self.FORMAT_KEYS:
				parser 	= self._parse_format_spec
			else:
				log.warning("Invalid part of format string: %s. Continuing.", part)
				continue

			# Pass all other parsing to the helper
			try:
				parsed_part = parser(part)
			except ValueError:
				print("Invalid format string '{}'.".format(spec))
				raise
			else:
				if parsed_part is not None:
					parts.append(parsed_part)

		return join_char.join(parts)

	def _parse_format_spec(self, part: list) -> str:
		"""Parses the format specificiation of the Environment class. If """
		if len(part) == 2:
			# is a key, value pair
			key, val 	= part

			if key in ["name"]:
				try:
					return "{:{spec:}}".format(self.name, spec=val)
				except ValueError:
					# not formattable with this spec, fallback to str call
					return str(self.name)

			elif key in ["step", "steps"]:
				return "{:{spec:}}".format(self.steps, spec=val)

			elif key in ["maxsteps"]:
				return "{:{spec:}}".format(self.max_num_steps, spec=val)

			elif key in ["dir", "dirs"]:
				return self.dirs.get(val, "")

			elif key in ["str"]:
				# append a string
				return str(val)

			else:
				raise ValueError("Invalid key value pair '{}: {}'.".format(key, val))

		elif len(part) == 1:
			key 	= part[0]

			if key in ["name"]:
				return str(self.name)

			elif key in ["pltdir"]:
				return self.dirs.get("plots", "")

			elif key in ["step", "steps"]:
				return str(self.steps)

			elif key in ["maxsteps"]:
				return str(self.steps)

			elif key in ["_"]:
				pass

			else:
				raise ValueError("Invalid key '{}'.".format(key))

	def _parse_extd_format_spec(self, part: list) -> str:
		raise NotImplementedError("This method needs to be implemented in a child class and the attribute constant EXTD_FORMAT_KEYS adjusted to redirect them to this method")

	# Monitoring ..............................................................

	def increment_steps(self):
		"""Increments the Environment's step count. This method should be called at the beginning of a simulation step (0 corresponds to the initial state, thus counting should start at 1).
		"""
		log.debug("Incrementing step of %s ...", self.__class__.__name__)
		self.steps 	+= 1
		return

	def setup_monitors(self, max_length: int=0, **mon_kwargs):
		"""Sets up a number of monitors from a dict."""

		log.info("Initialising %d monitors ...", len(mon_kwargs))

		# Work on a copy, because the values are changed below
		mon_kwargs 	= copy.deepcopy(dict(mon_kwargs))

		for dict_key, params in mon_kwargs.items():
			log.progress("Adding %s ...", dict_key)

			if not params.pop('enabled', True):
				log.debug("Is not enabled. Skipping ...")
				continue

			# Update the parameter dictionary with some values
			params 	= dict(params)
			# IMPORTANT Have to do this, because else it is an EasyDict object and causes quite some problems ...

			# Add the maximum length to the parameters (default: True)
			if params.pop('needs_max_length', True):
				if max_length > 0:
					params['max_length'] 	= max_length
				elif self.max_num_steps > 0:
					log.note("Using attribute max_num_steps of %s for max_length argument of monitor '%s' ...", self.__class__.__name__, params.get('name'))
					params['max_length'] 	= self.max_num_steps
				else:
					log.warning("Setting up monitor '%s' with length 0! Recording will not be possible", params.get('name'))
					params['max_length'] 	= 0


			# If necessary, parse source and put back into parameter dict
			if isinstance(params['source'], str):
				try:
					params['source'] = eval(params['source'])

				except Exception as err:
					log.error("Error while eval-ing source '%s' for monitor %s. Not initialising this monitor.\nError message: %s", params['source'], dict_key, err)
					continue

			# Parse mapping function and put back into params dict
			_map_func = params.get('variable_defaults',{}).get('map_func')

			if _map_func:
				try:
					_map_func = eval(_map_func)

				except Exception as err:
					log.error("Error while eval-ing variable default mapping function '%s' for monitor %s. Not initialising this monitor.\nError message: %s", _map_func, dict_key, err)
					continue

				else:
					# Write it to the params -- variable_defaults are sure to exist because else the if-condition would not have been fulfilled.
					params['variable_defaults']['map_func'] = _map_func

			# Get the monitor class
			if params.get('mon_class') is not None:
				mon_class 	= params.get('mon_class')
				if isinstance(mon_class, (list, tuple)):
					# was given as (package name, class name) tuple or list
					_pkg, _cls 	= mon_class

				elif isinstance(mon_class, str):
					# was given as a module string, where the last element is the class name
					mon_class 	= mon_class.split(".")
					_pkg, _cls 	= mon_class[:-1], mon_class[-1]

				else:
					raise TypeError("Could not extract package and class name from argument mon_class with value '{}'.\nmon_class needs to be module string ('pkg1.pkg2.ClassName') or a 2-tuple ('pkg1.pkg2', 'ClassName').".format(mon_class))

				# Set the monitor class via import
				_module  			= __import__(_pkg, fromlist=[_cls])
				params['mon_class']	= getattr(_module, _cls)
				log.debug("Imported %s for mon_class", params['mon_class'])

			# Initialise monitor
			self.add_monitor(dict_key=dict_key, **params)

		log.note("Initialised %d monitors.", len(mon_kwargs))

		return

	def add_monitor(self, *args, out_dir=None, mon_class=CompoundBinaryMonitor, data_saver=None, dict_key=None, **kwargs):
		"""Initialises a monitor and adds it to attributes"""

		if len(args) > 0:
			log.warning("Positional arguments were passed to add_monitor; all arguments to Monitor classes need to be keyword arguments.")

		if out_dir is None:
			if self.dirs.get('mon') is None:
				raise ValueError("No output directory for monitor given and none specified in attribute 'dirs' to use as fallback!\n Either pass argument out_dir to add_monitor() or specify a subdirectory 'mon' in the attribute dirs (type dict) of " + str(self.__class__.__name__))

			out_dir = self.dirs['mon']

		if data_saver is None:
			data_saver = self.data_saver

		log.debug("Adding monitor with key '%s' ...", dict_key)

		# Initialise monitor object
		try:
			mon = mon_class(out_dir=out_dir, data_saver=data_saver, **kwargs)

		except Exception as err:
			log.error("Error setting up %s monitor: %s\n\n" \
			          "Initialisation arguments:\n%s",
			          kwargs.get('name'), err, pformat(kwargs))
			if DEBUG:
				raise
			return

		# Which name to use to refer to these monitors
		if dict_key is not None:
			self.monitors[dict_key] = mon
		else:
			# use internally produced name as key
			self.monitors[mon.uname] = mon

		return mon

	def get_monitors(self):
		return self.monitors

	def monitoring_enabled(self):
		""""""
		return self._monitoring

	def update_monitors(self):
		"""Record a step in all monitors that are registered with this environment."""
		log.info("Updating %d monitors and recording step ...",
		         len(self.monitors))

		for mon in self.monitors.values():
			if mon.rec_mode not in ['get']:
				log.debugv("Skipping '%s' -- is configured for manual update.",
				           mon.name)
				continue

			try:
				log.progress("Updating %s ...", mon.name)
				mon.record_single_step()
			except:
				log.error("Failed updating monitor %s", mon.name)
				if DEBUG:
					raise

		log.note("Finished updating monitors.")

	def plot_monitors(self, *args, **kwargs):
		"""An alias to the private method that calls the plot method of all monitors registered with this Environment object."""
		return self._ana_plot_monitors(*args, **kwargs)

	def save_monitors(self, cleanup: bool=True, close: bool=False):
		"""Call the save_data mathod on all Environment monitors"""
		log.info("Saving monitors ...")
		for mon in self.monitors.values():
			log.progress("Saving %s ...", mon.name)
			try:
				mon.save_data(cleanup=cleanup, close=close)
			except NotImplementedError:
				log.warning("Saving data not implemented for %s!", mon.name)
			else:
				log.debug("Saved data of %s.", mon.name)

		log.note("Finished saving monitors.")

	# Other public methods ....................................................

	# Private methods . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def _ana_plot_monitors(self, only: list=None, exclude: list=None):
		"""Calls the plot method on the specified monitors."""

		log.note("Plotting all %d monitors of %s ...",
		         len(self.monitors), self.__class__.__name__)

		if exclude is None:
			exclude = []

		for name, mon in self.monitors.items():
			if ((only and name in only)
			    or (not only and name not in exclude)):
				mon.plot('all') # all relates to the variables of the monitor

