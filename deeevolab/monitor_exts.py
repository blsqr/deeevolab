''' Here, extensions of the main monitor classes are defined.'''

import deeevolab as dvl
from deeevolab.monitor import BinaryMonitor

# Initialise loggers and config file
log 	= dvl.setup_logger(__name__, level=dvl.get_log_level())
cfg 	= dvl.get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Local constants
DEBUG 	= dvl.get_debug_flag()

# -----------------------------------------------------------------------------

class PropertyMapMonitor(BinaryMonitor):
	''' This monitor specialises on recording PropertyMaps from deeevolab.network.Network instances.
	'''

	def __init__(self, *args, source, pm_type: str, pm_name: str, col_name_dtype: str=None, **kwargs):
		''' Initialise a PropertyMapMonitor, child of a BinaryMonitor.'''

		# Determine column name dtype and if the pm_type is valid
		if col_name_dtype:
			log.warning("Argument col_name_dtype was given, but %s '%s' will ignore it.", self.__class__.__name__, kwargs.get('name'))

		if pm_type in ['vp', 'ep']:
			col_name_dtype = 'int32'
		elif pm_type == 'gp':
			col_name_dtype = 'S24' # 24 byte string
		else:
			raise ValueError("Argument pm_type needs to be 'vp', 'ep', or 'gp', was {}".format(pm_type))

		# Save the type of property map that is to be recorded
		self.pm_type 	= pm_type
		self.pm_name 	= pm_name

		# Determine the variable names to use from the property map
		variables = self._get_variable_names(gv=source,
		                                     pm_type=pm_type, pm_name=pm_name)

		# Pass these to the parent init method, that creates the records
		super().__init__(*args, source=source, variables=variables,
		                 col_name_dtype=col_name_dtype, **kwargs)

		# Save a reference to the property map object to attributes
		if self.pm_type == 'vp':
			self.pmap 		= self.source.vp.get(self.pm_name)

		elif self.pm_type == 'ep':
			self.pmap 		= self.source.ep.get(self.pm_name)

		elif self.pm_type == 'gp':
			# cannot get the pm_name directly, because gp returns objects, not the property map itself -> value extracted in record_single_step
			self.pmap 		= self.source.gp

		# pm_type value checks performed in _get_variable_names

		if self.pmap is None:
			raise ValueError("No such PropertyMap (type {}, name {}) for the given source.".format(self.pm_type, self.pm_name))

		log.debug("Initialised %s:  %s", self.__class__.__name__, self.name)

	def add_variable(self, var_name: str, map_func=None, dtype=None, plot_types: list=None, add_column: bool=False):
		# NOTE overwrites the parent method completely

		# Save all the basic information for this variable
		self.variables[var_name] 	= {
			'map_func' 	 : map_func,
			'dtype'		 : dtype if dtype else self.dtype,
			'get_val'	 : None,
			'plot_types' : plot_types if plot_types else self.defaults.get('plot_types'),
			'valid_range': [self.steps, self.steps], # python style
			'column' 	 : len(self.variables)
		}

	def _get_variable_names(self, gv=None, pm_type=None, pm_name=None):

		# Get default values, if they were not given... need to have this for getting variable names before the parent class init method is called.
		if not gv:
			gv = self.source

			if not gv:
				raise ValueError("Need argument source specified for initialisation of {}!".format(self.__class__.__name__))

		pm_type = pm_type if pm_type else self.pm_type
		pm_name = pm_name if pm_name else self.pm_name

		# Distinguish between different pm_types
		if pm_type == 'vp':
			return [int(v) for v in gv.vertices()]

		elif pm_type == 'ep':
			return [tuple(e) for e in gv.edges()]

		elif pm_type == 'gp':
			# Distinguish between only one name being given or multiple
			if isinstance(pm_name, (list, tuple)):
				# was a list of property names -> use as variables
				return pm_name
			else:
				return [pm_name]

		else:
			raise ValueError("Unsupported pm_type "+str(pm_type))

	def _record_single_step(self):
		''' Adds the current values of the selected property map to the monitor.'''

		# Read PropertyMap depending on pmap type
		if self.pm_type in ['vp', 'ep']:

			# Get the row of data (possible a masked array)
			row 	= self.pmap.ma

			# Discard all masked values, if it is a masked array
			try:
				row 	= row.compressed()
			except AttributeError:
				pass

			# Add a row of elements to the data
			success = self._add_row(row)

		elif self.pm_type == 'gp':
			# Read the pmap directly for each variable (has no further content)
			success 	= True
			for var_name, var in self.variables.items():
				# Get value by calling the value_getter of this variable
				point 	= self.pmap.get(var_name)

				success *= self._add_point(var_name, point)

		return success

