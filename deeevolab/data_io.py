''' This module handles data input/output into deeevolab and is mainly used in the plotting module.'''

import re
import copy
import time
import yaml
import json

import h5py as h5

import deeevolab as dvl
import deeevolab.tools

# Initialise loggers and config file
log 	= dvl.setup_logger(__name__, level=dvl.get_log_level())
cfg 	= dvl.get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Local constants
DEBUG	= dvl.get_debug_flag()
WRITE_DATA_TO_LOG = cfg.get('write_data_to_log', False)

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# For monitors: Data Savers

class DataSaver:
	''' A class that is used to save the data from a Monitor class to one specific output file.'''

	FORMATS 		= []
	SHARED_SAVER	= False

	def __init__(self, fpath: str, data_format: str='auto', enabled: bool=True):
		''' Initialise a DataSaver object.

		Args:
			fname (str) 		: the desired filepath for the output file

			data_format (str)	: which data format to save to. Default: 'auto', determines the format from the filepath

			enabled (bool) 		: whether saving is enabled

		'''

		log.debug("Initialising %s ...", self.__class__.__name__)

		# Save attributes
		self.fpath 	= fpath

		if data_format == 'auto':
			data_format 		= fpath.split(".")[-1]
		else:
			data_format 		= data_format

		if data_format in self.FORMATS:
			self.data_format 	= data_format
		else:
			raise ValueError("Illegal data_format argument '{}', has to be one out of {}.".format(data_format, self.FORMATS))

		self.enabled 	= enabled
		self.read_only 	= False

		return

	def save(self, monitor):
		''' Performs the saving operation of the passed monitor object.'''
		# Distinguish between full-fletched monitors, that record steps and have a name, and between simple monitors like the MetadataDict
		if hasattr(monitor, 'get_name') and hasattr(monitor, 'get_steps'):
			log.debug("Saving %s as %s after %d steps...",
					 monitor.get_name(), self.data_format, monitor.get_steps())
		else:
			log.debug("Saving (simple) monitor as %s ...", self.data_format)

		if self.enabled:
			getattr(self, '_save_as_' + self.data_format).__call__(monitor)
		return True

	def close(self):
		''' Should be called at the end of a simulation and triggers the final saving step.'''

		if self.read_only:
			if self.SHARED_SAVER:
				log.debug("%s (shared) was already closed.", self.__class__.__name__)
			else:
				log.warning("%s was already closed!", self.__class__.__name__)

		else:
			# Close and set to read_only
			self._close()
			self.read_only 	= True

		return True

	# Saving functions ........................................................
	# All start with _save_as_ and then end with the specified data_format
	# These functions must be capable of saving the data every time step, if the settings were set to do that

	# Private methods .........................................................
	def _close(self):
		''' Method to perform the closing action. Can be subclassed if an action needs to be performed to close the file.'''
		pass

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class BinarySaver(DataSaver):
	''' A saver class that works on the binary hdf5 file format.'''

	FORMATS 		= ['hdf5']
	SHARED_SAVER	= True

	def __init__(self, *args, hdf5_file_kwargs: dict=None, reopen_after_close: bool=False, **kwargs):
		''' The BinarySaver saves all data to a specific file, the binfile.

		Args:
			fname (str) : file name of the binary file within the data directory
		'''

		# Initialise via parent class
		super().__init__(*args, **kwargs)

		self.reopen_after_close 	= reopen_after_close

		# Check kwargs for hdf5 file creation
		_def_hdf5_file_kwargs = dict(cfg.BinarySaver.hdf5_file_kwargs)
		if hdf5_file_kwargs is None:
			hdf5_file_kwargs  = _def_hdf5_file_kwargs
		else:
			hdf5_file_kwargs  = _def_hdf5_file_kwargs.update(hdf5_file_kwargs)

		# Create the binary hdf5 file (if it does not exist)
		self.binfile 	= h5.File(self.fpath, 'w-', **hdf5_file_kwargs)

		log.debug("%s initialised, target: %s",
				  self.__class__.__name__, self.fpath)

	def get_attributes_handle(self):
		''' Returns the h5 file's attribute object, which can be used to write data to...'''
		return self.binfile.attrs

	def _save_as_hdf5(self, monitor):
		''' In the case of hdf5, this method does not have anything to do, because the data is saved directly to the File object and the File object handles whether it will be written to disk or not. The monitor is not needed either.'''
		pass

	def _close(self):
		''' Close the hdf5 file handler and open again in read_only mode.'''

		self.binfile.close()

		if self.reopen_after_close:
			self.binfile 	= h5.File(self.fpath, 'r')

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class SerialSaver(DataSaver):
	''' A saver class that serialises data using YAML and saves it to a *.yml file...'''

	FORMATS 		= ['yml', 'json']

	def __init__(self, *args, **kwargs):
		# Initialise via parent class
		super().__init__(*args, **kwargs)

	def _save_as_yml(self, monitor):
		''' Serialises the data of the monitor and saves it to a YAML file'''
		with open(self.fpath, 'w') as f:
			yaml.dump(monitor.records, f,
					  default_flow_style=False)

		if hasattr(monitor, 'get_name'):
			log.debug("Saved records of %s as YAML.", monitor.get_name())
		else:
			log.debug("Saved records of simple monitor as YAML.")

	def _save_as_json(self, monitor):
		''' Serialises the data of the monitor and saves it to a json file'''
		with open(self.fpath, 'w') as f:
			json.dump(flatten(monitor.records), f,
					  sort_keys=True, indent=4)

		if hasattr(monitor, 'get_name'):
			log.debug("Saved records of %s as JSON.", monitor.get_name())
		else:
			log.debug("Saved records of simple monitor as JSON.")

