''' The Network class is a child of graph_tool.Graph and adds some general functionality like plotting and analysis methods.'''

import os
import sys
import copy
from itertools import chain
from pprint import pformat

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

from easydict import EasyDict as DotDict # for attribute access to dicts
from PyPDF2 import PdfFileWriter, PdfFileReader

import graph_tool as gt
import graph_tool.draw

import deeevolab as dvl
from deeevolab.environment import Environment

# Setup logging for this file (inherits from root logger)
log	= dvl.setup_logger(__name__, level=dvl.get_log_level())
cfg	= dvl.get_config(__name__)
log.debug("Loaded module and logger.")

# Local constants
DEBUG = dvl.get_debug_flag()

# -----------------------------------------------------------------------------

class Network(gt.Graph):
	''' The Network class is an extension of the graph_tool.Graph class and especially improves comfortable plotting capabilities and interfacing with Python objects that are associated with vertices or edges.'''

	PLOT_STYLES = cfg.plot_styles 	# NOTE has to have key 'default'

	def __init__(self, *, location=None, vtx_link=None, **kwargs):
		''' Initialise a Network object.

		Args:
			location (Environment) : an Environment-derived object the Network is attributed to. This information can be used to, for example, generate a filename automatically.

			vtx_link (gt.Graph) : another network object that this Network object's vertices will be linked to, which means: each vertex in this network will carry a PropertyMap 'link_idx' with the index of the vertex in the other network and the other network's vertex can thus easily be accessed. Note that the actual vertex index does not correspond to the one in the target network.

			**kwargs: passed on to gt.Graph
		'''
		super().__init__(**kwargs)

		# Initialise attributes
		self._location 	= None
		self.tmp_pos 	= None  # Temp. position information, used in plotting
		self.was_loaded = False # if it was loaded, it might not have all data
		self._original_graph = None # in case a graph was loaded, the original one might need to be held in scope to ensure data consistency

		# Set attributes
		if location:
			self.location 	= location

		# Set attributes as PropertyMaps to access objects associated with vertices or edges
		self.vobjs		= self.new_vp('object')
		self.eobjs		= self.new_ep('object')

		# Set graph views dictionary
		self.gvs 		= {}

		# Set up links
		if vtx_link:
			self.set_vtx_link(vtx_link)

		else:
			self.link 	= None

		return

	def set_vtx_link(self, vtx_link):
		log.note("Setting a vertex link ...")

		self.link 			= DotDict()

		# Create a property map to map local indices to those in the target network and to vertex objects, if given
		self.link.idx_map	= self.new_vp('int')
		self.link.vobjs		= self.new_vp('object')

		# Create a PropertyMap that marks each linked vertex in the target network with True, such that it can be used in the GraphView
		self.link.is_linked	= vtx_link.new_vp('bool')
		self.link.gv 		= gt.GraphView(vtx_link,
		                                   vfilt=self.link.is_linked)
		log.note("CAUTION: The linked GraphView only has those internal PropertyMaps available that are present at initialisation.")

		return

	def __format__(self, spec: str): # TODO
		if spec == "":
			return ""
		else:
			return "NW_Format_NotImplemented"

	# Properties - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	@property
	def location(self):
		''' An Environment-derived object the Network is attributed to. This information can be used to, for example, generate a filename automatically.'''
		return self._location

	@location.setter
	def location(self, env):
		if not isinstance(env, Environment):
			raise TypeError("{} attribute location needs to be Environment-derived, was {}".format(self.__class__.__name__, type(env)))
		log.debug("Associating %s object as location of %s",
		          env.__class__.__name__, self.__class__.__name__)
		self._location 	= env

	# Inherited methods - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	def add_vertex(self, n: int=1, linked_vtx: int=None, linked_vobj=None):
		''' Adds a vertex and, if a linked vertex index is given, links it to the vertex in the linked graph.'''
		vtx = super().add_vertex(n=n)

		if linked_vtx:
			if not self.link:
				log.warning("Attempted to link a new vertex, but no link was set for this graph. Use the set_vtx_link method to do so.")
				return vtx

			elif n > 1:
				log.warning("Cannot link vertices when more than one vertex is added at one time. Call add_vertex method with argument n==1 to perform a link.")
				return vtx

			# Link the vertex and its vertex object
			self.link.idx_map[vtx] 	= linked_vtx
			self.link.vobjs[vtx] 	= linked_vobj

			# Set the vertex to linked in the property map
			self.link.is_linked[linked_vtx] = True

			log.debug("Linked external vertex %s to internal vertex %s.", linked_vtx, vtx)

		return vtx

	def remove_vertex(self, *_, **__):
		"""Disallow using this as it might break things."""
		log.warning("gt.Graph.remove_vertex method is not available in the %s class, as it may lead to invalidating NetworkObject instances.", self.__class__.__name__)
		# TODO consider going over all vertex objects and adjusting them such that it is still possible to remove vertices
		return

	def remove_edge(self, *_, **__):
		"""Disallow using this as it might break things."""
		log.warning("gt.Graph.remove_edge method is not available in the %s class, as it may lead to invalidating EdgeObject instances.", self.__class__.__name__)
		# TODO same as with remove_vertex
		return

	def _remove_vertex(self, *args, **kwargs):
		"""Supply this functionality as private behaviour.

		NOTE Only use if you know what you are doing! I.e.: ensure that no
		NetworkObject is affected by this, otherwise there will be subtle
		breakage.
		"""
		super().remove_vertex(*args, **kwargs)

	def _remove_edge(self, *args, **kwargs):
		"""Supply this functionality as private behaviour.

		NOTE Only use if you know what you are doing! I.e.: ensure that no
		NetworkObject is affected by this, otherwise there will be subtle
		breakage.
		"""
		super().remove_edge(*args, **kwargs)

	def vertices(self, select_from: str=None, vtx_list: list=None):
		'''Adjusts the gt.Graph.vertices() iterator to allow selecting a GraphView beforehand or selecting a list of vertices or vertex indices.'''

		if vtx_list:
			log.debug("From the given list of vertices or vertex indices, returning a iterator over the corresponding Vertex descriptors ...")
			# Convert the indices to vertex objects
			vtcs 	= [v if isinstance(v, gt.Vertex)
			           else self.vertex(v)
			           for v in vtx_list]

			# Check if they do not clash with the `select_from` argument and if yes, warn.
			if select_from:
				if any([v not in self.gvs[select_from].vertices()
				        for v in vtcs]):
					log.debug("There were vertices in the given `vtx_list` that are not included in the '%s' GraphView!", select_from)
					# TODO make this more consistent

			return iter(vtcs)

		if select_from:
			log.debug("Returning iterator over GraphView '%s' vertices.", select_from)
			return self.gvs[select_from].vertices()
		else:
			return super().vertices()

	def edges(self, select_from: str=None, edge_list: list=None):
		'''Adjusts the gt.Graph.edges() iterator to allow selecting a GraphView beforehand or choosing from a list of edges.'''

		if edge_list:
			log.debug("Returning iterator over given edges.")
			return edge_list

		if select_from:
			log.debug("Returning iterator over GraphView '%s' edges.", select_from)
			return self.gvs[select_from].edges()
		else:
			return super().edges()

	# Graph manipulation - - - - - - - - - - - - - - - - - - - - - - - - - - -

	def copy_vertex(self, vtx):
		''' Copies a vertex and its associated network object.

		Returns the vertex descriptor of the newly created vertex.'''
		log.debug("Copying vertex %s ...", vtx)

		# Create a new vertex
		new_vtx 	= self.add_vertex()

		# If there is a VertexObject associated with it, copy that as well
		obj 		= self.vobjs[vtx]

		if obj:
			# Copy the object and adjust its descriptor and index
			new_obj 			= copy.copy(obj)
			new_obj.desc 		= new_vtx
			new_obj.idx 		= int(new_vtx)
			self.vobjs[new_vtx] = new_obj
			# NOTE this will only copy those object attributes that are _not_ saved in PropertyMaps

			# Invalidate any links. They need to be recreated elsewhere
			new_obj._linked 	= []

		# Copy the PropertyMap entries
		for vp_name, pm in self.vp.items():
			if vp_name in ['vobjs']: # do not copy these entries
				continue

			pm[new_vtx] = copy.copy(pm[vtx])

		# Add the edges
		for e_src, e_target, _ in chain(self.get_in_edges(vtx),
		                                self.get_out_edges(vtx)):
			e 	= (e_src, e_target)
			log.debug("Copying edge %s ...", e)

			# Add the edge with the new vertex as source or as target
			if e_src == vtx:
				# copy with new_vtx as source
				new_e 	= self.add_edge(source=new_vtx, target=e_target)
			elif e_target == vtx:
				# copy with new_vtx as target
				new_e 	= self.add_edge(source=e_src, target=new_vtx)
			else:
				raise RuntimeError("Something went wrong during copying of edges...")

			# Copy edge object
			eobj 		= self.eobjs[e]
			if eobj:
				new_eobj 			= copy.copy(eobj)
				new_eobj.desc		= new_e
				self.eobjs[new_e]	= new_eobj
				# NOTE this will only copy those object attributes that are _not_ saved in PropertyMaps

			# Copy edge properties
			for ep_name, pm in self.ep.items():
				if ep_name in ['eobjs']:
					# do not copy these objects
					continue
				pm[new_e] 	= copy.copy(pm[e])

		return new_vtx

	# Property maps - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	def copy_from_linked_pms(self, pm_names: list, select_from: str=None, vtx_list: list=None):
		'''For the given list of attribute names, go through the linked graph's property maps, access the one with that name, and copy its value to a corresponding PropertyMap with the same name in this network.'''
		if not self.link:
			log.warning("Trying to copy from a linked Network, but no link was given during initialisation of %s", self.__class__.__name__)
			return

		for vtx in self.vertices(select_from=select_from, vtx_list=vtx_list):
			log.debugv("Vertex:  %s", vtx)
			idx 	= self.link.idx_map[vtx] # Breaks with changing graphs!

			for pm_name in pm_names:
				log.debugv("  PropertyMap name:  %s", pm_name)
				self.vp[pm_name][vtx] 	= self.link.gv.vp[pm_name][idx]

	def copy_from_linked_vobjs(self, attr_list: list, select_from: str=None, vtx_list: list=None):
		'''For the given list of attribute names, go through the linked VertexObjects, access the attribute with that name, and copy its value to a corresponding PropertyMap with the same name in this network.'''
		if not self.link:
			log.warning("Trying to copy from a linked Network, but no link was given during initialisation of %s", self.__class__.__name__)
			return

		log.debug("Copying attributes %s from linked vertex objects ...", attr_list)

		for vtx in self.vertices(select_from=select_from, vtx_list=vtx_list):
			vobj 	= self.link.vobjs[vtx]

			if vobj is None:
				log.debugv("No link available for vertex %s.", vtx)
				continue

			log.debugv("Vertex:  %s", vtx)
			vtx_idx = int(vtx)

			for attr in attr_list:
				self.vp[attr].a[vtx_idx] 	= getattr(vobj, attr)

	# In-/Output - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	def load(self, file: str): # TODO use parent load method?!
		''' Load a graph from a file.'''

		# Use the graph-tool method to load a Graph object
		g 	= gt.load_graph(file)

		# Carry over all attributes
		self.__dict__.update(g.__dict__)

		# Set flags and save original graph (to keep in scope, fails otherwise)
		self.was_loaded = True
		self._original_graph = g

		log.note("Loaded graph from  {}".format(file))

	def save(self, output: str=None, output_strf: str=None, fmt: str='auto'):
		''' Saves the graph using the method inherited from gt.Graph. Can be overwritten to do more ...'''

		if isinstance(output_strf, (list, tuple)):
			# Multiple outputs given, call each individually
			for _ostrf in output_strf:
				self.save(output=output, output_strf=_ostrf, fmt=fmt)

			return


		log.note("Saving %s ...", self.__class__.__name__)

		if output_strf and not output:
			# Format strf with additional information, if the output string was not already given.
			log.debugv("Generating custom output path using format string")
			log.debugv("  %s", output_strf)
			output = output_strf.format(loc=self.location, nw=self)

		if not output:
			raise ValueError("Need an output path defined, either via argument output or via output_strf.")

		# Save
		super().save(output, fmt=fmt)

		log.debug("%s saved to %s", self.__class__.__name__, output)

	# Analysis - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	# TODO implement usage of find_cycles, filter_cycles, cycle_stats

	# Plotting - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	def plot(self, output: str=None, output_strf: str=None, filter_kwargs: dict=None, subgraph: list=None, subgraph_kwargs: dict=None, layout_kwargs: dict=None, plot_style: str=None, cbar_kwargs: dict=None, graphviz_kwargs: dict=None):
		'''
		Plots the network.

		Args:
			output (str) : the file path to save it to -- alternatively, output_strf can be passed

			output_strf (str, optional) : only used if argument output was not passed! A format string to generate the filename from

			filter_kwargs (dict) : how to filter the graph before plotting. possible keys:
				- enabled (bool)
				- property (str) ... # TODO

			subgraph (seq, optional) : a list of vertices to plot

			subgraph_kwargs (dict) : settings for the subgraph selection. Possible keys:
				- add_neighbors (bool) : (default: False) Whether to add the neighbors of the specified vertices to the plot

			layout_kwargs (dict) : how to layout the graph. Possible keys:
				- enabled (bool) : whether to use a layout algorithm or not
				- tmp_pos_mode (str) : which mode to use for the positions in the graph: 'save', 'read', or 'both'
				- algo (str) : which algorithm to use for layouting
				- algo_kwargs (dict) : kwargs passed to layouting algorithm

			plot_style (str or dict) : determines the style for the vertices and edges. If it is a string, the corresponding plot_style is read from the instance variable self.PLOT_STYLES. If it is a dict, the possible keys are:
				- vpdef (dict) : default vertex properties
				- vsdef (dict) : default vertex size
				- epdef (dict) : default edge properties
				- efuncs (dict) : (keys:) which functions to use to determine edge properties; the function needs to be a method of the Network class (or its children, if executed from the children)size. (values:) the keyword arguments to pass to that function.
				- vfuncs (dict) : same as efuncs but for vertex properties
				- cm_src (str) : which function to use the colormap and color normalisation from

			cbar_kwargs (dict) : kwargs for the colorbar. Possible keys:
				- enabled (bool)
				- ... # TODO

		'''

		log.note("Plotting %s ...", self.__class__.__name__)

		# Handle default arguments -- all need to be dicts
		filter_kwargs 	= filter_kwargs if filter_kwargs else {}
		subgraph_kwargs	= subgraph_kwargs if subgraph_kwargs else {}
		layout_kwargs 	= layout_kwargs if layout_kwargs else {}
		cbar_kwargs	 	= cbar_kwargs if cbar_kwargs else {}


		if isinstance(plot_style, dict):
			log.debug("Got custom plot_style dict for plotting of %s.", self.__class__.__name__)
			style_kwargs 	= plot_style

		elif isinstance(plot_style, str):
			log.debug("Using plot_style %s ...", plot_style)
			style_kwargs 	= self.PLOT_STYLES.get(plot_style, {})
			if not style_kwargs:
				log.error("Plot style '%s' not found in %s. Check your configuration file for the corresponding key. Will use default style.", plot_style, self.__class__.__name__)

		else:
			style_kwargs 	= self.PLOT_STYLES.get('default', {})
			if not style_kwargs:
				log.warning("Default plot_style was not set for plotting of %s.", self.__class__.__name__)

		log.debugv("plot_style dict:\n%s", pformat(style_kwargs))


		# Filename and directories ............................................
		# Determine filename
		if output is None:
			# Try to generate a filename using the outdir and fname str formats
			if not isinstance(output_strf, str):
				raise ValueError("If argument output is not set, argument 'output_strf' needs to be passed as string.")
			elif not isinstance(self.location, Environment):
				raise ValueError("%s was not initialised with an Environment-derived location object, that can be used to derive a filename for the plot.", self.__class__.__name__)
			output 	= output_strf.format(loc=self.location, nw=self)

		# Check if output directory exists and create if not
		if not os.path.isdir(os.path.dirname(output)):
			try:
				os.mkdir(os.path.dirname(output))
			except FileExistsError:
				log.caution("The output directory of %s is already present; probably was created between the check if it exists and the time it was tried to create it. Will continue ...")

		# GraphView and Filtering .............................................
		# Select the GraphView: either a subgraph, or a filtered graph
		if isinstance(subgraph, (list, tuple, np.ndarray)):
			gv 	= select_subgraph(self, subgraph, *+subgraph_kwargs)

		elif filter_kwargs.get('enabled'):
			# Filtering. Only possible when not on subgraph, because only one vertex filter can be applied at a time
			log.debug("Setting filtered GraphView ...")

			# TODO

			raise NotImplementedError

		else:
			gv 	= gt.GraphView(self)

		# Adjusting vertex and edge properties ................................
		props, cmap, cnorm	= self._set_props(gv, **style_kwargs)

		# Determine layout, if it was not passed to the arguments .............
		pos, pin 	= self._layout_graph(gv=gv, **layout_kwargs)

		# Plot using graphviz .................................................
		# Determine additional keyword arguments
		if graphviz_kwargs:
			_gvkw 				= copy.deepcopy(cfg.graphviz)
			_gvkw.update(graphviz_kwargs)
			graphviz_kwargs 	= _gvkw
		else:
			graphviz_kwargs 	= cfg.graphviz

		# ...and plot.
		pos  = gt.draw.graphviz_draw(gv,
				 					 pos=pos,
				 					 output=output,
									 eprops=props['eprops'],
									 vprops=props['vprops'],
									 vsize=props['vsize'],
									 pin=pin,
									 **graphviz_kwargs)

		# Save the positions, if selected to do so
		if layout_kwargs.get('tmp_pos_mode') in ['save', 'both']:
			self.tmp_pos 	= pos
			log.debug("Vertex positions saved to attribute.")

		# Handle colorbar kwargs and call the colorbar method
		if cbar_kwargs:
			defaults		= copy.deepcopy(cfg.pdf_cbar)
			defaults.update(cbar_kwargs)
			cbar_kwargs 	= cbar_kwargs
		else:
			cbar_kwargs 	= cfg.pdf_cbar

		try:
			add_cbar_to_pdf(output, cmap=cmap, cnorm=cnorm, **cbar_kwargs)
		except Exception as err:
			log.error("Could not add colorbar to %s plot: %s\ncbar_kwargs: %s", self.__class__.__name__, err, cbar_kwargs)
			if DEBUG:
				raise

		log.debug("Saved plot to %s", output)

		# return the positions, so another plot can use the same ones
		return pos

	# Private methods  - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	def _layout_graph(self, *_, gv=None, tmp_pos_mode: str=None, algo: str=None, **algo_kwargs):
		''' Applies a layout algorith to the graph or the given GraphView and returns a property map of positions.'''
		if not gv:
			gv = self

		if not tmp_pos_mode:
			log.debug("Calculating layout using algorithm %s...", algo)

			try:
				algo_func = getattr(gt.draw, algo)

			except AttributeError as err:
				log.error("No layout algorithm with name %s found; layouting skipped.\nAttributeError: %s", algo, err)
				pos = None

			else:
				if algo == 'radial_tree_layout':
					pos = algo_func(gv, gv.vertex(0), **algo_kwargs)
				else:
					pos = algo_func(gv, **algo_kwargs)

				log.debugv("Calculated layout.")

			pin 	= True # need to pin, such that graphviz does not do layout

		elif tmp_pos_mode in ['read', 'both']:
			pos 	= self.tmp_pos
			pin 	= False
			log.debug("Vertex positions read from attribute.")

		else:
			pos 	= None
			pin 	= False

		return pos, pin

	def _set_props(self, gv: gt.GraphView, *args, vpdef: dict=cfg.vpdef, epdef: dict=cfg.epdef, vsdef: float=cfg.vsdef, vfuncs: dict=None, efuncs: dict=None):
		''' Set the vertex and edge properties using the given functions.

		Arguments:
			gv (gt.GraphView) : the graph to work on

			vpdef (dict) : default values for vertex properties
			epdef (dict) : default values for edge properties

			vfuncs (dict) : list of functions that return a style for the vertex properties. # TODO elaborate ...

			efuncs (dict) : same as vfuncs but for edge properties.

		'''
		log.debug("Setting vertex and edge properties...")

		# Set default values for arguments
		if not vfuncs:
			vfuncs 	= {}
		if not efuncs:
			efuncs 	= {}

		# Set default values for vertex and edge properties
		props 		= dict(vprops=copy.deepcopy(vpdef),
		               	   eprops=copy.deepcopy(epdef),
		               	   vsize=copy.deepcopy(vsdef))
		cmap, cnorm = None, None

		# Loop over all specified vertex and edge property functions and update the property maps
		for params in vfuncs:
			# Get attributes
			vfunc_name 		= params.get('func_name')
			vfunc_kwargs	= params.get('kwargs', {})

			if not vfunc_name:
				log.warning("Invalid network style format; no function name given to determine vertex style. Continuing ...")
				continue

			log.debugv("Setting vertex properties using %s ...", vfunc_name)

			# Find specified private methods for setting the props
			vfunc 			= getattr(self, "_vp_"+vfunc_name)

			# Call specified method -- directly modifies the props
			_cmap, _cnorm 	= vfunc(gv, props, **vfunc_kwargs)

			if params.get('add_cbar'):
				log.debugv("Selected %s for cmap and cnorm.", vfunc_name)
				cmap, cnorm 	= _cmap, _cnorm

		for params in efuncs:
			# Get attributes
			efunc_name 		= params.get('func_name')
			efunc_kwargs	= params.get('kwargs', {})

			if not efunc_name:
				log.warning("Invalid network style format; no function name given to determine edge style. Continuing ...")
				continue

			log.debugv("Setting edge properties using %s ...", efunc_name)

			# Find specified private methods for setting the props
			efunc 			= getattr(self, "_ep_"+efunc_name)

			# Call the method -- directly modifies props
			_cmap, _cnorm 	= efunc(gv, props, **efunc_kwargs)

			if params.get('add_cbar'):
				log.debugv("Selected %s for cmap and cnorm.", efunc_name)
				cmap, cnorm 	= _cmap, _cnorm

		log.debugv("Vertex and edge properties set.")

		return props, cmap, cnorm

	# Methods for setting vertex and edge properties
	# TODO consider not inheriting them but putting them someplace all together and then importing them ...
	# Those for vprops need to start with _vp_, those for eprops with _ep_
	# Those with _properties in their name take several boolean property map names and assign a value assuming one-hot encoding of these pmaps -- usefol for categorising! Those with _property only look at a single property map.
	# TODO consider using the self.gvs instead of applying filters?!

	def _vp_default(self, gv, props, *_, **kwargs):
		return None, None

	def _vp_label_from_property(self, gv, props, *_, vp_name: str=None, filter_vp: str=None, filter_inverted: bool=False, label_kwargs: dict=None, fstr: str=None): # TODO add mapping?

		# Apply a filter
		if filter_vp:
			gv 	= gt.GraphView(gv) 	# use a new GraphView
			gv.set_vertex_filter(gv.vp[filter_vp], inverted=filter_inverted)

		vprops 	= props['vprops']

		# Get the vertex property map, if a name was given
		if vp_name:
			vp 	= gv.vp[vp_name]
		else:
			vp 	= None

		if 'label' in vprops:
			log.debug("Overwriting already present vertex property label.")
		else:
			vprops['label'] = gv.new_vp('string', val='')

		if isinstance(label_kwargs, dict):
			vprops.update(label_kwargs)

		if not fstr:
			fstr = "{}"

		# Iterate over the vertices and apply the label
		if vp:
			# Apply the  property map
			for vtx in gv.vertices():
				vprops['label'][vtx] = fstr.format(vp[vtx])
		else:
			# Just use the vertex id
			for vtx in gv.vertices():
				vprops['label'][vtx] = str(vtx)

		return None, None

	def _vp_color_from_property(self, gv, props, *_, vp_name: str, filter_vp: str=None, filter_inverted: bool=False, **ckwargs):

		# Apply a filter
		if filter_vp:
			gv 	= gt.GraphView(gv) 	# use a new GraphView
			gv.set_vertex_filter(gv.vp[filter_vp], inverted=filter_inverted)

		# Get the vertex property map
		try:
			vp = gv.vp[vp_name]
		except KeyError as err:
			raise KeyError("No such Vertex PropertyMap '{}' in the selected GraphView! Check the plotting style (e.g., vfuncs) against your network vertex properties whether the correct vp_name argument is set. Available PropertyMaps: {}".format(vp_name, [k for k in gv.vp.keys()])) from err
		else:
			log.debugv("Got PropertyMap %s:\n%s", vp_name, vp.ma)

		# Call the modularised helper method
		return self._color_from_vp(gv, props, vp, **ckwargs)

	def _vp_val_if_property(self, gv, props, *_, vp_name: str, prop_name: str='fillcolor', filter_vp: str=None, filter_inverted: bool=False, true_val: str=None, false_val: str=None):

		# Apply a filter
		if filter_vp:
			gv 	= gt.GraphView(gv) 	# use a new GraphView
			gv.set_vertex_filter(gv.vp[filter_vp], inverted=filter_inverted)

		# Get the vertex property map
		try:
			vp = gv.vp[vp_name]
		except KeyError:
			raise KeyError("No such Vertex PropertyMap '{}' in the selected GraphView! Check the plotting style (e.g., vfuncs) against your network vertex properties whether the correct vp_name argument is set. Available PropertyMaps: {}".format(vp_name, [k for k in gv.vp.keys()]))

		# Check if the property map is already present
		vprops 	= props['vprops']
		if prop_name in vprops:
			log.debugv("vprop %s already present -- overwrites might occur, if vertex filters overlap.", prop_name)

			if isinstance(vprops[prop_name], str):
				# Carry over previous value
				vprops[prop_name] = gv.new_vp('string', val=vprops[prop_name])
		else:
			vprops[prop_name] = gv.new_vp('string')

		# Apply the values to the property map
		for vtx in gv.vertices():
			if bool(vp[vtx]):
				if true_val:
					vprops[prop_name][vtx] = true_val
			else:
				if false_val:
					vprops[prop_name][vtx] = false_val

		# Return everything
		return None, None

	def _vp_shape_from_property(self, gv, props, *_, vp_name: str, mapping: dict, default_shape: str, **kwargs):

		# Check if the shape property map is already present
		vprops 	= props['vprops']

		if 'shape' in vprops: # FIXME need to check if property map!
			log.debugv("shape vprop already present -- overwrites might occur.")
		else:
			vprops['shape'] = gv.new_vp('string')

		vp 		= gv.vp[vp_name]

		for vtx in gv.vertices():
			vprops['shape'][vtx] = mapping.get(vp[vtx], default_shape)

		return None, None

	def _vp_shape_from_boolean_properties(self, gv, props, *_, vp_names: list, mapping: dict, default_shape: str, **kwargs):
		''' Assigns a shape depending on a list of boolean vertex property names -- one-hot encoded via the mapping dict.'''

		vprops 	= props['vprops']

		if 'shape' in vprops: # FIXME need to check if property map!
			log.debugv("shape vprop already present -- overwrites might occur.")
		else:
			vprops['shape'] = gv.new_vp('string')

		# Gather vertex properties
		vps 	= {}
		for vp_name in vp_names:
			vps[vp_name] 	= gv.vertex_properties[vp_name]

		for vtx in gv.vertices():
			for vp_name, vp in vps.items():
				if vp[vtx]:
					vprops['shape'][vtx] = mapping.get(vp_name, default_shape)
					break # -> takes the first fitting one
			else:
				# Not fitting to any vertex property
				vprops['shape'][vtx] = default_shape

		return None, None

	def _vp_size_from_property(self, gv, props, *_, vp_name: str, filter_vp: str=None, filter_inverted: bool=False, map_func: str=None, min_size: float=1., **kwargs):

		# Apply a filter
		if filter_vp:
			gv 	= gt.GraphView(gv) 	# use a new GraphView
			gv.set_vertex_filter(gv.vp[filter_vp], inverted=filter_inverted)

		if map_func:
			map_func 	= eval(map_func) # TODO is there a better way?
		else:
			map_func 	= lambda x: x

		# Get the desired vertex property map
		vp = gv.vp[vp_name]

		# Check if the vsize property map is already present
		if 'vsize' not in props:
			# Create new property map
			log.debugv("No property map for vertex size was given. Creating one ...")
			props['vsize'] = gv.new_vp('float', val=min_size)

		elif isinstance(props['vsize'], (float, int)):
			log.debugv("Constant vertex size was already set -- will be used as the default value for the property map.")
			props['vsize'] = gv.new_vp('float', val=props['vsize'])

		# Apply the map_func to the vsize property map
		for vtx in gv.vertices(): # TODO vectorize!
			props['vsize'][vtx] = max(min_size, map_func(vp[vtx]))

		return None, None

	def _vp_size_if_property(self, gv, props, *_, vp_name: str, filter_vp: str=None, filter_inverted: bool=False, true_size: float=None, false_size: float=None):

		# Apply a filter
		if filter_vp:
			gv 	= gt.GraphView(gv) 	# use a new GraphView
			gv.set_vertex_filter(gv.vp[filter_vp], inverted=filter_inverted)

		# Get the desired vertex property map
		vp = gv.vp[vp_name]

		# Check if the vsize property map is already present
		if 'vsize' not in props:
			# Create new property map
			log.debugv("No property map for vertex size was given. Creating one with default size 1 ...")
			props['vsize'] = gv.new_vp('float', val=1.)

		elif isinstance(props['vsize'], (float, int)):
			log.debugv("Constant vertex size was already set -- will be used as the default value for the property map.")
			props['vsize'] = gv.new_vp('float', val=props['vsize'])

		# Apply to the vsize property map
		for vtx in gv.vertices():
			if bool(vp[vtx]):
				if true_size:
					props['vsize'][vtx] = true_size
			else:
				if false_size:
					props['vsize'][vtx] = false_size

		return None, None

	# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def _ep_default(self, gv, props, *_, **kwargs):
		return None, None

	def _ep_width_from_property(self, gv, props, *_, ep_name: str, filter_ep: str=None, filter_inverted: bool=False, map_func: str=None, min_width: float=1., **kwargs):
		''' Maps an edge property map to edge width.'''

		# Apply a filter
		if filter_ep:
			gv 	= gt.GraphView(gv) 	# use a new GraphView
			gv.set_edge_filter(gv.ep[filter_ep], inverted=filter_inverted)

		if map_func:
			map_func 	= eval(map_func) # TODO is there a better way?
		else:
			map_func 	= lambda x: x


		# Get the desired vertex property map
		ep = gv.ep[ep_name]

		# Get the target property map
		eprops 	= props['eprops']

		# Check if the vsize property map is already present
		if isinstance(eprops['penwidth'], float): # FIXME need to check if property map!
			log.debugv("Constant edge width was already set -- will be overwritten ...")

		eprops['penwidth'] = gv.new_ep('float')

		# Apply the map_func to the vsize property map
		for edge in gv.edges(): # TODO vectorize!
			eprops['penwidth'][edge] = max(min_width, map_func(ep[edge]))

		return None, None

	def _ep_arrhead_from_property(self, gv, props, *_, ep_name: str, scale: str='lin', **kwargs): #TODO

		raise NotImplementedError

	def _ep_color_from_property(self, gv, props, *_, ep_name: str, prop_name: str='color', filter_ep: str=None, filter_inverted: bool=False, cmap_name: str='viridis', scale: str='lin', extend: bool=True, sup_color: str=None, sub_color: str=None, val_range: tuple=None, **kwargs):

		# Apply a filter
		if filter_vp:
			gv 	= gt.GraphView(gv) 	# use a new GraphView
			gv.set_edge_filter(gv.ep[filter_vp], inverted=filter_inverted)

		# Get the edge property map
		try:
			ep = gv.ep[ep_name]
		except KeyError:
			raise KeyError("No such Edge PropertyMap '{}' in the selected GraphView! Check the plotting style (e.g., efuncs) against your network edge properties whether the correct ep_name argument is set.".format(ep_name))

		# Create the colormap
		cmap 	= mpl.cm.get_cmap(cmap_name)
		if extend:
			if sup_color:
				cmap.set_over(mpl.colors.hex2color(sup_color))

			if sub_color:
				# FIXME set_under is somehow not applied
				cmap.set_under(mpl.colors.hex2color(sub_color))

		# Which range should be used for the colormap?
		if val_range is None:
			# Determine the data range of the vertex property and use those values
			rg 		= [np.min(ep), np.max(ep)]
		else:
			# use the passed values
			rg 		= val_range

		# Set the scale
		if scale in ['lin', 'linear']:
			cnorm 	= mpl.colors.Normalize(vmin=rg[0], vmax=rg[1])
		elif scale in ['log', 'logarithmic']:
			cnorm 	= mpl.colors.LogNorm(vmin=rg[0], vmax=rg[1])
		elif scale in ['symlog']:
			raise NotImplementedError("Symlog color normalisation not implemented yet.")
		else:
			log.warning("No scale %s implemented in _vp_color_from_property! Falling back to linear...", scale)
			cnorm 	= mpl.colors.Normalize(vmin=rg[0], vmax=rg[1])

		# Check if the property map is already present
		eprops 	= props['eprops']
		if prop_name in eprops: # FIXME need to check if property map!
			log.debugv("eprop %s already present -- overwrites might occur, if edge filters overlap.", prop_name)
		else:
			eprops[prop_name] = gv.new_ep('string')

		# Apply the colormap to the corresponding property map
		for edge in gv.edges():
			eprops[prop_name][edge] = mpl.colors.rgb2hex(cmap(cnorm(ep[edge])))

		# Return everything
		return cmap, cnorm

	def _ep_color_if_property(self, gv, props, *_, ep_name: str, prop_name: str='color', filter_ep: str=None, filter_inverted: bool=False, true_color: str=None, false_color: str=None):

		# Apply a filter
		if filter_ep:
			gv 	= gt.GraphView(gv) 	# use a new GraphView
			gv.set_edge_filter(gv.ep[filter_ep], inverted=filter_inverted)

		# Get the vertex property map
		try:
			ep = gv.ep[ep_name]
		except KeyError:
			print(gv, gv.ep)
			raise KeyError("No such Edge PropertyMap '{}' in the selected GraphView! Check the plotting style (e.g., efuncs) against your network edge properties whether the correct ep_name argument is set. Available PropertyMaps: {}".format(ep_name, [k for k in gv.ep.keys()]))
		else:
			log.debugv("Edge PropertyMap '%s':\n  %s", ep_name, ep.ma)

		# Check if the fillcolor property map is already present
		eprops 	= props['eprops']
		if prop_name in eprops: # FIXME need to check if property map!
			log.debugv("eprop %s already present -- overwrites might occur, if vertex filters overlap.", prop_name)
		else:
			eprops[prop_name] = gv.new_ep('string')

		# Apply the values to the property map
		for edge in gv.edges():
			if bool(ep[edge]):
				if true_color:
					eprops[prop_name][edge] = true_color
			else:
				if false_color:
					eprops[prop_name][edge] = false_color

		# Return everything
		return None, None

	# Plotting helpers . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def _color_from_vp(self, gv, props, vp, *_, prop_name: str='fillcolor', cmap_name: str='viridis', scale: str='lin', extend: bool=True, sup_color: str=None, sub_color: str=None, val_range: tuple=None):

		# Create the colormap
		cmap 	= mpl.cm.get_cmap(cmap_name)
		if extend:
			if sup_color:
				cmap.set_over(mpl.colors.hex2color(sup_color))

			if sub_color:
				cmap.set_under(mpl.colors.hex2color(sub_color))

		# Which range should be used for the colormap?
		if val_range is None:
			# Determine the data range of the vertex property values and use those values
			rg 		= [float(np.min(vp.ma)), float(np.max(vp.ma))]

		else:
			# can be a list, tuple, or single value (which will then be the central value)
			if isinstance(val_range, (list, tuple)):
				# use the passed values if present, else look for min or max
				rg_min, rg_max 	= val_range

				if rg_min is None:
					rg_min 	= float(np.min(vp.ma))
				if rg_max is None:
					rg_max 	= float(np.max(vp.ma))
			else:
				# use the passed value as central value, determine upper and lower bounds using the differences.
				mid_val 	= val_range
				min_val 	= float(np.min(vp.ma))
				max_val		= float(np.max(vp.ma))
				max_diff 	= max(mid_val-min_val, max_val-mid_val)
				rg_min 		= mid_val - max_diff
				rg_max 		= mid_val + max_diff

			rg 		= (rg_min, rg_max)

		if not rg[0] <= rg[1]:
			# Invalid range, set to some value
			log.caution("Invalid range %s for %s. Setting to interval of length 1.", rg, self.__class__.__name__)
			rg = (rg[0], rg[0] + 1)

		log.debugv("Determined value range to: %s", rg)

		# Set the scale
		if scale in ['lin', 'linear']:
			cnorm 	= mpl.colors.Normalize(vmin=rg[0], vmax=rg[1])
		elif scale in ['log', 'logarithmic']:
			cnorm 	= mpl.colors.LogNorm(vmin=rg[0], vmax=rg[1])
		elif scale in ['symlog']:
			raise NotImplementedError("Symlog color normalisation not implemented yet.")
		else:
			log.warning("No scale %s implemented in _vp_color_from_property! Falling back to linear...", scale)
			cnorm 	= mpl.colors.Normalize(vmin=rg[0], vmax=rg[1])

		# Check if the fillcolor property map is already present
		vprops 	= props['vprops']
		if prop_name in vprops: # FIXME need to check if property map!
			log.debugv("vprop %s already present -- overwrites might occur, if vertex filters overlap.", prop_name)

			if isinstance(vprops[prop_name], str):
				# Carry over previous value
				vprops[prop_name] = gv.new_vp('string', val=vprops[prop_name])
		else:
			vprops[prop_name] = gv.new_vp('string')

		# Apply the colormap to the corresponding property map
		for vtx in gv.vertices():
			vprops[prop_name][vtx] = mpl.colors.rgb2hex(cmap(cnorm(vp[vtx])))

		# Return everything
		return cmap, cnorm

# Helpers - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

def select_subgraph(g: gt.Graph, *, vtcs: list, add_neighbors: bool=False) -> gt.GraphView:
	''' Given a Graph and a list of vertices, a GraphView with the selected vertices is returned.'''

	log.debug("Setting GraphView to subgraph with %d vertices.", len(vtcs))

	gv 	= gt.GraphView(g)

	subgraph_vtx 	= gv.new_vertex_property('bool', val=False)
	for vtx in vtcs:
		subgraph_vtx[vtx] = True

	if add_neighbors:
		# Add the neighboring vertices to the subgraph
		log.debug("Adding neighbors to subgraph ...")
		for vtx in vtcs:
			for neighbor_vtx in gv.vertex(vtx).all_neighbors():
				subgraph_vtx[neighbor_vtx] = True

	gv.set_vertex_filter(subgraph_vtx)

	return gv

# -----------------------------------------------------------------------------

class NetworkObject:
	''' A NetworkObject can be associated with a network vertex or edge and performs actions originating from the vertex or edge. It should be used as a object that works _on_ network property maps, and ideally has no attributes on its own but only methods.

	NOTE IMPORTANT: NetworkObjects require that the passed Edge or Vertex descriptors do not become invalidated, which can happen by removing a vertex or an edge from the graph.'''

	__slots__ 	= ('g', 'desc', '_pdict')
	_properties	= () # the properties this NetworkObject manages explicitly via @property decorators -- should be set in a subclass that actually implements some things as properties.

	def __init__(self, *, g: Network, desc=None):
		'''Initialise a NetworkObject on Graph g and for Descriptor desc. If no descriptor is given, the NetworkObject attaches to the graph's property map.'''
		log.debug("Initialising %s ...", self.__class__.__name__)

		self.g				= g 	# The graph
		self.desc 			= desc 	# The associated descriptor object
		# self.follow_link	= follow_link # Whether to follow a linked graph

		if isinstance(desc, gt.Vertex):
			self._pdict = self.g.vp
		elif isinstance(desc, gt.Edge):
			self._pdict = self.g.ep
		elif desc is None:
			self._pdict = self.g.gp
			self.desc 	= None 	# Cannot use index
		else:
			raise TypeError("Invalid type for argument 'desc'. Needs to be gt.Vertex, gt.Edge, or None, was "+str(type(self.desc)))


	def __getitem__(self, prop_name: str):
		''' Returns the value of the property with the given name from the property map.'''
		try:
			if self.desc is not None:
				# Is a vertex or edge property map -> get via descriptor
				return self._pdict[prop_name][self.desc]
			else:
				# Is a graph property map -> no descriptor specified
				return self._pdict[prop_name]

		except KeyError:
			log.error("No such PropertyMap '%s' defined for this %s.", prop_name, self.__class__.__name__)
			if DEBUG:
				raise

	def __setitem__(self, prop_name: str, val):
		''' Sets the value of this property name for this network object.'''
		try:
			if self.desc is not None:
				self._pdict[prop_name][self.desc] 	= val
			else:
				self._pdict[prop_name] 	= val

		except KeyError:
			log.error("No such PropertyMap '%s' defined for this %s.", prop_name, self.__class__.__name__)
			if DEBUG:
				raise

	def __getattr__(self, prop_name: str):
		'''This gets only called, if no attribute could be acquired via __getattribute__; forward it to getitem...'''
		try:
			return self[prop_name]
		except:
			log.error("Failed getting attribute %s in %s", prop_name, self.__class__.__name__)
			if DEBUG:
				raise
		# NOTE will lead to an recursion error for an attribute that is not defined in __slots__

	def __setattr__(self, prop_name: str, val):

		if prop_name in self.__slots__ or prop_name in self._properties:
			# Set via the regular attribute setter or via defined properties
			super().__setattr__(prop_name, val)

		else:
			# Forward to setitem
			self[prop_name] 	= val

	def __getstate__(self):
		d = {}
		for s in self.__slots__:
			d[s] = getattr(self, s)
		return d

	def __setstate__(self, d):
		# Set the absolutely required slots first.
		self.desc 	= d.pop('desc')
		self.g 		= d.pop('g')
		self._pdict	= d.pop('_pdict')

		for key, val in d.items():
			setattr(self, key, val)

class VertexObject(NetworkObject):
	''' A NetworkObject that is restricted to being on a vertex.'''

	__slots__ 	= ('g', 'desc', '_pdict', 'idx', '_linked')

	# For all but the following value types, the _getitemfast method is used
	VTYPES_FROM_PMAP = ['object', 'string', 'vector<string>']

	def __init__(self, *nwo_args, linked_vtcs: list=None, **nwo_kwargs):
		''' Initialises a VertexObject.

		The linekd_vtcs argument is a list of 2-tuples with networks as the first element and corresponding descriptors as the second. The networks defined here are checked after the attributes and the primary network is checked.
		'''
		super().__init__(*nwo_args, **nwo_kwargs)

		# From the Vertex descriptor, get the index
		self.idx = int(self.desc)

		# If a linked vertex is given, initialise the linked attribute, which reads in the linekd_vtcs argument and creates the necessary entries.
		if linked_vtcs:
			self._linked 	= []

			# Loop over the argument and extract property map dicts and descs and indices with which the links are added
			for _g, _desc in linked_vtcs:
				self.add_link(g=_g, desc=_desc)

			log.debug("Updated the linked vertices.")

		else:
			self._linked 	= []

	# Speed improvements for VertexObject .....................................
	# Access items not via their vertex descriptor, but by the

	def __getitem__(self, prop_name: str):
		''' Returns the value of the property with the given name from the property map.'''
		try:
			return self._getitemfast(prop_name)

		except KeyError:
			# There is no such key and no other links to check -> desired to inform about the error
			log.error("No such PropertyMap '%s' defined for this %s.", prop_name, self.__class__.__name__)
			if DEBUG:
				raise

	def _getitemfast(self, prop_name: str):
		''' Returns the item with the given name either via the PorpertyMap object of graph_tool or (which is faster) via the underlying numpy array.'''
		if self._pdict[prop_name].value_type() in self.VTYPES_FROM_PMAP:
			# need to use the vertex descriptor
			return self._pdict[prop_name][self.desc]
		else:
			# may use the data array and the index, which is slightly faster
			return self._pdict[prop_name].a[self.idx]

	def __setitem__(self, prop_name: str, val):
		''' Sets the value of this property name for this network object.'''
		try:
			self._setitemfast(prop_name, val)

		except KeyError:
			# There is no such key and no other links to check -> inform
			log.error("No such PropertyMap '%s' defined for this %s.", prop_name, self.__class__.__name__)
			if DEBUG:
				raise

		except IndexError as err:
			log.error("IndexError: %s\nNo such index in PropertyMap '%s' for this %s.", err, prop_name, self.__class__.__name__)
			if DEBUG:
				raise

	def _setitemfast(self, prop_name: str, val):
		''' Sets an item via the property map or via the underlying numpy array, depending on the value type.'''
		if self._pdict[prop_name].value_type() in self.VTYPES_FROM_PMAP:
			# need to use the vertex descriptor
			self._pdict[prop_name][self.desc] = val
		else:
			# may use the data array and the index, which is slightly faster
			self._pdict[prop_name].a[self.idx] = val

	def __setstate__(self, d):
		# Set the absolutely required slots first.
		self.desc 	= d.pop('desc')
		self.idx 	= d.pop('idx')
		self.g 		= d.pop('g')
		self._pdict	= d.pop('_pdict')
		self._linked= d.pop('_linked')

		for key, val in d.items():
			setattr(self, key, val)

	# Graph-related ...........................................................
	# Mapping some methods to the graph for easier handling

	def in_degree(self, efilt: bool=None, inverted: bool=False):
		'''Returns the in-degree of the vertex. If an edge filter is given, it is applied to the edges.'''
		if not efilt:
			return self.desc.in_degree()

		raise NotImplementedError("efilt for in_degree")

	def out_degree(self, efilt: bool=None, inverted: bool=False):
		'''Returns the out-degree of the vertex. If an edge filter is given, it is applied to the edges.'''
		if not efilt:
			return self.desc.out_degree()

		raise NotImplementedError("efilt for out_degree")

	# FIXME problem with select_from option and edge filters?!
	def in_edges(self, select_from: str=None, with_idx: bool=False):
		if not select_from:
			if not with_idx:
				return self.desc.in_edges() # returns 2-tuple
			return self.g.get_in_edges(self.idx) # returns 3-tuple
		
		# First select from the graph view, then generate the iterator
		# CAREFUL: returns a 3-tuple iterator, not 2-tuple as above!
		return self.g.gvs[select_from].get_in_edges(self.idx)

	def out_edges(self, select_from: str=None, with_idx: bool=False):
		if not select_from:
			if not with_idx:
				return self.desc.out_edges() # 2-tuple iterator
			return self.g.get_out_edges(self.idx) # 3-tuple iterator
	
		# First select from the graph view, then generate the iterator
		# CAREFUL: returns a 3-tuple iterator, not 2-tuple as above!
		return self.g.gvs[select_from].get_out_edges(self.idx)

	def in_neighbors(self, select_from: str=None):
		if not select_from:
			return self.desc.in_neighbors()
	
		# First select from the graph view, then generate the iterator
		# CAREFUL: returns a 3-tuple iterator, not 2-tuple as above!
		return self.g.gvs[select_from].get_in_neighbors(self.idx)

	def out_neighbors(self, select_from: str=None):
		if not select_from:
			return self.desc.out_neighbors()
	
		# First select from the graph view, then generate the iterator
		# CAREFUL: returns a 3-tuple iterator, not 2-tuple as above!
		return self.g.gvs[select_from].get_out_neighbors(self.idx)

	# Custom methods

	def get_vobj(self, vtx):
		return self.g.vobjs[vtx]

	def get_edges_by_src(self, src, select_from: str=None):
		''' Looks among the neighbors for the edges that originate at src.'''
		return [(src_vtx, self.desc)
				for src_vtx, _ in self.in_edges(select_from=select_from)
				if src_vtx == src]

	def get_edges_by_target(self, target, select_from: str=None):
		''' Looks among the neighbors for the edges that go towards target.'''
		return [(self.desc, target_vtx)
				for _, target_vtx, _ in self.out_edges(select_from=select_from)
				if target_vtx == target]

	# Regarding links .........................................................

	def _init_new_linked_vtx(self):
		'''Using this method, properties of the linked vertices can be initialised. It is called from add_link.'''
		pass

	def add_link(self, *, g, desc):
		'''Adds a link to this VertexObject'''
		self._linked.append(dict(g=g, pdict=g.vp, desc=desc, idx=int(desc)))

		# Call the initialisation method on this new link
		self._init_new_linked_vtcs()

		log.debug("Added link to %s and initialised it. Now have %d links.", self.__class__.__name__, len(self._linked))
		return

	def get_linked_descs(self) -> list:
		return [d['desc'] for d in self._linked]

	def get_linked_idcs(self) -> list:
		return [d['idx'] for d in self._linked]

	def get_link_item(self, prop_name: str):
		'''Like __getitem__ but for the links.'''

		if not self._linked:
			log.warning("No networks linked to %s '%s'; cannot set link item '%s'.", self.__class__.__name__, self.idx, prop_name)
			return

		for d in self._linked:
			pd, desc, idx = d['pdict'], d['desc'], d['idx']
			if prop_name not in pd.keys():
				continue

			if pd[prop_name].value_type() in self.VTYPES_FROM_PMAP:
				# need to use the vertex descriptor
				return pd[prop_name][desc]
			# may use the data array and the index, which is slightly faster
			return pd[prop_name].a[idx]
		else:
			log.error("No such PropertyMap '%s' defined for this %s's linked networks.", prop_name, self.__class__.__name__)
			if DEBUG:
				raise KeyError(prop_name)

	def set_link_item(self, prop_name: str, val, set_all: bool=True):
		'''Like __setitem__ but for the links.

		For set_all == True, this is asymmetric to __getitem__, which only gets the item of _one_ (the first applicable) linked network.'''

		if not self._linked:
			log.warning("No networks linked to %s '%s'; cannot get link item '%s'.", self.__class__.__name__, self.idx, prop_name)
			return

		i 	= 0

		for d in self._linked:
			pd, desc, idx = d['pdict'], d['desc'], d['idx']

			if prop_name not in pd.keys():
				continue

			if pd[prop_name].value_type() in self.VTYPES_FROM_PMAP:
				# need to use the vertex descriptor
				pd[prop_name][desc] = val
			else:
				# may use the data array and the index, which is slightly faster
				pd[prop_name].a[idx] = val
				# NOTE there can still be an uncaught index error here

			# Set the value. Check the others...
			i += 1
			if not set_all:
				break

		if i == 0:
			log.error("No such PropertyMap '%s' defined for this %s's linked networks.", prop_name, self.__class__.__name__)
			if DEBUG:
				raise KeyError(prop_name)
		else:
			log.debugv("Set value of PropertyMap '%s' in %d linked networks of %s.", prop_name, i, self.__class__.__name__)

	# TODO consider introducing a .link.prop_name syntax for calling get_link_item and set_link_item

class EdgeObject(NetworkObject):
	''' A NetworkObject that is restricted to being on a vertex.'''

	def __init__(self, *nwo_args, **nwo_kwargs):
		super().__init__(*nwo_args, **nwo_kwargs)

	# Graph-related ...........................................................
	# Mapping some methods to the graph for easier handling

	def get_eobj(self, vtx):
		return self.g.eobjs[vtx]


# -----------------------------------------------------------------------------
# Helpers

def add_cbar_to_pdf(target_pdf, *, cnorm, cmap, label: str='', enabled: bool=True, reuse_tmp_cbar: bool=cfg.pdf_cbar.reuse_tmp_cbar, extend: str=cfg.pdf_cbar.extend, scale=cfg.pdf_cbar.scale, dpi=cfg.pdf_cbar.dpi, axpos=cfg.pdf_cbar.axpos): # TODO clean up arguments
	''' Hack your way to a pdf with a colorbar -- for example when a colorbar is wanted in a graphviz plot, where it is not possible to add one via graphviz ...'''

	TMP_CBAR_NAME 	= ".cbar.tmp.pdf"

	if not enabled:
		log.debug("Not adding colorbar.")
		return

	if cnorm is None or cmap is None:
		log.debug("No cnorm or cmap given, cannot add colorbar.")
		return

	log.debugv("Adding colorbar to %s ... (reuse_tmp_cbar: %s)",
	          os.path.basename(target_pdf), str(reuse_tmp_cbar))

	# The target pdf to add the colorbar to
	tpdf 	= PdfFileReader(open(target_pdf, 'rb'))
	graph 	= tpdf.getPage(0)

	# Calculate figsize for the colorbar pdf
	figsize	= (graph.mediaBox[2]/dpi*scale, graph.mediaBox[3]/dpi*scale)
	log.debugv("figsize: %s", figsize)

	# Where to save/read the colorbar from
	cbar_pdf 	= os.path.join(os.path.dirname(target_pdf), TMP_CBAR_NAME)

	# Create temporary colorbar pdf -- if it does not already exist
	if not (reuse_tmp_cbar and os.path.isfile(cbar_pdf)):
		# The file is not there yet or it is supposed to be created again

		# Make a figure and axes with dimensions as desired
		fig 	= plt.figure(figsize=figsize)
		ax 		= fig.add_axes(axpos)

		# Create the colorbar from the norm and cmap given
		cbar 	= mpl.colorbar.ColorbarBase(ax, cmap=cmap, norm=cnorm,
		                                 	orientation='horizontal',
		                     			 	extend=extend)
		cbar.set_label(label)

		# save the colorbar as pdf
		# NOTE crucial to pass bbox, else (if configured in matplotlibrc) it might lead to the colorbar being cropped upon saving by bbox_inches='tight'
		fig.savefig(cbar_pdf, bbox_inches=fig.get_window_extent().transformed(fig.dpi_scale_trans.inverted()))
		plt.close('all')

		log.debug("Created the colorbar.")

	# Open colorbar pdf and prepare output writer
	cpdf 	= PdfFileReader(open(cbar_pdf, 'rb'))
	cbar 	= cpdf.getPage(0)
	output 	= PdfFileWriter()

	# Merge them together
	log.debugv("Merging graph to colorbar ...")
	cbar.mergeScaledPage(graph, scale)
	output.addPage(cbar)

	os.remove(target_pdf) 	# overwriting causes bugs, delete first

	with open(target_pdf, 'wb') as f:
		output.write(f)
		log.debugv("Merged pdf written.")

	if not reuse_tmp_cbar:
		os.remove(cbar_pdf)
		log.debugv("Removed temporary colorbar pdf.")

	return
