"""Defines the Species class

TODO: Implement this.
"""

import abc

import deeevolab as dvl

# Initialise loggers and config file
log = dvl.setup_logger(__name__, level=dvl.get_log_level())
cfg = dvl.get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Local constants
DEBUG = dvl.get_debug_flag()

# -----------------------------------------------------------------------------


class Species(metaclass=abc.ABCMeta):
	"""Class for a species, defined as a collection of agents.

	This is an abstract class and cannot be instantiated itself.

	The way the collection is defined can be adjusted in a child class. Species should be used when a description in terms of population measures rather than single agents is reasonable.
	"""

	__slots__ = ()

	def __init__(self):
		""""""
		pass
