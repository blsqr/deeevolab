"""The deeevolab package supplies a python framework for implementing models,
running simulations, and monitoring their data.

It supplies the basic tools to perform these tasks. See README for more info.

This __init__ module sets up the logging facilities. If only this module is
imported, the logging's verbosity levels etc. can still be fully configured.
This is not the case after other deeevolab modules were imported and their
loggers initialised.
"""

import os
from pkg_resources import resource_filename

from . import shared

# Make some important methods available -- other modules can then import deeevolab and use them this way
from deeevolab.logging import setup_root, setup_logger
from deeevolab.config import get_config, load_cfg_file

# Important: Subpackages are not meant to be imported here!

# Setup the root logger
setup_root()

# Setup logging for this file (inherits from root logger)
log	= setup_logger(__name__)
log.debug("Loaded module and logger.")

log.highlight("Initialising deeevolab package ...")

# Local constants
__version__ = '0.7.0-pre.0'
# NOTE this need always match the version in setup.py

USER_CFG_TEMPLATE = resource_filename('deeevolab', 'user_cfg.yml')
USER_CFG_PATH = os.path.expanduser('~/.config/deeevoLab/user_cfg.yml')

# -----------------------------------------------------------------------------
# Flags
def get_debug_flag() -> bool:
	'''Returns a debug flag depending on the log level'''
	return False if not get_log_level() else bool(get_log_level() <= 10)

# -----------------------------------------------------------------------------
# Log levels
# Globally:
def set_verbosity(v: int):
	if v > 0:
		if v == 1:
			shared.log_level = 20 #'INFO'
			s = 'INFO'
		elif v == 2:
			shared.log_level = 13 #'PROGRESS'
			s = 'PROGRESS'
		elif v == 3:
			shared.log_level = 10 #'DEBUG'
			s = 'DEBUG'
		elif v >= 4:
			shared.log_level = 5 #'DEBUGV'
			s = 'DEBUGV'

		log.note("Set debug log level to:     %s (%d)", s, shared.log_level)

	else:
		shared.log_level = None

def get_log_level(is_universe: bool=False) -> int:
	uni_level = get_universe_log_level()
	log_level = shared.log_level

	if is_universe:
		if uni_level and log_level:
			lvl = min(uni_level, log_level)
		elif uni_level:
			lvl = uni_level
		elif log_level:
			lvl = log_level
		else:
			lvl = None
	else:
		lvl = log_level
	log.debugv("Requested log level (is_universe == %s). Returning %d.", is_universe, lvl)
	return lvl

# For the universe:
def set_universe_verbosity(v: int):
	if v > 0:
		if v == 1:
			shared.uni_log_level = 25 #'HILIGHT'
			s = 'HIGHLIGHT'
		elif v == 2:
			shared.uni_log_level = 15 #'STATE'
			s = 'STATE'
		elif v >= 3:
			shared.uni_log_level = 5 #'DEBUGV'
			s = 'DEBUGV'

		log.note("Set Universe log level to:  %s (%d)", s,shared.uni_log_level)

	else:
		shared.uni_log_level = None

def get_universe_log_level() -> int:
	if hasattr(shared, 'uni_log_level'):
		return shared.uni_log_level
	return None
