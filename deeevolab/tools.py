''' Generally useful functions.'''

# Internal
import os
import sys
import copy
import re
import collections
import json
import yaml
import subprocess
from pprint import PrettyPrinter

import numpy as np

import deeevolab as dvl
from deeevolab.rng import rec_apply

# Setup logging for this file (inherits from root logger)
log	= dvl.setup_logger(__name__, level=dvl.get_log_level())
cfg = dvl.get_config(__name__)
log.debug("Loaded module and logger.")

# Local constants
DEBUG = dvl.get_debug_flag()

# Terminal, TTY-related
IS_A_TTY = sys.stdout.isatty()
try:
	_, TTY_COLS	= subprocess.check_output(['stty', 'size']).split()
except:
	# Probably not run from terminal --> set value manually
	TTY_COLS = 79
else:
	TTY_COLS = int(TTY_COLS)
log.debug("Determined TTY_COLS: %s, IS_A_TTY: %s", TTY_COLS, IS_A_TTY)
NUM_BLOCKED_COLS = cfg.get('num_blocked_cols', 20)
AVAILABLE_COLS = TTY_COLS - NUM_BLOCKED_COLS

# -----------------------------------------------------------------------------
# Dumping and loading yaml

def dump_yaml(d, output: str, cleanup: bool=False):
	''' Save the dict-like d to output as a yaml file.

	Args:
		d : data to dump
		output (str)  : output path of the file; extension will be automatically added, if not present.
	'''

	if output.split(".")[-1] not in ["yaml", "yml"]:
		output 	+= ".yml"

	if cleanup:
		d = cleanup_for_yaml(d)

	with open(output, 'w') as outfile:
		yaml.dump(d, outfile, default_flow_style=False)

def yamlize(d: dict, cleanup: bool=False, prefix: bool=True): # TODO combine with dump?
	''' Returns dict-like data as a yaml-parsed string'''
	if cleanup:
		d 	= cleanup_for_yaml(d)

	if prefix:
		if isinstance(prefix, str):
			_prefix 	= prefix
		else:
			_prefix 	= "#yaml\n"
	else:
		_prefix 	= ""

	return _prefix + yaml.dump(d, default_flow_style=False)

def cleanup_for_yaml(d):
	''' Cleans up dict-like d for output via yaml, i.e. removing unnecessary constructs and casting to base types ...
	'''
	# FIXME
	raise NotImplementedError

	# d = copy.deepcopy(d)
	# d = rec_apply(d, select_f=isinstance, select_args=(DotDict,), apply_f=dict)

	# return d

def load_yaml(path):
	''' Load a yaml file from the given path ...'''
	with open(path, 'r') as f:
		try:
			d = dict(yaml.load(f.read()))
		except TypeError:
			# file was probably empty
			log.warning("Could not load file %s!", path)
			return {}
		else:
			log.debug("Loaded yaml from %s", path)
			return d

# -----------------------------------------------------------------------------
# error handling --------------------------------------------------------------

def handle_exception(*exc_args, colored_tb=True):
	'''Custom exception handler, which is used to set sys.excepthook and automatically logs uncaught exceptions.'''

	if issubclass(exc_args[0], KeyboardInterrupt):
		sys.__excepthook__(*exc_args)
		return

	if not colored_tb or not sys.stdout.isatty():
		# just do the regular logging
		log.error("Uncaught %s!", exc_args[0].__name__, exc_info=exc_args)

	else:
		# For colored traceback
		import traceback

		tb 	= traceback.format_exception(*exc_args)

		try:
			from pygments import highlight
			from pygments.lexers import get_lexer_by_name
			from pygments.formatters import Terminal256Formatter
		except ImportError:
			tb 	= ''.join(tb)
		else:
			TF_BG 			= 'dark' 	# terminal formatter background
			TF_STYLE 		= 'default' # manni,  igor,  lovelace,  xcode,  vim,  autumn,  vs,  rrt,  native,  perldoc,  borland,  tango,  emacs,  friendly,  monokai,  paraiso-dark,  colorful,  murphy,  bw,  pastie,  algol_nu,  paraiso-light,  trac,  default,  algol,  fruity
			lexer 		= get_lexer_by_name("pytb", stripall=True)
			formatter 	= Terminal256Formatter(bg=TF_BG, style=TF_STYLE)
			tb 			= highlight(''.join(tb), lexer, formatter)

		log.error("Uncaught %s!\n\n%s", exc_args[0].__name__, tb)

class try_and_except: # TODO improve this, e.g. by adding traceback
	"""A decorator to encapsulate the decorated function into a try-except block"""

	def __init__(self, ExcCls: Exception=Exception, msg_strf: str=None, reraise: bool=False, callback=None):
		'''If there are decorator arguments, the function
		to be decorated is not passed to the constructor!
		'''
		log.debug("Initialising @try_and_except for %s ...",
		          ExcCls.__name__)

		# Save the Exception class that is to be used
		if (not callable(ExcCls) or
			not isinstance(ExcCls(), (Exception, KeyboardInterrupt))):
			raise TypeError("ExcCls argument needs to be derived from Exception or KeyboardInterrupt, was of type '{}'.".format(type(ExcCls())))
		self.ExcCls 	= ExcCls

		# Handle message format string
		if msg_strf is None:
			msg_strf = "@try_and_except: Exception raised in method {func_name:}!\n\n{exc:}: {err:}\n"

		# Save attributes
		self.msg_strf	= msg_strf
		self.reraise	= reraise
		self.callback 	= callback

		log.debugv("try_and_except decorator initialised.")

	def __call__(self, f):
		''' If there are decorator arguments, __call__() is only called
		once, as part of the decoration process! You can only give
		it a single argument, which is the function object.
		'''
		log.debug("@try_and_except called.")

		def wrapped_f(*args, **kwargs):
			# print("Inside wrapped_f()")
			# print("Decorator arguments:", self.ExcCls, self.msg_strf, self.reraise)
			# print("Exceuting function {} with...\nargs:{}\nkwargs:{}".format(f.__name__, args, kwargs))

			try:
				return f(*args, **kwargs)
			except self.ExcCls as err:
				log.error(self.msg_strf.format(func_name=f.__name__,
											   exc=self.ExcCls.__name__,
											   err=self.ExcCls(err)))
				if self.reraise:
					raise

				if self.callback:
					return self.callback()

		return wrapped_f

# TODO merge this decorator and the above method

# formatting-related ----------------------------------------------------------

def format_time(time_in_s, ms_precision=1):
	''' Helper function to format time in seonds. If ms_precision > 0, decimal places will be shown'''

	divisors    = [24*60*60, 60*60, 60, 1]
	letters     = ['d', 'h', 'm', 's']
	remaining   = float(time_in_s)
	text        = ''

	for divisor, letter in zip(divisors, letters):
		time_to_represent   = int(remaining/divisor)
		remaining           -= time_to_represent * divisor

		if time_to_represent > 0 or len(text):
			if len(text):
				text += ' '

			# Distinguish between seconds and other divisors for short times
			if ms_precision <= 0 or time_in_s > 60:
				# Regular behaviour: Seconds do not have decimals
				text += '{:d}{:}'.format(time_to_represent, letter)
			elif ms_precision > 0 and letter == 's':
				# Decimal places for seconds
				text += '{val:.{prec:d}f}s'.format(val=time_to_represent+remaining, prec=int(ms_precision))
			else:
				raise TypeError('Argument ms_precision was expected to be int, was '+str(type(ms_precision)))

	if len(text) == 0 and ms_precision == 0:
		# Just show an approximation
		text = '< 1s'
	elif len(text) == 0 and ms_precision > 0:
		# Show as decimal with ms_precision decimal places
		text = '{val:{tot}.{prec}f}s'.format(val=remaining, tot=int(ms_precision)+2, prec=int(ms_precision))

	return text

def fill_tty_line(s: str, fill_char: str=" ", align: str="left", num_blocked: int=0) -> str:
	'''If the terminal is a tty, returns a string that fills the whole tty line with the specified fill character.'''
	if not IS_A_TTY:
		return s

	fill_str = fill_char * (TTY_COLS - len(s) - num_blocked)

	if align in ["left", "l", None]:
		return s + fill_str

	elif align in ["right", "r"]:
		return fill_str + s

	elif align in ["center", "centre", "c"]:
		return fill_str[:len(fill_str)//2] + s + fill_str[len(fill_str)//2:]

	else:
		raise ValueError("align argument '{}' not supported".format(align))

def tty_centred_msg(s: str, fill_char: str="·", num_blocked: int=0, spacing: str=" ") -> str:
	'''Shortcut for a common fill_tty_line use case.'''
	return fill_tty_line(spacing + s + spacing, fill_char=fill_char,
	                     align='centre', num_blocked=num_blocked)

def log_centred_msg(s: str, fill_char: str="-", num_blocked: int=NUM_BLOCKED_COLS, spacing: str=" "):
	'''Shortcut for centred message with some columns already reserved by the logging.'''
	return fill_tty_line(spacing + s + spacing, fill_char=fill_char,
	                     align='centre', num_blocked=num_blocked)

# output-related --------------------------------------------------------------

def pprint(d):
	'''Nicely prints a dictionary using json.dump method'''
	try:
		print(json.dumps(d, indent=4, sort_keys=True))
	except:
		# Fallback
		print("(Fallback to print). ", d)

def flushprint(text, end=''):
	''' '''
	print(text, end=end)
	sys.stdout.flush()

def clear_line(only_in_tty=True, break_if_not_tty=True):
	''' Clears the current terminal line and resets the cursor to the first position using a POSIX command.'''
	# http://stackoverflow.com/questions/5419389/how-to-overwrite-the-previous-print-to-stdout-in-python
	if break_if_not_tty or only_in_tty:
		# check if there is a tty
		is_tty = sys.stdout.isatty()

	# Differentiate cases
	if (only_in_tty and is_tty) or not only_in_tty:
		# Print the POSIX character
		print('\x1b[2K\r', end='')

	if break_if_not_tty and not is_tty:
		# print linebreak (no flush)
		print('\n', end='')

	# no linebreak, flush manually
	sys.stdout.flush()

# working on dicts ------------------------------------------------------------

def traverse_dict(d, *keys):
	''' Recursively goes through a dictionary d and traverses along the given keys. Fails, if the key is not present.'''
	if len(keys) > 1:
		return traverse_dict(d[keys[0]], *keys[1:])
	elif len(keys) == 1:
		return d[keys[0]]
	else:
		# no keys passed - have this case for generality
		return d

def rec_update(d, u, update_pspace: bool=False):
	''' Recursively updates the Mapping-like object d with the Mapping-like object u and returns the updated Mapping-like object.

	From: http://stackoverflow.com/a/32357112/1827608
	'''
	for k, v in u.items():
		if isinstance(d, collections.Mapping):
			# Already a Mapping
			if isinstance(v, collections.Mapping):
				# Already a Mapping, continue recursion
				d[k] = rec_update(d.get(k, {}), v)
			else:
				# Not a mapping -> at leaf -> update value
				if update_pspace and isinstance(v, ParamSpace):
					# Update the ParamSpace object with the values from d
					log.debug("Found a ParamSpace object during rec_update.")
					v.update(d[k], recessively=True)
					# ...this is done in the already created ParamSpace object and is done recessively, i.e. if the keys are not already present in the ParamSpace, they will not be added. The already present keys have priority over the ones that are passed for the update

					# Now the updated ParamSpace object needs to be put in place of d[k], which is done outside this if-construct

				# do the regular update step
				d[k] = v 	# ... which is just u[k]

		else:
			# Not a mapping -> create one
			d = {k: u[k]}
	return d

# walking along lists of keys -------------------------------------------------

def get_via_keylist(obj, keylist: list, try_attr: bool=True, try_item: bool=True):
	'''Traverses along the keys in the keylist and returns the value encountered at the end.

	Args:
		obj: 		the object to start traversing from
		keylist: 	the keys to traverse along; if integer or string, the recursion does not start.
		try_attr:	(optional, default True) whether to try to traverse along attributes. This is tried before items.
		try_item: 	(optional, default True) whether to try to traverse along items. This is tried after the attributes were tried.

	Returns:
		the desired object

	Raises:
		KeyError: 	a key in the keylist is not present
	'''

	# Ensure list
	if not isinstance(keylist, list):
		keylist 	= [keylist]

	# Get current key
	key 	= keylist[0]

	# Get the next object
	if try_attr and hasattr(obj, key):
		# Get via attribute
		obj 	= getattr(obj, key)

	elif try_item:
		# Could not get via attribute; try to get via item
		try:
			obj 	= obj[key]

		except KeyError as err:
			raise KeyError("{} has no key '{}'. (try_attr: {}, try_item: {})".format(type(obj), key, try_attr, try_item))

	else:
		raise KeyError("{} has no attribute with the name '{}'. (try_attr: {}, try_item: {})".format(type(obj), key, try_attr, try_item))

	if len(keylist) == 1:
		# End of recursion
		return obj
	else:
		# Continue recursion
		return get_via_keylist(obj, keylist[1:],
		                       try_attr=try_attr, try_item=try_item)

def set_via_keylist(obj, keylist: list, val, try_attr: bool=True, try_item: bool=True):
	'''Traverses along the keys in the keylist and returns the value encountered at the end.

	Args:
		obj: 		the object to start traversing from
		keylist: 	the keys to traverse along; if integer or string, the recursion does not start.
		val: 		the value to set
		try_attr:	(optional, default True) whether to try to traverse along attributes. This is tried before items.
		try_item: 	(optional, default True) whether to try to traverse along items. This is tried after the attributes were tried.

	Raises:
		KeyError: 	a key in the keylist is not present
	'''

	# Ensure list
	if not isinstance(keylist, list):
		keylist 	= [keylist]

	# Get current key
	key 	= keylist[0]

	# Traverse and set item or continue recursively
	if try_attr and hasattr(obj, key):
		# Continue via attribute
		if len(keylist) > 1:
			# Continue recursion
			set_via_keylist(getattr(obj, key), keylist[1:], val,
			                try_attr=try_attr, try_item=try_item)
		else:
			# End of recursion -> set attribute
			setattr(obj, key, val)

	elif try_item:
		# Could not continue via attribute; try to continue via item
		try:
			if len(keylist) > 1:
				# Continue recursion
				set_via_keylist(obj[key], keylist[1:], val,
				                try_attr=try_attr, try_item=try_item)
			else:
				# End of recursion -> set item
				obj[key] 	= val


		except KeyError as err:
			raise KeyError("{} has no key '{}'. (try_attr: {}, try_item: {})".format(type(obj), key, try_attr, try_item))

	else:
		raise KeyError("{} has no attribute with the name '{}'. (try_attr: {}, try_item: {})".format(type(obj), key, try_attr, try_item))

# misc ------------------------------------------------------------------------

def generate_setlist(*_, range_args=None, add=None, remove=None) -> list:
	''' Generates a set list, i.e. one without duplicate entries'''
	if range_args:
		l = list(range(*range_args))
	else:
		l = []

	if isinstance(add, (list, tuple)):
		l += list(add)
	elif isinstance(add, (int, float)):
		l += [add]

	# Remove duplicates
	l 	= list(set(l))

	# Remove items
	if isinstance(remove, (list, tuple)):
		remove_list 	= list(set(remove))
		for n in remove_list:
			if n in l:
				l.remove(n)
	elif isinstance(remove, (int, float)):
		while n in l:
			l.remove(remove)

	return l

def flatten(d: dict, use_repr: bool=False, enumerate_lists: bool=True, allow_numbers: bool=True):
	''' Recursively moves through a dict-like object and calls repr() or str() on all elements.

	# TODO write documentation
	'''

	new_d = copy.deepcopy(d)

	# Loop over keys in the dictionary
	for key in new_d:
		if isinstance(new_d[key], dict):
			# Is a dict --> recursively go through the elements inside
			new_d[key] 	= flatten(d[key], use_repr=use_repr, enumerate_lists=enumerate_lists, allow_numbers=allow_numbers)

		elif enumerate_lists and isinstance(new_d[key], list):
			new_d[key] 	= {k:v for k,v in enumerate(new_d[key])}
			# Is now a dict --> recursively go through the elements inside
			new_d[key] 	= flatten(new_d[key], use_repr=use_repr, enumerate_lists=enumerate_lists, allow_numbers=allow_numbers)

		elif allow_numbers and isinstance(new_d[key], (int, float)):
			# the key stays as it is
			pass

		else:
			# Not a dict and not a number, try to serialize
			try:
				new_d[key]  = repr(d[key]) if use_repr else str(d[key])
			except:
				raise ValueError('Not serialisable element of type {}.'.format(type(d[key])))

	return new_d

def purge(path, pattern):
	''' Removes all files matching pattern in directory path.'''
	try:
		files 	= os.listdir(path)
	except FileNotFoundError:
		log.debug("Directory %s not present for purge. Not purging.")
		return

	for f in files:
		if re.search(pattern, f):
			os.remove(os.path.join(path, f))

def isclose(a, b, rel_tol=1e-6, abs_tol=0.0):
	if np.isnan(a) or np.isnan(b) or a is None or b is None:
		return False
	else:
		return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

# make a PrettyPrinter available ----------------------------------------------

PP = PrettyPrinter(indent=2, width=AVAILABLE_COLS)
