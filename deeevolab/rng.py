''' The RNG class can be used to generate random numbers, e.g. for initialising classes ...'''

import copy
import random

import numpy as np

from deeevolab.logging import setup_logger

# Get logger
log = setup_logger(__name__)
log.debug("Loaded module and logger.")

# NOTE there is no config variable available in this module

# -----------------------------------------------------------------------------
# TODO move this out of deeevoLab and into an own package: GenTools
# FIXME this is a bit overcomplicated ... not the nicest python.

class BaseGen:

	def __init__(self, *, enabled: bool=True, resolvable: bool=True, default=None, **gen_kwargs):

		self.enabled 		= enabled
		self.resolvable		= resolvable
		self.gen_kwargs		= gen_kwargs
		self._gen			= None
		self.default 		= default

		if self.default is None:
			log.caution("No default value given for %s! Will always try to return from the list, regardless of enabled state.", self.__class__.__name__)
			self.enabled 		= True

		log.debug("Initialising generator ...")
		self._init_gen()

	def _init_gen(self):
		'''Initialise the generator.'''
		pass

	@property
	def gen(self):
		'''Needs to return a callable!'''
		return self._gen

	def __call__(self):
		if self.enabled:
			val = self.gen()
			log.state("Generated value:  %s", val)
			return val
		else:
			return self.default

	def resolve(self):
		if self.resolvable:
			return self.__call__()
		else:
			# Resolving disabled, just return the BaseGen object itself
			return self

class ListGen(BaseGen):
	'''Generates list and can return individual elements from that list.'''

	@property
	def gen(self):
		'''Needs to return a callable!'''
		return self._gen.__next__

	def __call__(self):
		try:
			return super().__call__()
		except StopIteration:
			# The iterator ran out; create a new one and try again
			self._init_gen()

			try:
				return super().__call__()
			except StopIteration as err:
				# The generated list was empty
				raise ValueError("The generated list was empty! Generator kwargs: "+str(self.gen_kwargs)) from err

	def _init_gen(self):
		'''Create an iterator over the generated list.'''
		self._gen 	= iter(listgen(**self.gen_kwargs))

	def resolve_completely(self):
		return listgen(**self.gen_kwargs)

class RNG(BaseGen):

	def __call__(self, **kwargs):
		if self.enabled:
			if kwargs:
				# Also take the additional kwargs into account
				kwargs.update(self.gen_kwargs)
				# TODO shouldn't this be the other way around?!
			else:
				kwargs 	= self.gen_kwargs

			val = self.gen(**kwargs)
			log.state("Generated value:  %s", val)
			return val
		else:
			return self.default

	def _init_gen(self):
		self._gen 	= generate_random_val


# Custom methods ..............................................................
def switch_with_prob(old_val: bool, p_switch: float) -> bool:
	switch 	= random.random() < p_switch
	return old_val if not switch else (not old_val)

# .............................................................................

def generate_random_val(*, dist: str, prng=None, dist_args: list=None, mod: float=None, factor: float=None, offset: float=None, min_val: float=None, max_val: float=None, p_true: float=None, as_int: bool=False, **dist_kwargs):
	''' Generate a random value using a given distribution and then applying some boundary conditions.

	The arguments are applied in the order of appearance.

	Args:
		dist (str) 		: distribution name (must be in DISTS or numpy.random)
		prng 			: the PRNG object to use instead
		dist_args  		: get passed to distribution method
		dist_kwargs		: get passed to distribution method
		mod (float)     : a modulo to apply
		factor (float) 	: by which the result is multiplied
		offset (float) 	: that is added (after multiplication)
		min_val (float) : minimum value possible
		max_val (float) : maximum value possible
		p_true (float) 	: will result in return value being boolean: True if val < p_true (evaluated just before returning)
		as_int (bool) 	: whether to cast to int (evaluated before returning, if p_true is not set)
	'''
	if not dist_args:
		dist_args = []

	if prng:
		dist_func 	= getattr(prng, dist)
	elif dist in DISTS:
		dist_func 	= DISTS[dist]
	else:
		# Take from np.random module
		dist_func 	= getattr(np.random, dist)

	# Get the random value
	val 		= dist_func(*dist_args, **dist_kwargs)

	# Apply modulo
	if mod is not None:
		val %= mod

	# Multiply and add offset
	if factor is not None:
		val	*= factor

	if offset is not None:
		val += offset

	# Check range
	if min_val is not None and max_val is not None and min_val > max_val:
		raise ValueError("Arguments min_val and max_val not specified correctly: {} > {}".format(min_val, max_val))

	if min_val is not None and val < min_val:
		val 	= min_val
	if max_val is not None and val > max_val:
		val 	= max_val

	# Cast to bool or int and return
	if p_true is not None:
		val 	= bool(val < p_true)
	elif as_int:
		val 	= int(val)

	return val
DISTS = {
	# Map some distributions to the random module (faster than np.random)
	'rand': 			random.random,
	'random': 			random.random,
	'randint': 			random.randint,
	'uniform': 			random.uniform,
	'normal': 			random.gauss,
	'gauss': 			random.gauss,
	'lognorm': 			random.lognormvariate,
	'lognormal':		random.lognormvariate,
	'exp': 				random.expovariate,
	'exponential': 		random.expovariate,
	'pareto': 			random.paretovariate,
	# Include some custom methods
	'switch_with_prob': switch_with_prob
	# if the distribution is not found in here, np.random is searched
}

# .............................................................................

def listgen(*, mode: str=None, args: list=None, add: list=None, remove: list=None, remove_duplicates: bool=False) -> list:
	''' Generates a list from the given parameters.'''
	if mode in ['range', 'linspace', 'logspace'] and not args:
		raise ValueError("No args given for mode "+mode)

	if mode in ['range']:
		l = range(*args)
	elif mode in ['linspace']:
		l = np.linspace(*args)
	elif mode in ['logspace']:
		l = np.logspace(*args)
	else:
		l = []

	# Make sure it is a list:
	l = list(l)

	# Add elements
	if isinstance(add, (list, tuple)):
		l += list(add)
	elif isinstance(add, (int, float)):
		l += [add]

	# Remove items
	if isinstance(remove, (list, tuple)):
		remove_list 	= list(set(remove))
		for n in remove_list:
			if n in l:
				l.remove(n)
	elif isinstance(remove, (int, float)):
		while n in l:
			l.remove(remove)

	# Remove duplicates
	if remove_duplicates:
		# NOTE this will also lead to sorting!
		l 	= list(set(l))

	return l

# .............................................................................

def rec_apply(itr, *, select_f, apply_f, select_args=None, apply_args=None, apply_to_copy: bool=True):
	''' Update iterator-like d with values from Mapping u

	If `apply_to_copy`, the apply function is called on the original dictionary but the resulting value is applied to a copy that is carried along ...'''

	select_args 	= [] if select_args is None else select_args
	apply_args 		= [] if apply_args is None else apply_args

	if apply_to_copy:
		itr_copy 		= copy.copy(itr)
		# NOTE Important to do a shallow copy at each recursion level; this allows applications that destroy the selected object (but depend on that object's state) to be carried out on each level of the recursion.

	if isinstance(itr, (list, tuple)):
		_itr 	= enumerate(itr)
	elif isinstance(itr, dict):
		_itr 	= itr.items()
	else:
		# is a single value -> nothing to do here -> return directly
		return itr

	for k, v in _itr:
		if select_f(v, *select_args):
			# is the desired element -> apply function
			# print("Found one!", v)
			# Generate new value
			new_val 	= apply_f(itr[k], *apply_args)

			# Distinguish between application to the iterator or to the copy
			if not apply_to_copy:
				# Apply the value directly to itr
				itr[k] 		= new_val
			else:
				# Generated the value using itr but applying it to the copy
				itr_copy[k]	= new_val

			# Whether recursion should continue is checked on the new value
			rec_val = new_val

		else:
			# Recursion continue on v, as no changes were made to it
			rec_val = v

		if isinstance(rec_val, (list, tuple, dict)):
			# can be recursed further, either on the copy or the original
			rec_val	= rec_apply(rec_val,
			                    select_f=select_f,
			                    apply_f=apply_f,
			                    select_args=select_args,
			                    apply_args=apply_args,
			                    apply_to_copy=apply_to_copy)

			if not apply_to_copy:
				itr[k]		= rec_val
			else:
				itr_copy[k] = rec_val

	# Return either the copied dict or the original
	if apply_to_copy:
		return itr_copy
	else:
		return itr

def resolve_gen(d):
	''' Recursively goes through a dictionary and -- when encountering BaseGen objects -- resolves them, i.e. makes them return a value and puts this value in their place.'''
	return rec_apply(d,
	                 select_f=isinstance, select_args=(BaseGen,),
	                 apply_f=lambda x: x.resolve(), apply_to_copy=True)

def call_if_gen(obj, name=None):
	''' If object o is callable, calls it, else returns o.'''

	if callable(obj):
		log.debugv("Determining %s using RNG...", name if name else "variable")
		return obj()
	else:
		return obj

def call_if_rng(*args, **kwargs):
	return call_if_gen(*args, **kwargs)
