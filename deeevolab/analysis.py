''' '''


import copy
from collections import defaultdict

import deeevolab as dvl

# Setup logging for this file (inherits from root logger)
log		= dvl.setup_logger(__name__, level=dvl.get_log_level())
cfg 	= dvl.get_config(__name__)
log.debug("Loaded module and logger.")

# Local constants
DEBUG 	= dvl.get_debug_flag()

# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# CYCLE DETECTION -------------------------------------------------------------
# -----------------------------------------------------------------------------

def find_cycles(graph, convert_back: bool=False, max_stack_len: int=None):
	''' Returns a list of cycles in the graph found using the Hawick-James Algorithm.

	Args:
		graph (graph_tool.Graph) : on which graph the cycle should be searched

		convert_back (bool, optional) : whether to convert the vertex indices back to vertex objects # TODO Implement

		max_stack_len (int, optional) : whether to limit the stack length (limiting detectable cycles to those up to this length)

	Returns:
		list : of cycles, where each vertex is identified by their vertex index
	'''

	def unblock(vtx):
		''' Unblocks the vertex with the passed id. Additionally, it goes through B and unblocks all those in the chain ...'''
		blocked[vtx] = False

		while len(B[vtx]) > 0:
			w = B[vtx][0] # start with first val ... (does not matter too much)
			B[vtx] = [v for v in B[vtx] if v != w]
			if blocked[w]:
				unblock(w)

	def find_cycle(graph, vtx):
		''' The recursively called method to find circuits in the graph, starting from vtx.

		Shared variables: graph, B, accs, start_vtx, stack
		'''

		# Set flag to show whether a circuit was found in this recursion
		circuit_found 	= False

		# Put the current vertex onto the stack and set it blocked
		stack.append(vtx)
		blocked[vtx] 	= True

		# Compile list of target nodes (for speedup, because needed twice)
		out_neighbors = [graph.vertex_index[i] for i in graph.vertex(vtx).out_neighbors()]

		# Iterate over target nodes, which are the outgoing edges of this vertex
		for target_vtx in out_neighbors:
			if max_stack_len and len(stack) > max_stack_len:
				# do not continue with the recursion
				break # -> find circuit will not be called again

			if target_vtx < start_vtx:
				# The circuits that are not looked at here will be found from a different start node. Not having this leads to shifted duplicates of the same circuits
				continue

			if target_vtx == start_vtx:
				# We have a circuit!
				circuit_found = True
				accs.append(copy.copy(stack))

			elif not blocked[target_vtx]: # --> target > start == True
				# This node is not blocked -> recursively continue the search
				if find_cycle(graph, target_vtx):
					circuit_found = True

		# Distinguish into the cases where circuit was found and not
		if circuit_found:
			unblock(vtx)
			# ...such that it can be included in other circuits

		else:
			# iterate over all target nodes
			for target_vtx in out_neighbors:
				if target_vtx < start_vtx:
					# don't look at that part of the graph (same as above)
					continue

				if vtx not in B[target_vtx]:
					B[target_vtx].append(vtx)
					# NOTE B is defaultdict -> this is the time-consuming step

		stack.pop() # important, because the recursion works on this stack

		return circuit_found

	# -------------------------------------------------------------------------
	# Initialise variables
	num_vtcs 	= graph.num_vertices()
	accs 		= [] # list of the found autocatalytic cycles

	_blocked 	= {graph.vertex_index[vtx]:False for vtx in graph.vertices()}
	# ...to use for reintialisation of block list

	log.debug("Looking for autocatalytic cycles in graph with %d vertices", num_vtcs)

	for start_vtx in graph.vertices():
		# (Re-)initialise variables
		B = defaultdict(list) # this step is fast
		blocked = copy.copy(_blocked) # faster then re-creating
		stack 	= []

		# Search for circuits starting at this vertex (by index, not object)
		find_cycle(graph, graph.vertex_index[start_vtx])

	if convert_back:
		# Convert the indices back to Vertex objects of the argument graph
		raise NotImplementedError()

	return accs

def filter_cycles(graph, cycles: list, *_, mode: str): # TODO implement modes
	''' Filters the list of cycles in the graph depending on different modes and returns the filtered list of cycles.

	Args:
		graph (Network) : The graph it all takes place on
		cycles (seq) : The sequence of cycles found in the graph
		mode (str) : The mode to use for filtering.

	Return:
		list of cycles that fit the filtering mode.
	'''
	raise NotImplementedError()

	MODES = []

	if mode not in MODES:
		raise ValueError("Mode '{}' invalid, choose from {}.".format(mode, MODES))

	log.debug("Filtering %d cycles using mode '%s' ...", len(cycles), mode)

	f_cycs = [] # the list that is to be populated

	# TODO implement filtering here -- see acn.cycle_detection for how it is implemented there

	log.debug("Found %d cycles in mode '%s'.", len(f_cycs), mode)

	return f_cycs

def cycle_stats(graph, cycles, *args, modes: list, enabled: bool=True, **kwargs):
	''' Compute statistics on the given cycles'''
	stats = {}

	if not enabled:
		return stats

	for mode in modes:
		if mode not in STAT_MODES:
			log.warning("Mode %s not available, choose from %s. Skipping.",
			            mode, STAT_MODES)
			continue

		mode_kwargs = kwargs.get('mode_kwargs', {}).get("_"+mode, {})

		# Call evaluation method with these kwargs and save results
		stats[mode] = globals()["_"+mode+"_stats"](graph, cycles, **mode_kwargs)

	return stats

STAT_MODES 	= ['length']
def _length_stats(graph, cycles):
	''' Returns a list of the cycle lenghts.'''
	return [len(cyc) for cyc in cycles]


# -----------------------------------------------------------------------------
# ... -------------------------------------------------------------------------
# -----------------------------------------------------------------------------
