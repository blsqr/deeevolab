"""Defines the Agent class

TODO: Implement this.
"""

import abc

import deeevolab as dvl

# Initialise loggers and config file
log = dvl.setup_logger(__name__, level=dvl.get_log_level())
cfg = dvl.get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Local constants
DEBUG = dvl.get_debug_flag()

# -----------------------------------------------------------------------------

class Agent(metaclass=abc.ABCMeta):
	"""Class for an agent -- an abstract evolutionary entity.

	This class needs to be subclassed and cannot be instantiated itself."""

	def __init__(self):
		""""""
		pass
