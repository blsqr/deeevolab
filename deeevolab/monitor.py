''' The Monitor classes allow to keep track of the state of variables.'''

import copy
import re
from collections import OrderedDict, Hashable

import numpy as np

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import Locator, MaxNLocator

import deeevolab as dvl
from deeevolab.data_io import DataSaver, BinarySaver, SerialSaver

# Initialise loggers and config file
log = dvl.setup_logger(__name__, level=dvl.get_log_level())
cfg = dvl.get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Local constants
DEBUG = dvl.get_debug_flag()

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class StateMonitor:
	''' The StateMonitor class is the base class for recording the values of variables over time.'''

	def __init__(self, *args, source, variables: list, name: str, out_dir: str, source_keys: list=None, data_saver: DataSaver=None, record_mode: str='get', variable_defaults: dict=None, dtype: str=None, resizable: bool=True, plot_type: str='lineplot', plot_kwargs: dict=None, plot_every_n_steps: int=None, save_every_n_steps: int=None, record_init_kwargs: dict=None):
		''' Initialise a StateMonitor to record the values of the given variables in the source object.

		Args:
			source (object) : the class object to keep track of

			variables (str, sequence of str, or sequence of 2-tuple) : the variable names to record (optionally as 2-tuples with the dtype as second element)

			data_saver (None or DataSaver object) : handles saving of data

			name (str, default: None) : a name for this monitor, otherwise one is generated

		# TODO finish writing docstring
		'''

		log.info("Initialising %s '%s' ...", self.__class__.__name__, name)

		if len(args) > 0:
			log.warning("'%s' was called with positional arguments; takes keyword arguments only!", name)

		# Initialise empty variables dictionary; keeping track of dtypes, map_funcs, column associations, valid ranges, ...
		self.variables	= OrderedDict()

		# Initialise the source object and whether it needs to be accessed via a keylist or not
		# Check whether the source is assumed a fixed reference or not; if not, the property source will have to access the object each time it is accessed by this monitor.
		self._source 	= source
		if isinstance(source, (list, tuple, str)):
			self._src_keys 	= source_keys
		else:
			self._src_keys 	= None

		# Regarding saving
		self.data_saver = data_saver
		self._read_only = False
		self.out_dir 	= out_dir

		# Record initialisation params
		_rik_def = cfg.get(self.__class__.__name__, {}).get('record_init_kwargs', {})
		self._record_init_kwargs = copy.deepcopy(_rik_def)
		if record_init_kwargs:
			self._record_init_kwargs.update(record_init_kwargs)

		# Regarding recording and steps
		self.dtype 		= dtype
		self.resizable	= resizable
		self.rec_mode 	= record_mode

		self.steps 		= 0
		self.save_every = save_every_n_steps
		self.plot_every = plot_every_n_steps

		# Regarding plotting
		self.plot_type 	= plot_type
		self.plot_kwargs= {} if plot_kwargs is None else plot_kwargs

		# Regarding defaults
		self.defaults 	= variable_defaults if variable_defaults else {}

		if not self.defaults.get('plot_type'):
			# Ensure that each variable has a plot type assigned
			self.defaults['plot_types'] = [self.plot_type]

		if not self.defaults.get('dtype'):
			self.defaults['dtype'] 		= self.dtype

		# Give a name to the monitor, one for labels (name) and one that is path-safe (sname)
		self.name 	= name
		self.sname 	= _make_path_safe(name)

		# Initialise the variables (which might act upon the records)
		self._init_variables(variables)

		# Initialise records attribute
		self._init_records(**self._record_init_kwargs)

		log.note("'%s' initialised.", self.name)

		return

	# Magic methods ...........................................................

	def __getitem__(self, key):
		try:
			# All data in self.records is assumed valid, thus no further checks necessary
			return self.records[key]
		except KeyError:
			log.error("Key Error: could not find key '%s' in records of data monitor '%s'.", key, self.name)
			raise

	def __setitem__(self, key, val):
		''' Setting an item directly is prohibited.'''
		raise RuntimeError("Setting a record item manually is prohibited; use the methods 'record_single_step' or 'manual_record_step', depending on record_mode.")

	def __len__(self):
		''' The length of a monitor is just how many steps it has recorded'''
		return self.steps

	# Properties ..............................................................

	@property
	def source(self):
		''' Returns the source object.'''
		if not self._src_keys:
			# Source is assumed to have a fixed reference
			return self._source
		# Have a keylist to walk through to get to the actual source object
		return self._rec_get_src_()

	# Remap some iterator methods to the records attribute ....................

	def keys(self):
		''' Returns an iterator over the variable names.'''
		return self.variables.keys()

	def values(self):
		''' Returns the iterator over the record values.'''
		return self.records.values()

	def items(self):
		''' Returns the record.items() iterator.'''
		return self.records.items()

	# Most important public methods ...........................................
	# Managing variables . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def add_variable(self, var_name: str, map_func=None, dtype=None, plot_types: list=None, add_column: bool=True):
		'''Adds a variable to the Monitor object, including its mapping function and (optionally) a dtype.

		Args:
			var_name (str)  	: variable name
			map_func (optional) : a mapping function for this variable
			dtype (optional)    : the variables dtype
			plot_types (opt.) 	: with which type of plot to plot this variable
			add_column (bool)   : whether to add a new column or not; False is used when adding multiple variables at once.
		'''

		# Check if this variable is already present
		if var_name in self.variables.keys():
			log.warning("Variable '%s' already present in %s; was not added.", var_name, self.name)
			return

		if plot_types is None:
			log.debug("Using default plot types for new variable %s of %s because no plot types were specified", var_name, self.name)
			plot_types = copy.copy(self.default_plot_types)

		# Check which value-getter to use for this variable (if not manual)
		if self.rec_mode in ['get']:
			log.debug("Determining value getter for variable %s ...", var_name)

			_source	= self.source
			log.debugv("Identified source of type %s", type(_source))

			if hasattr(_source, var_name):
				if callable(getattr(_source, var_name)):
					log.debugv("Value getter set to 'by attr call'.")
					value_getter = self._get_val_by_attr_call
				else:
					log.debugv("Value getter set to 'by attr'.")
					value_getter = self._get_val_by_attr

			elif hasattr(_source, '__contains__'):
				log.debugv("Value getter set to 'by item'.")
				value_getter = self._get_val_by_item

			else:
				log.warning("No value getter could be found for variable '%s' "
				            "of monitor '%s'! Using generic value getter.",
				            var_name, self.name)
				value_getter = self._get_val_generic

		else:
			# no value_getter needed, because values are recorded manually
			value_getter = None

		# Add info to variables dict
		self.variables[var_name] = {
			'map_func' 	 : map_func,
			'dtype'		 : dtype,
			'get_val'	 : value_getter,
			'plot_types' : plot_types if plot_types is not None else [],
			'valid_range': [self.steps, self.steps] # python style
		}

		if add_column:
			self._new_column(var_name)
		else:
			log.debug("Not adding column for this variable.")

		return

	def add_variables(self, var_names: list):
		'''Add multiple variables at once, thus reducing resize operations.'''

		for var_name in var_names:
			self.add_variable(var_name, add_column=False)

		self._update_records(new_vars=var_names)

	def update_variables(self):
		'''Uses the private method _get_variable_names to determine whether any new variables were added. If so, it adds them to the variables attribute and finally adjusts the dataset such that the old data is copied over and the new data '''

		if not self.resizable:
			log.error("Updating variables not allowed for %s '%s'. Make sure to set the resizable attribute to True during monitor initialisation.", self.__class__.__name__, self.name)
			return

		new_vars = [var_name for var_name in self._get_variable_names()
					if var_name not in self.variables]

		self.add_variables(new_vars)

		return new_vars

	# Recording . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def record_single_step(self) -> bool:
		''' Records a single step of all variables kept track of in this StateMonitor.'''

		if self.rec_mode != 'get':
			log.debugv("%s: Trying to record a step via 'get', but record_mode is set to '%s'. Data has not been recorded.", self.name, self.rec_mode)
			return None

		log.debug("Recording step %d (%s) of %s ...",
				  self.steps, self.rec_mode, self.name)

		# Record step using private method and, if successful, increment the step counter.
		if self._record_single_step():
			self.next_step()
			return True
		else:
			log.debug("Not incrementing step of %s, because there was an error in adding a point.", self.name)
			return False

	def manual_record_step(self, record_data: dict, allow_missing: bool=False, step: int=None) -> bool:
		''' Manually record a step using the data given in the record_data dictionary. The keys should correspond to the variable names.

		Args:
			allow_missing (bool): allows missing data for this step
			step (int) : if given, writes the data to this step,'''

		if self.rec_mode != 'manual':
			log.error("%s: Trying to record a step via 'manual', but record_mode is set to '%s'. Data has not been recorded.", self.name, self.rec_mode)

			return False

		if step is not None:
			log.debug("Setting step manually to %d.", step)
			self.steps 	= step

		log.debug("Recording step %d (%s) of %s ...",
				  self.steps, self.rec_mode, self.name)

		# Record the data that was passed via dict
		if self._manual_record_step(rd=record_data,
		                            allow_missing=allow_missing, step=step):
			# everything successful -> increment step and return True
			self.next_step()
			return True
		else:
			log.error("Not incrementing step of %s, because there was an error in manually data: %s", self.name, record_data)
			return False

	def next_step(self, update_valid_range: bool=False):
		''' Increments the step count and -- if set to do so -- triggers the plotting of the monitor after each step increment.
		Is called after a successful manual or automatic recording.
		'''
		log.debugv("Called next_step on %s", self.name)
		if self.save_every and int(self.save_every) > 0:
			if self.steps % self.save_every == 0:
				self.save_data()

		if self.plot_every and int(self.plot_every) > 0:
			if self.steps % self.plot_every == 0:
				self.plot('all')

		if update_valid_range:
			for var_name in self.variables:
				self._update_valid_range(var_name) # NOTE this is also called in _record_single_step

		self.steps += 1

	# Others . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def save_data(self, cleanup: bool=False, close: bool=False) -> bool:
		''' Saves the data of this monitor using the '''

		if cleanup:
			self._cleanup_records()

		if self.data_saver is None:
			log.warning("No DataSaver available in %s.", self.name)
			return False

		log.debug("Saving data of monitor %s ...", self.name)
		self.data_saver.save(self)

		log.note("Data of %s saved using %s.",
				  self.name, self.data_saver.__class__.__name__)

		if close:
			self.data_saver.close()
			log.debug("%s of %s closed.", self.data_saver.__class__.__name__, self.name)

			# Mark read-only
			self._read_only 	= True
		return True

	def get_valid_range(self, var_name: str) -> tuple:
		''' Returns the range of indices (python-style) in which data of variable var_name is valid.'''
		return tuple(self.variables[var_name]['valid_range'])

	# Private methods .........................................................
	# Initialisation . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	def _init_records(self, **record_init_kwargs) -> bool:
		''' Initialises the empty records attribute, is called by __init__'''
		self.records 	= {} # ... very simple for this one
		return True

	def _init_variables(self, variables, add_column: bool=True):
		''' Adds the variables to the records attribute.'''

		# If the variables argument is the string 'all' and the source is found to be a dictionary, all dictionary keys are used as variables.
		if isinstance(variables, str) and variables == 'all':
			_source = self.source

			if isinstance(_source, dict):
				log.debug("Initialising monitor to keep track of all "
				          "dictionary entries that are present at "
				          "initialisation: %s", list(_source.keys()))
				# Call the method again with all dictionary keys as variables
				return self._init_variables(sorted(list(_source.keys())),
				                            add_column=add_column)

			raise TypeError("The source object needs to be derived from dict, "
			                "but was of type {}.".format(type(_source)))

		elif not isinstance(variables, (tuple, list)):
			raise TypeError("Argument 'variables' needs to be a sequence, was {}, with value: {}".format(type(variables), variables))

		# Now have an iterable of variables
		log.note("Initialising %d variables ...", len(variables))

		# Make the default variable parameters available
		default_map_func 	= self.defaults.get('map_func')
		default_plot_types 	= self.defaults.get('plot_types', [])
		default_dtype 		= self.defaults.get('dtype', self.dtype)

		# Look at them separately
		for variable in variables:
			log.progress("Initialising variable '%s' ...", variable)

			# Distinguish different modes of initialising the variables from the type of variable declaration that was given
			if _is_numpy_style_variable_description(variable):
				# Add variable in numpy-style (name, dtype)
				log.debug("Got numpy-style variable descriptor.")
				self.add_variable(variable[0],
								  map_func=default_map_func,
								  dtype=variable[1],
								  plot_types=default_plot_types,
								  add_column=add_column)

			elif _is_dict_style_variable_description(variable):
				# Add variable using the info from a dict
				log.debug("Got dict-style variable descriptor.")

				if isinstance(variable, dict):
					_v 			= variable  	# everything is the dict
					_name		= _v['name']
				elif isinstance(variable, (tuple, list)):
					_name, _v 	= variable 		# second element is the dict

				# Check for a mapping function for this specific variable
				_map_func = _parse_map_func(_v, default_map_func)

				# Add the variable
				self.add_variable(_name,
								  map_func	=_map_func,
								  dtype		=_v.get('dtype',
													default_dtype),
								  plot_types=_v.get('plot_types',
													default_plot_types),
								  add_column=add_column)

			elif isinstance(variable, Hashable):
				# Add just the variable with the default map_func and dtype
				self.add_variable(variable,
				                  map_func=default_map_func,
				                  dtype=default_dtype,
				                  plot_types=default_plot_types,
				                  add_column=add_column)

			else:
				raise TypeError("Argument 'variables' needs to be a sequence of str or a 2-tuple or a hashable type, but it contained an element '{}' of type {}".format(variable, type(variable)))

		return True

	# Managing the records  . . . . . . . . . . . . . . . . . . . . . . . . . .

	def _rec_get_src_(self, tmp_src=None, src_name: str=None):
		''' Get the target object by starting at the actual source and walking along the attributes specified in the keylist.'''

		if src_keys is None and tmp_src is None:
			# not inside the recursion yet, use the saved source name
			src_keys 	= self._src_keys
			tmp_src  	= self.source

		if isinstance(src_keys, list):
			if len(src_keys) > 1:
				# recursive step
				return self.get_src(tmp_src=getattr(tmp_src, src_keys[0]),
									src_keys=src_keys[1:])
			elif len(src_keys) == 1:
				# last step with src_keys in list notation
				return getattr(tmp_src, src_keys[0])

		elif isinstance(src_keys, str):
			# last step with src_keys in str notation
			return getattr(tmp_src, src_keys)

	def _new_column(self, var_name):

		if self.steps > 0:
			# populate with None values to get the same length
			self.records[var_name] 	= [None for _ in range(self.steps)]
		else:
			self.records[var_name] 	= []
		return

	def _new_columns(self, var_names: list):
		''' Adds multiple columns to the records.'''
		for var_name in var_names:
			self._new_column(var_name)

	def _add_point(self, var_name, val):
		''' Adds a datapoint to the record list'''
		self.records[var_name].append(val)

	def _update_valid_range(self, var_name):
		''' Updates the upper valid range entry of the given variable. The indexing is python-style, i.e. the upper index is the first invalid index.'''
		self.variables[var_name]['valid_range'][1] = self.steps + 1

	def _update_records(self, new_vars: list):
		''' Is called to bring the records up to date after variables were added. This adds a new entry to the records dictionary.'''
		if not new_vars:
			return

		self._new_columns(new_vars)

	def _cleanup_records(self):
		''' Called when a recording is finished and can be subclassed to handle cleanup operations on the record data, e.g. remove invalid data points.'''
		if self._read_only:
			log.warning("Monitor %s is already in read_only mode, cannot perform cleanup.", self.name)

		else:
			log.debug("Performing cleanup on %d variables ...",
					  len(self.variables))

		# NOTE nothing to do here for StateMonitor

	def _record_single_step(self):
		# Record by reading the variable names or calling the methods
		success 	= True
		for var_name, var in self.variables.items():
			if var_name == "t":
				self._add_time_point()
				continue

			# Get value by calling the value_getter of this variable
			val = var['get_val'](var_name)

			success *= self._add_point(var_name, val)

			self._update_valid_range(var_name)

		return success

	def _manual_record_step(self, rd: dict, allow_missing: bool=False, step: int=None):
		if step:
			raise NotImplementedError("Manual recording for a specific step is not implemented for %s", self.__class__.__name__)

		for var_name in self.variables.keys():
			if not allow_missing and var_name not in rd:
				log.error("Variable '%s' is not in record_data for manual record step of %s.", var_name, self.name)
				return False

			# Get the value out of the dict and apply the mapping function to it
			val = self._map_val(var_name, rd.get(var_name))

			if not self._add_point(var_name, val):
				return False

			self._update_valid_range(var_name)

		return True

	def _map_val(self, var_name, val):
		''' '''
		map_func 	= self.variables[var_name]['map_func']
		if map_func is not None:
			val 	= map_func(val)
		return val

	def _get_variable_names(self):
		raise NotImplementedError("This method still has to be implemented in a general way.")

	# Value getters  . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	# Which one is used is set in the get_val attribute of each variable

	def _get_val_generic(self, var_name):
		raise NotImplementedError()

	def _get_val_by_attr(self, var_name):
		''' Get by attribute'''
		val = getattr(self.source, var_name)

		return self._map_val(var_name, val)

	def _get_val_by_attr_call(self, var_name):
		''' By calling an attribute'''
		val = getattr(self.source, var_name).__call__()

		return self._map_val(var_name, val)

	def _get_val_by_item(self, var_name):
		''' By item'''
		val = self.source.get(var_name)

		return self._map_val(var_name, val)


	# Plotting ................................................................

	def plot(self, variables: list, fname_suffix: str=None, title: str=None, plot_type: str=None, **add_plot_kwargs):
		''' Plots the variables of the monitor according to the specified plot_type.'''

		# Get the plot parameters from the configuration
		# NOTE These should only be read, not changed!
		params 		= cfg.plt

		# Only plot if a plot_type was declared
		plot_type 	= plot_type if plot_type else self.plot_type

		if not plot_type:
			# Do not plot this monitor
			log.caution("Not plotting %s, as no plot_type was passed to method plot or declared during initialisation.", self.name)
			return

		if plot_type not in params.types:
			log.error("Invalid plot type '%s'. Choose one from: %s", plot_type, params['types'])
			return

		# Gather variables needed for plotting
		if variables == 'all':
			# All variables with the same plot type
			variables = [var_name for var_name, var in self.variables.items() if plot_type in var['plot_types']]

		if len(variables) == 0:
			# nothing to plotting
			log.caution("No variables to plot with plot type '%s' for %s. Skipping.", plot_type, self.name)
			return

		# All checks passed. Start plotting now ...
		log.note("Plotting %s (%s, %d vars) ...",
				  self.name, plot_type, len(self.variables))

		if not title:
			title 	= self.name.replace("_", " ")

		if isinstance(fname_suffix, str):
			suffix = "_" + params.fname_suffix_strf.format(fn_suffix=fname_suffix)
		else:
			suffix = params.fname_suffix_strf.format(fn_suffix="")

		# Create output filename
		output 	= self.out_dir + self.name.replace(" ", "_").lower() + suffix

		# build plot kwargs:
		# start with defaults, updated by type-specific, updated by user-set
		plot_kwargs = copy.deepcopy(dict(params.plot_kwargs))
		plot_kwargs.update(params.types[plot_type]['kwargs'])
		plot_kwargs.update(self.plot_kwargs)

		# Got all necessary parameters now.
		# Call private plotting function
		try:
			fig	= getattr(self, '_' + plot_type).__call__(variables,
														  title=title,
														  **plot_kwargs)
		except Exception as err:
			log.error("Error while plotting %s with plot_type %s, variables %s.\nError Message: %s", self.name, plot_type, variables, err)
			if DEBUG:
				raise
			return

		fig.savefig(output, **params.save_kwargs)
		plt.close()

		log.note("Plotting %s finished.", self.name)

		return

	# Private methods regarding plotting  . . . . . . . . . . . . . . . . . . .

	def _get_data_points(self, var_name: str, max_num_points: int=None, last_point_inside: bool=True):
		''' This method is used by the plotting methods to get the relevant data points from the records of this variable.

		Args:
			var_name (str): the var_name to get the data for

			max_num_points (int, default: None) : the maximum number of points (in time) to get. If this is smaller than the number of recorded steps, it will choose reasonably spaced points (without duplicates) and return those.

		Returns:
			tuple:
				list of selected steps
				list of data points. The structure and data type of each data point remains unchanged.
		'''

		# TODO take into account valid range

		if max_num_points is None or max_num_points > self.steps:
			# Just return the steps and the corresponding data
			steps 	= range(self.steps)
			data 	= self[var_name]

		else:
			if last_point_inside:
				# the last point counts to the total number of points
				max_num_points -= 1

			# Need to select which steps to choose
			steps 	= list(range(0, self.steps,
								int(self.steps/(max_num_points - 1))))
			if last_point_inside:
				# Add the last point manually
				steps.append(self.steps-1)

			# Get the corresponding data
			data 	= [self[var_name][s] for s in steps]

		return steps, data

	def _lineplot(self, variables: list, *args, title: str, params: dict=cfg.plt, **plot_kwargs):
		'''Do a lineplot on the passed axis'''

		# Create figure
		fig, ax 	= plt.subplots(**params.fig_kwargs) # TODO move this up

		max_steps 	= 0

		# Plot the variables and keep track of the maximum step
		for var_name in variables:
			steps 		= list(range(*self.get_valid_range(var_name)))
			if not steps:
				log.debug("No valid data for variable %s in %s. Continuing.", var_name, self.name)
				continue

			max_steps 	= max(max_steps, max(steps))
			valid_data 	= self[var_name]

			# Plot the variable over the steps
			plt.plot(steps, valid_data, label=var_name,
			         **params.types.lineplot.linekwargs)

		# Force integer tick labels on x axis and have them only in nice locations, i.e. numbers starting with 1, 2, 5 ...
		ax.xaxis.set_major_locator(MaxNLocator(integer=True, steps=[1,2,5]))

		# Extra settings
		# y-axis ticks, scale, label and limits
		if plot_kwargs.get('integer_yticks'):
			ax.yaxis.set_major_locator(MaxNLocator(integer=True, steps=[1,2,5]))

		if plot_kwargs.get('symlogy'):
			linthreshy 	= plot_kwargs.get('linthreshy')
			if linthreshy is None:
				log.warning("Plot kwarg 'symlogy' passed to %s, but no 'linthresy'. Please also pass 'linthreshy'.", self.name)
			else:
				# Symlog y-scale and custom axis tick locator
				plt.yscale('symlog', linthreshy=linthreshy)
				ax.yaxis.set_minor_locator(MinorSymLogLocator(linthreshy))

		if plot_kwargs.get('ylabel'):
			plt.ylabel(plot_kwargs.get('ylabel'))

		if plot_kwargs.get('ylims'):
			ylims 	= plot_kwargs.get('ylims')
			if isinstance(ylims, dict):
				plt.ylim(**ylims)
			elif isinstance(ylims, (list, tuple)):
				plt.ylim(*ylims)
			else:
				raise ValueError("Invalid ylims format for monitor "+str(self.name))

		# Horizontal and vertical lines
		if plot_kwargs.get('hlines', False) is not False:
			hlines 	= plot_kwargs.get('hlines')
			if isinstance(hlines, (int, float)):
				hlines 	= [hlines]

			for hline in hlines:
				plt.axhline(y=hline, **params.types.lineplot.hline)

		if plot_kwargs.get('vlines', False) is not False:
			vlines 	= plot_kwargs.get('vlines')
			if isinstance(vlines, (int, float)):
				vlines 	= [vlines]

			for vline in vlines:
				plt.axvline(x=vline, **params.types.lineplot.vline)

		# x-limits
		xlims 	= copy.deepcopy(params.types.lineplot.xlims)
		xlims.update(plot_kwargs.get('xlims', {}))
		if xlims:
			if xlims.get('right') == 'max':
				xlims['right'] = max_steps
			plt.xlim(**xlims)

		# Legend
		if plot_kwargs.get('inline_labels'):
			inline_labels(axis=ax)
		else:
			if plot_kwargs.get('external_legend'):
				leg_kwargs 	= params.types.lineplot.external_legend
			else:
				leg_kwargs 	= params.types.lineplot.default_legend

			if plot_kwargs.get('legend_kwargs'):
				leg_kwargs.update(plot_kwargs.get('legend_kwargs', {}))

			plt.legend(**leg_kwargs)

		# Finalise figure
		plt.xlabel(params.types.lineplot.xlabel)
		plt.title(title)

		return fig



# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class BinaryMonitor(StateMonitor):
	''' Monitor that works on a numerical data structure compatible to the hdf5 binary format. This monitor only allows one data type for all data and has a fixed recording length.
	BinaryMonitor objects can share one BinarySaver, meaning that they all store their records in a shared binary file.
	'''

	def __init__(self, *args, data_saver: BinarySaver, dtype: str, max_length: int, base_group: str=None, col_name_dtype: str='int32', resizable: bool=False, **kwargs):
		''' Initialise a BinaryMonitor object with a certain size.

		Args:
			dtype (str, np.dtype) : the datatype to be used for all data (!) in this binary monitor

			data_saver (BinarySaver) : specifies which binary file object will be used -- this DataSaver can be shared among several BinaryMonitors

			max_length (int, required) : expected size of the data to be written

			# TODO finish writing docstring

			Other args and kwargs are passed to the parent __init__ method.
		'''

		# Requires BinarySaver and then works on the output file created there
		if not isinstance(data_saver, BinarySaver):
			raise TypeError("Need a data_saver of type {}, was {}.".format(type(BinarySaver), type(data_saver)))

		# Save attributes
		self.max_length 	= max_length
		self.base_group 	= "/" if base_group is None else base_group
		self.resizable 		= resizable
		self._col_name_dtype= np.dtype(col_name_dtype)

		# Initialise via parent class init method, which calls _init_records and _new_column
		super().__init__(*args, dtype=dtype, data_saver=data_saver, resizable=resizable, **kwargs)

		return

	def __getitem__(self, key):
		''' Get the data of a column from the monitor. Note: only the valid data is returned.

		To read a item of the BinaryMonitor, it might be necessary to re-reference it, after it has been closed. This method performs the re-referencing, if necessary.'''

		if isinstance(key, slice):
			log.debug("Requested item by slice.")
			return self.records[key]

		log.debug("Requested item by variable name.")
		col 		= self.variables[key]['column']
		start, stop = self.get_valid_range(key)

		try:
			return self.records[start:stop,col]

		except KeyError:
			log.error("Key Error: could not find key '%s' in records of data monitor '%s'.", key, self.name)
			raise

		except ValueError:
			# h5py throws a ValueError, if it has an invalid object id, which is due to a bad reference of self.records
			# NOTE this might also happen for a different reason, which is problematic, because then there would be many re-referencing steps

			self._reref_records()

			# Re-try
			return self.__getitem__(key)

	def values(self):
		''' Returns a list of the columns of the records'''
		# TODO implement as iterator
		return [self.records[:][v['column']]
				for v in self.variables.items()]

	def items(self):
		''' Returns a list of (var_name, column data) tuples for all variables.
		'''
		# TODO implement as iterator
		return [(k,self.records[:][v['column']])
				for k, v in self.variables.items()]

	def add_variable(self, var_name: str, map_func=None, dtype=None, add_column: bool=True):
		''' Add a variable to the monitor.'''

		log.debug("Adding variable %s to %s ...", var_name, self.name)

		# Give a warning if a it was tried to give a different dtype
		if not issubclass(np.dtype(dtype).type, self.dtype.type):
			log.warning("Trying to add a variable with dtype '%s' to monitor '%s', although the monitor's dtype was fixed to '%s'. Using the monitor's dtype instead.", dtype, self.name, self.dtype)

		# Use the parent method -- the _new_column is called from there, if add_column is True.
		super().add_variable(var_name, map_func=map_func,
		                     dtype=self.dtype, add_column=add_column)

		self.variables[var_name]['column'] 	= len(self.variables) - 1

		if add_column:
			# Save some data to the record attributes
			log.debug("Saving new variable information to record attributes ...")
			# The column names
			_var_names 	= list(self.records.attrs['col_names'])
			_var_names.append(var_name)
			_var_names 	= np.array(_var_names, dtype=self._col_name_dtype)
			self.records.attrs['col_names'] 	= _var_names

			# The last resize
			self.records.attrs['last_resize']	= self.steps

			# The first valid (row) index of that column, in order to reconstruct the valid data range later on ...
			_col_start	= list(self.records.attrs['col_start'])
			_col_start.append(self.variables[var_name]['valid_range'][0])
			_col_start 	= np.array(_col_start, dtype='uint32')
			self.records.attrs['col_start'] 	= _col_start

			log.debug("Finished saving to record attributes.")

	def record_single_step(self, updated: bool=False) -> bool:
		''' Record a single step and, if that happens to fail, update the variables and resize the records and try again.'''

		# Record using parent method... NOTE: have to check for False, because None might mean that it was set to manual record mode...
		if super().record_single_step() is False: # This is where rec happens
			# Could not write the data, probably due to a wrong size ...
			if self.resizable and not updated:
				log.debug("Recording a single step of %s failed; updating variables and trying again...")
				# Update the variables and records, then try to write again ...
				self.update_variables()
				self.record_single_step(updated=True)
			else:
				log.error("Could not record single step for %s '%s'.\n"\
				          "Updated: %s, Resizable: %s",
				          self.__class__.__name__, self.name,
				          updated, self.resizable)
				return False

		# Successfully recorded step.
		return True

	# Private methods .........................................................

	def _init_records(self, max_cols: int=None, **record_init_kwargs) -> bool:
		''' Initialises the records attribute, is called by __init__. Note that the variables might or might not yet be initialised.

		Args:
			max_cols (int, None) : maximum possible columns. If None, the maximum number is only limited by memory.
		'''

		# Use the monitors unique name for the group name
		_g 		 	 = self.data_saver.binfile[self.base_group]
		self.records = _g.create_dataset(self.sname,
										 (self.max_length,len(self.variables)),
										 dtype=self.dtype,
										 chunks=True,
										 maxshape=(self.max_length,
												   max_cols),
										 **record_init_kwargs)

		# Initialise data for record attributes: column name and valid range starts
		col_names 	= np.array(list(self.variables.keys()),
		                       dtype=self._col_name_dtype)
		col_start 	= np.array([v['valid_range'][0]
		                        for v in self.variables.values()],
		                       dtype='uint32')

		# Initialise the record attributes
		self.records.attrs['col_names']		= col_names
		self.records.attrs['col_start']		= col_start
		self.records.attrs['last_resize']	= self.steps # When the last write step was

		log.debug("Records initialised.")

		return True

	def _new_column(self, var_name):
		''' Add a column to the dataset.'''
		# Create a new column by resizing the dataset along the last axis
		self.records.resize(self.records.shape[-1] + 1, axis=1)

	def _new_columns(self, var_names: list):
		''' Creates multiple new columns -- use this for reducing the number of resize operations.'''
		# Create new columns by resizing the dataset along the last axis
		self.records.resize(self.records.shape[-1] + len(var_names), axis=1)

	def _update_records(self, new_vars: list):
		''' Is called to bring the records up to date after variables were added. This can lead to resize operations.'''

		if not new_vars:
			return

		log.debug("Adding new columns to %s: %s", self.name, new_vars)
		self._new_columns(new_vars)


		col_names 	= np.array(list(self.variables.keys()),
		                       dtype=self._col_name_dtype)
		col_start 	= np.array([v['valid_range'][0]
		                        for v in self.variables.values()],
		                       dtype='uint32')
		col_stop 	= np.array([v['valid_range'][1]
		                        for v in self.variables.values()],
		                       dtype='uint32')

		self.records.attrs['col_names'] 	= col_names
		self.records.attrs['col_start'] 	= col_start
		self.records.attrs['col_stop'] 		= col_stop

		self.records.attrs['last_resize'] 	= self.steps

		log.debug("Successfully updated records of %s.", self.name)

	def _cleanup_records(self):
		''' Called upon finishing of the recordings'''
		super()._cleanup_records()

		# Resize the dataset to the actual number of recorded steps; reasonable when finishing a simulation earlier than expected
		self.records.resize(self.steps, axis=0)

		# Save the last valid (row) index of each column
		_col_stop = np.array([v['valid_range'][1]
		                      for v in self.variables.values()],
		                     dtype='uint32')
		self.records.attrs['col_stop'] 	= _col_stop

		log.debug("Resized dataset (max: %d,  new: %d) and wrote last valid index to attributes.", self.max_length, self.steps)

	def _reref_records(self):
		''' Re-references the records attribute to an (already created) group in the data_saver object.'''
		self.records 	= self.data_saver.binfile[self.base_group][self.sname]
		log.debug("Re-referenced records for monitor %s", self.name)

	def _add_row(self, data):
		''' Directly add a whole row of data. Not usually used, because record_single_step only calls _add_point, but can be used by monitor extensions.'''

		try:
			# If iterable data, convert to float to be able to add whole row
			self.records[self.steps] = tuple(data)

		except TypeError as err:
			# probably: not iterable data
			if self.resizable:
				return False
			log.error("Could not add row of data! TypeError: %s\nThis might be resolved, if the monitor is made resizable.\nData:\n%s", err, data)
			raise

		except ValueError as err:
			if self.resizable:
				log.debug("Failed to add row to %s! Will trigger creation of new columns ...", self.name)
			else:
				log.warning("Failed to add row to %s!\n\tGiven data: '%s'\n\tValueError: %s", self.name, data, err)
			return False

		except AttributeError as err:
			log.error("Could not add row of data! AttributeError: %s\n%s",
			          err, data)
			raise

		# Update valid ranges for all variables
		for var_name in self.variables:
			self._update_valid_range(var_name)

		return True

	def _add_point(self, var_name: str, val):
		''' Adds a data point to the dataset at the current step.'''

		# Get the column for the variable
		col 	= self.variables[var_name]['column']

		# Write data point and update valid range
		try:
			self.records[self.steps, col] = val

		except OSError as err:
			# probably thrown with error message:
			# Can't prepare for writing data (no appropriate function for conversion path), hinting to an invalid data type

			log.error("OSError: %s"\
			      "Monitor name: %s\nVariable: %s\nValue: %s (type %s)\n"\
			      "Target dtype: %s\nValue Getter: %s\nSource: (type %s)",
			      		err, self.name, var_name, val, type(val),
			      		self.variables[var_name]['dtype'],
			      		self.variables[var_name]['get_val'].__name__,
			      		type(self.source))

			if DEBUG:
				raise
			return False

		self.variables[var_name]['valid_range'][1] = self.steps
		return True




# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class CompoundBinaryMonitor(BinaryMonitor):
	''' Like binary monitor, but saves data in a structured numpy array, thus allowing directly naming columns. This class should be used, if no new variables need to be added to the monitor.'''

	def __init__(self, *args, resizable: bool=False, **kwargs):

		self._vars_locked 	= False

		super().__init__(*args, resizable=resizable, **kwargs)

		if self.resizable:
			log.caution("Enabled resize operations in %s '%s'. Note that these operations will reduce performance; consider using a pure BinaryMonitor instead.", self.__class__.__name__, self.name)

		self._vars_locked 	= True

	def __getitem__(self, key):
		''' Get the data of a variable key (i.e. one column) from the monitor. Note: only the valid data is returned.

		To read a item of the BinaryMonitor, it might be necessary to re-reference it, after it has been closed. This method performs the re-referencing, if necessary.'''

		if isinstance(key, slice):
			log.debug("Requested item by slice.")
			return self.records[key]

		log.debug("Requested item by column name.")
		start, stop = self.get_valid_range(key)

		try:
			return self.records[key, start:stop]

		except KeyError:
			log.error("Key Error: could not find key '%s' in records of data monitor '%s'.", key, self.name)
			raise

		except ValueError:
			# h5py throws a ValueError, if it has an invalid object id, which is due to a bad reference of self.records
			# NOTE this might also happen for a different reason, which is problematic, because then there would be many re-referencing steps

			self._reref_records()
			log.debug("Re-referenced records for monitor %s", self.name)

			# Re-try
			return self.__getitem__(key)

	def add_variable(self, *args, **kwargs):

		if not self._vars_locked or self.resizable:
			# Call the parent's parent method, because in the BinaryMonitor there is a type check for the dtype
			super(BinaryMonitor, self).add_variable(*args, **kwargs)

		else:
			log.warning("Cannot add variables to %s '%s' after initialisation ended or if resizable is disabled.", self.__class__.__name__, self.name)

	# Private methods .........................................................

	def _init_records(self, max_cols=None, **record_init_kwargs):
		''' This method builds the records as a h5py dataset with a structured numpy array.'''

		# Gather necessary variables
		_vars 	= self.variables.items()
		dtype 	= np.dtype([(_name, _var['dtype']) for _name, _var in _vars])
		shape 	= (self.max_length,)

		if not len(dtype):
			log.error("Empty dtype, possibly due to no monitor variables having been initialised for monitor %s.", self.__class__.__name__)
			return False

		log.debug("Creating dataset with unique name '%s',"\
		          " %d rows, %d cols: %s",
		          self.sname, self.max_length,
		          len(_vars), str([_name for _name, _ in _vars]))

		if shape < (1,):
			log.error("Invalid shape %s for dataset of monitor %s",
			          shape, self.name)
			return False

		# Initialise dataset with the monitors unique name as dataset name and starting from the given base_group
		g 	= self.data_saver.binfile[self.base_group]

		try:
			self.records = g.create_dataset(self.sname, shape, dtype=dtype,
			                                chunks=True, maxshape=shape,
			                                **record_init_kwargs)

		except Exception as err:
			log.error("Error creating dataset with parameters: %s\nshape: %s\ndtype: %s", err, shape, dtype)
			if DEBUG:
				raise
			return False

		# This might have been a resize operation; keep track of it.
		self.records.attrs['last_resize'] 	= self.steps

		# Column names are not saved here, as they are in the compound data. Still create an entry
		self.records.attrs['col_names'] 	= []

		# Save the first valid (row) index for each column in a structured
		_dt = np.dtype([(_name, 'uint32') for _name in self.variables.keys()])
		_col_start 	= np.zeros((1,), dtype=_dt)
		for name, var in self.variables.items():
			_col_start[name] = var['valid_range'][0]
		self.records.attrs['col_start'] 	= _col_start

		return True

	def _new_column(self, var_name: str):
		# NOTE this needs to be passed, because otherwise _init_variables fails
		pass

	def _new_columns(self, var_names: list):
		# NOTE: this is no feasible method here; everything should be done via the _update_records method
		pass

	def _update_records(self, new_vars: list):
		''' Updates the records if more variables. This has to be done by deleting the old dataset and creating a new, larger one.

		NOTE: as the underlying data structure for this class is a structured numpy array, resize operations are costly and definitely discouraged. The parent BinaryMonitor class has better capabilities to do that.'''

		if not new_vars:
			log.debug("No new variables; no need to update the records of %s", self.name)
			return

		log.debug("Updating records of monitor %s ...", self.name)

		old_recs 	= self.records

		# Detach the dataset from the binary file
		del(self.data_saver.binfile[self.base_group][self.sname])
		log.debug("Saved and detached old dataset.")

		# Create the new dataset
		self._init_records(**self._record_init_kwargs)

		# Put the old data into the new dataset and set the new data to nan
		# Explixit method: by field names
		# for fname in self.records.dtype.names:
		# 	if fname in old_recs.dtype.names:
		# 		self.records[fname] = old_recs[fname]
			# else: is new data column, leave it as it is

		# Faster method: via fancy indexing of h5py
		self.records[old_recs.dtype.names] = old_recs

		# Done
		log.debug("Finished updating records.")
		return True

	def _cleanup_records(self):
		''' Called upon finishing of the recordings'''
		super()._cleanup_records()

		# Resize to the number of steps
		self.records.resize(self.steps, axis=0)

		# Add the last valid index of each variable to the attributes
		_dt = np.dtype([(_name, 'uint32') for _name in self.variables.keys()])
		_col_stop 	= np.zeros((1,), dtype=_dt)
		for name, var in self.variables.items():
			_col_stop[name] = var['valid_range'][1]
		self.records.attrs['col_stop'] 	= _col_stop

		log.debug("Resized dataset (max: %d,  new: %d) and wrote last valid index to attributes.", self.max_length, self.steps)

	def _add_point(self, name: str, val):
		''' Adds a data point to the dataset at the current step.'''

		# Write data point and update valid range
		try:
			self.records[name, self.steps] = val
		except ValueError as err:
			log.warning("Failed to add point to %s!\n\tVariable: '%s'\n\tValueError: %s", self.name, name, err)
			return False

		self.variables[name]['valid_range'][1] = self.steps
		return True




# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# For saving metadata: not quite a monitor, but a bit more than a Saver

class MetadataDict:
	''' A dict-like class for saving metadata to files upon setitem calls...'''

	def __init__(self, *args, fpath: str=None, save_upon_write: bool=cfg.MetadataDict.save_upon_write, initial_dict: dict=None):
		''' Initialise MetadataDict object.

		Args:
			fname, data_dir, data_format (str) : if given, they are passed on to SerialSaver
		'''

		if len(args):
			log.warning("MetadataDict takes no positional arguments, got: %s", args)

		# Initialise dict
		if initial_dict:
			self._dict 	= dict(**initial_dict)
		else:
			self._dict 	= dict()

		# Initialise saver and other attributes
		if fpath:
			self._saver	= SerialSaver(fpath=fpath)
			self._suw 	= save_upon_write

		else:
			log.warning("No argument 'fpath' given to MetadataDict. Cannot create SerialSaver, thus cannot save its data.")
			self._saver = None
			self._suw 	= False

	def __setitem__(self, key, val):
		''' Set a value and -- if configured to do so -- save afterwards.'''
		log.debug("Setting MetadataDict:  %s=%s", key, val)
		self._dict.__setitem__(key, val)
		self.save()

	def __getitem__(self, key):
		self._dict.__getitem__(key)

	def __delitem__(self, key):
		log.debug("Deleting key  %s  from MetadataDict...", key)
		self._dict.__delitem__(key)
		self.save()

	@property
	def records(self):
		''' Method used to make it compatible with the DataSaver infrastructure.'''
		return self._dict

	def update(self, d: dict, no_save: bool=False):
		''' Updates the dict and has the option not to save.'''
		log.debug("Updating MetadataDict...")
		self._dict.update(d)
		if not no_save:
			self.save()
		else:
			log.debug("Updated with no_save == True.")

	def update_item(self, key, d, no_save: bool=False):
		''' Updates not on the root level, but on level `key`, if the key is present and leads to a dict. If a keylist is passed, goes to that key...'''

		log.debug("Updating item  %s  of MetadataDict...", key)

		if isinstance(key, list):
			# Is a list of keys; walk to that key...
			_d = self._dict[key.pop(0)]
			while key:
				_d = d[key.pop(0)]
			_d.update(d)

		else:
			if isinstance(self._dict[key], dict):
				self._dict[key].update(d)
			else:
				log.warning("Item  %s  not a dictionary, cannot update.", key)
				return

		if not no_save:
			self.save()
		else:
			log.debug("Updated with no_save == True.")

	def add_items(self, **kwargs):
		''' Add multiple items via keyword arguments.'''
		self.update(kwargs)

	def save(self):
		''' '''
		if self._saver and self._suw:
			self._saver.save(self)
			log.debug("Saved MetadataDict.")
		else:
			log.debug("MetadataDict.save() called, but configured not to save.")


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Helpers

def _make_path_safe(s: str):
	''' Returns a path-safe string.'''
	pss	= s.replace(" ", "_")
	pss	= "".join([c for c in pss if re.match(r'\w', c)]) # clean up illegal characters
	pss	= pss.lower()
	log.debugv("Created path-safe string '%s'.", pss)
	return pss

def _parse_map_func(vd: dict, default_map_func):
	''' Used during variable initialisation. Returns the mapping function from the info passed as dict for a certain variable.
	'''
	if 'map_func' in vd:
		if vd['map_func'] is None:
			# None was passed, signaling to remove the map_func
			return None
		else:
			# Something else was passed, try to eval...
			try:
				_map_func 	= eval(vd.get('map_func'))
			except Exception as err:
				log.warning("Error while eval-ing key map_func '%s' for variable '%s' of monitor. Fallback to default_map_func '%s'.\nError message: %s", vd.get('map_func'), vd['name'], default_map_func, err)
			else:
				return _map_func

	return default_map_func

def _is_numpy_style_variable_description(v):
	return (isinstance(v, (tuple, list))
			and len(v) == 2
			and not isinstance(v[1], dict))

def _is_dict_style_variable_description(v):
	return (isinstance(v, dict)
			or (isinstance(v, (tuple, list))
				and len(v) == 2
				and isinstance(v[1], dict))
			)


class MinorSymLogLocator(Locator):
	'''	Dynamically find minor tick positions based on the positions of
	major ticks for a symlog scaling.

	FROM: http://stackoverflow.com/questions/20470892/how-to-place-minor-ticks-on-symlog-scale
	'''
	def __init__(self, linthresh):
		'''	Ticks will be placed between the major ticks.
		The placement is linear for x between -linthresh and linthresh,
		otherwise its logarithmically
		'''
		self.linthresh = linthresh

	def __call__(self):
		'Return the locations of the ticks'
		majorlocs = self.axis.get_majorticklocs()

		# iterate through minor locs
		minorlocs = []

		# handle the lowest part
		for i in range(1, len(majorlocs)):
			majorstep = majorlocs[i] - majorlocs[i-1]
			if abs(majorlocs[i-1] + majorstep/2) < self.linthresh:
				ndivs = 10
			else:
				ndivs = 9
			minorstep = majorstep / ndivs
			locs = np.arange(majorlocs[i-1], majorlocs[i], minorstep)[1:]
			minorlocs.extend(locs)

		return self.raise_if_exceeds(np.array(minorlocs))

	def tick_values(self, vmin, vmax):
		raise NotImplementedError('Cannot get tick locations for a '
								  '%s type.' % type(self))

