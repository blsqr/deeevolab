"""The Universe handles everything regarding a single simulation.

The Universe class implemented here needs to be derived and specialised by 
other classes in order to become usable.
"""

import os
import copy
import random
import abc
from time import process_time
from pprint import PrettyPrinter

import numpy as np
from easydict import EasyDict as DotDict

from paramspace import ParamSpace

import deeevolab as dvl
import deeevolab.environment
from deeevolab.monitor import BinarySaver, MetadataDict
import deeevolab.tools
from deeevolab.tools import log_centred_msg

# Initialise loggers and config file
log = dvl.setup_logger(__name__, level=dvl.get_log_level(is_universe=True))
cfg = dvl.get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Local constants
DEBUG = dvl.get_debug_flag()
MSG_WIDTH = dvl.tools.AVAILABLE_COLS

# -----------------------------------------------------------------------------

class Universe(dvl.environment.Environment, metaclass=abc.ABCMeta):
	"""The Universe is where everything takes place. It needs to be subclassed by the current model implementation, and is adjusted to the needs there.

	It is special in the sense that it takes care of everything specific regarding setup and provides an interface for management of the simulation, plotting of data and saving of data -- this functionality is implemented here in the parent class.

	The only methods to be subclassed by a custom Universe implementation are __init__ and run_simulation_step. All other capabilities (e.g. for analysis) can be configured using the config files.
	"""

	@abc.abstractmethod
	def __init__(self, *env_args, base_dir: str, seed=None, monitor_init: dict=None, actions: dict=None, **env_kwargs):
		"""Initialise the Universe.

		This will...
		- save relevant attributes, that are used to call the simulation methods
		- create the directories used for the simulations
		- initialise an Execution Time Monitor
		- initialise further monitors defined in `monitor_init`
		- initialise a BinarySaver to use for a shared binary file
		"""

		log.info("Initialising Universe with name '%s' ...",
		         env_kwargs.get('name', "(no name)"))
		self._t_init = process_time()

		# Initialise Environment object
		super().__init__(*env_args, **env_kwargs)

		# Initialise a flag DotDict
		self.flags = DotDict()

		# Directory stuff . . . . . . . . . . . . . . . . . . . . . . . . . . .
		self.dirs = dict(cfg.subdirs)
		self.dirs['out'] = cfg.out_dir_strf.format(uni=self, base_dir=base_dir)
		# 'out' is the base directory for all the simulation results

		# create the simulation directory and the subdirectories and also make the subdirectory entries in self.dirs absolute
		for key, path in self.dirs.items():
			if key != 'out':
				# make the path absolute
				self.dirs[key] = os.path.join(self.dirs['out'], path)
			# Create the directories
			os.makedirs(self.dirs[key], exist_ok=True)
		log.debug("Output directory created:\n%s", self.dirs['out'])

		# Universe DataSaver and Metadata object . . . . . . . . . . . . . . .
		# Create a default data saver object which is used for all binary data
		if env_kwargs.get('data_saver') is not None:
			log.warning("Overwriting DataSaver object passed to Environment.")
		self.data_saver	= BinarySaver(cfg.io.binary.format(uni=self))

		# Create a MetadataDict and make it accessible via attribute. Changes to the attribute are directly written to a file
		self.metadata = MetadataDict(fpath=cfg.io.metadata.format(uni=self))

		# Monitor stuff . . . . . . . . . . . . . . . . . . . . . . . . . . . .
		# List of other objects with monitors
		self._monitor_containers = []

		# Create default monitors (method inherited from Environment)
		self.setup_monitors(**cfg.default_monitors) # execution time monitor

		# Create additional monitors passed by monitor_init
		if monitor_init:
			self.setup_monitors(**monitor_init)

		# Link the time monitor
		self.time_monitor = self.monitors['exec_time']

		# Seeding . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
		# Set the seed (manually or randomly) and save the value
		if seed is not None:
			self._seed = seed
			log.state("Setting Universe seed manually:  %d", self._seed)
			_seed_rnd = False
		else:
			self._seed = np.random.randint(4294967295) # largest possible
			log.state("Setting Universe seed randomly:  %d", self._seed)
			_seed_rnd = True
		np.random.seed(self._seed)
		random.seed(self._seed)

		# Initialise some other variables . . . . . . . . . . . . . . . . . . .
		# For stop conditions
		self.stop_conds	= dict(funcs={}, params={}, check_every={},
		                       min_steps={}, enabled=False)

		# Initialise the actions dictionary
		self.init_actions(actions)

		# Dict to store action results
		self.action_res	= {}
		self.metadata.add_items(action_res={})

		# Save some metadata . . . . . . . . . . . . . . . . . . . . . . . . .
		log.debug("Saving metadata ...")
		self.metadata['init_kwargs'] = dict(seed=self._seed,
		                                    seed_random=_seed_rnd,
		                                    out_dir=self.dirs['out'],
		                                    name=self.name)

	def _init_finished(self):
		"""Actions to perform after the subclass has finished its setup.

		Has to be called at the end of the subclass initialisation and is not called by this classes init method!!
		"""

		log.debug("Finishing initialisation ...")

		# Record initial state (counts to the total number of steps)
		if self._monitoring:
			log.note("Recording initial state ...")
			self.gather_monitors()
			self.update_monitors()

		# Analyse and plot initial state
		self.perform_actions(self.actions.get('initial'))

		# Record time
		self.time_monitor.manual_record_step({
				'actions (before)': 	0.,
				'simulation': 			0.,
				'actions (after)': 		0.,
				'monitoring': 			0.,
				'step': 				0.,
				'cumulative total': 	process_time() - self._t_init,
				'initialisation': 		process_time() - self._t_init
			})

		log.note("%s initialisation finished.", self.__class__.__name__)

	# Simulation ..............................................................

	def run_simulation(self, *, num_steps: int, step_kwargs: dict=None, stop_kwargs: dict=None):
		"""Method to run the simulation. Details are specified in the child universe; it does not need to be subclassed and takes care of most of the necessary infrastracture.

		NOTE: This method can be called several times and continues with the previous state. The different counters correspond to different time frames:
			n_step 		: the number of the step in the current execution
			num_steps	: the total number of steps in the current execution
			self.steps 	: the number of this step when also counting previous executions
		However, this is not yet fully tested and might lead to problems, e.g. because cleanup_and_save was already called ...
		"""

		log.highlight(log_centred_msg(cfg.log.run_sim.format(uni=self, num_steps=num_steps)))

		# Set default values
		step_kwargs = step_kwargs if step_kwargs else {}
		stop_kwargs = stop_kwargs if stop_kwargs else {}

		# Stop conditions . . . . . . . . . . . . . . . . . . . . . . . . . . .
		# Prepare stop condition stuff: find methods and parse params
		self.init_stop_conditions(**stop_kwargs)


		# Prepare result dictionary (will be the callback) . . . . . . . . . .
		result = {
			'status': 		'running', 	# placeholder for later status
			'status_msg':	"",			# placeholder for more info
			'name':			self.name, 	# universe name is simulation name
			'profile': 		{} 			# to fill with profiling info
		}

		# Save arguments to metadata
		self.metadata['run_kwargs'] = dict(num_steps=num_steps,
		                                   step_kwargs=step_kwargs,
		                                   stop_kwargs=stop_kwargs)

		# Set up step formatstring
		max_num_steps = self.steps + num_steps
		step_strf = cfg.log.step.format(digits=len(str(max_num_steps)))

		# Simulation loop (starting at 1, because initial state is step 0)
		for n_step in range(1, num_steps + 1):
			# beginning time of step
			_t0 = process_time()

			# increment self.steps here, because initial state is at 0
			self.increment_steps()
			# Needs to be subclassed, such that it applies to all Environment-derived classes ...

			# Give info about the current step (aligned right, slightly clumsy)
			_step_str = step_strf % (self.steps, max_num_steps)
			log.highlight("%s" + _step_str, " "*(MSG_WIDTH-len(_step_str)))

			# Actions before step . . . . . . . . . . . . . . . . . . . . . . .
			self.perform_actions(self.actions.get('before_step'))
			_t1 = process_time()

			# Run step (implemented in child universe) . . . . . . . . . . . .
			log.info("Running simulation step ...")
			success = self.run_simulation_step(**step_kwargs)
			_t2 = process_time()
			log.note("Finished simulation step.")

			# Actions after step  . . . . . . . . . . . . . . . . . . . . . . .
			self.perform_actions(self.actions.get('after_step'))
			_t3 = process_time()

			# Monitoring . . . . . . . . . . . . . . . . . . . . . . . . . . .
			# Call all monitors and let them record their steps
			if self._monitoring:
				self.gather_monitors()
				self.update_monitors()

			# Build profiling information to manually record exec time monitor
			log.debug("Creating profiling information for %s ...",
			          self.time_monitor.name)
			
			_t4 	= process_time()
			_times 	= {
				'actions (before)':		_t1 - _t0,
				'simulation': 			_t2 - _t1,
				'actions (after)':		_t3 - _t2,
				'monitoring': 			_t4 - _t3,
				'step': 				_t4 - _t0,
				'cumulative total': 	_t4 - self._t_init
			}
			self.time_monitor.manual_record_step(_times)
			
			log.note("Finished recording step of universe monitors.")

			# Check stop condition . . . . . . . . . . . . . . . . . . . . . .
			stop_flag, stop_method, stop_msg = self.check_stop_conditions()

			log.state("Finished step in %s\n",
			          dvl.tools.format_time(process_time() - _t0,
			                                ms_precision=2))

			# Was the simulation step successful? . . . . . . . . . . . . . . .
			if not success:
				log.highlight("Break signal sent from run_simulation_step.")
				result['status'] = 'failed'
				break
			elif stop_flag:
				result['status'] = stop_method
				result['status_msg'] = stop_msg
				break
			# else: continue with loop . . . . . . . . . . . . . . . . . . . .
		else:
			# Loop finished without breaking out -> 'success'
			result['status'] = 'success'
		# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

		_t5 = process_time()
		log.state("Total simulation time:  %s\n",
		          dvl.tools.format_time(_t5 - self._t_init))

		log.info(log_centred_msg(cfg.log.sim_finished.format(uni=self)))

		# Final analysis...
		log.highlight(log_centred_msg(cfg.log.final_analysis.format(uni=self)))
		self.perform_actions(self.actions.get('final'))
		_t6 = process_time()

		# Cleanup and save the data
		log.highlight(log_centred_msg(cfg.log.saving_data.format(uni=self)))

		# Calculate profiling info and add to result dict
		result['profile'] = self._create_profile()
		result['profile']['actions (final)'] = _t6 - _t5

		# Call cleanup and save on the monitors (and subclassed behaviour)
		self.perform_actions(self.actions.get('cleanup_and_save'))

		# Add to profile
		result['profile']['cleanup_and_save'] = process_time() - _t6

		# Save steps and exit status to metadata
		self.metadata.add_items(last_step=self.steps,
		                        exit_status=str(result['status']),
		                        exit_msg=str(result['status_msg']),
		                        profile=result['profile'])

		log.note("Finished saving data.")

		# All done.
		log.highlight(log_centred_msg(cfg.log.done.format(uni=self)))

		return result

	@abc.abstractmethod
	def run_simulation_step(self, **step_kwargs) -> bool:
		"""The method to perform one step of the simulation."""

	# Monitoring ..............................................................
	# ...extending the functionality inherited from parent class Environment
	# The Universe is able to look for monitor containers and perform action on all monitors in these containers.

	def add_monitor_containers(self, *containers):
		"""Adds multiple objects ("container") to look for further monitors in to the list of containers."""
		for container in containers:
			if (hasattr(container, "get_monitors")
			    and callable(container.get_monitors)):
				# has the capabilities of a monitor container
				self._monitor_containers.append(container)

			else:
				raise ValueError("Container {} has no callable method get_monitors".format(container))

	def gather_monitors(self):
		"""Gathers missing monitors from the list of monitor containers. This should be called previous to any monitor recordings."""
		for c in self._monitor_containers:
			for mon in c.get_monitors().values():
				if mon not in self.monitors.values():
					self.monitors[mon.uname] 	= mon

	def _create_profile(self) -> dict:
		"""Evaluates the time monitor to create profiling information."""
		prof 	= {}
		tm 		= self.time_monitor

		if not self._monitoring:
			return prof

		# Loop over columns and perform sums
		for var_name in self.time_monitor.keys():
			if var_name in ['cumulative total', 'step']:
				# summing over these does not make any sense, as they are already added up -> would sum to more than 100%
				continue

			prof[var_name] 			= float(np.sum(tm[var_name]))

		# Get the initialisation times for the universe (first value in cumulative total)
		prof['initialisation'] 	= float(tm['cumulative total'][0])

		return prof

	# Actions .................................................................

	def init_actions(self, d):
		"""Initialises the actions attribute, which is a dict of action lists.

		It is initalised from the given dictionary. Disabled entries are removed and functions are resolved, such that checks at runtime are minimsed.
		"""
		actions_dict = d if d else {}

		self.actions 	= {}

		# Loop over all different action lists in the actions dictionary
		for name, actions in actions_dict.items():
			# Generate a list to be filled with actions
			self.actions[name] = []

			if actions is None:
				# Nothing to do here
				continue

			# Loop over the list, only add the enabled actions, resolve action functions, and save to attributes
			for action in actions:
				if not action.pop('enabled', True):
					continue

				# Resolve action function and whether to append results
				_func 	= getattr(self, '_actn_' + action['func_name'])
				action['func'] 		= _func
				action['append_res']= getattr(_func, 'APPEND_RES', False)

				if isinstance(action.get('only_at_steps'), int):
					# Ensure it is a list
					action['only_at_steps'] 	= [action['only_at_steps']]

				# Save to attributes
				self.actions[name].append(action)

		log.debug("Action functions initialised.")

	def perform_actions(self, actions: list):
		"""Method to perform before, during, or after the simulation."""
		if not actions:
			return

		log.note("Performing %d actions ...", len(actions))

		for action in actions:
			func_name 	= action['func_name']
			results 	= []

			# Check whether to perform this action or not
			if not action.get('enabled', True):
				log.debug("%s not enabled ...", func_name)
				continue

			only_at	= action.get('only_at_steps')
			if only_at and self.steps not in only_at:
				log.debug("Skipping %s this step ...", func_name)
				continue
			else:
				log.progress("Performing %s ...", func_name)

			# Get the kwargs and check if it is a ParameterSpace
			kwargs 	= action.get('kwargs', {})

			if not isinstance(kwargs, ParamSpace):
				# Call the method
				res = action['func'](**kwargs)

				# Save the result in a list of results
				results.append(res)

			else:
				log.debug("Got a ParamSpace with volume %d to sweep over.",
				          kwargs.volume)

				fstr 	= action.get('state_fstr', '')

				# Loop over the Parameter Space
				for pt_no, _kwargs, state_str in kwargs.get_points(fstr=fstr):
					# Call the method
					try:
						res = action['func'](pt_no=pt_no, state_str=state_str,
					    	                 **_kwargs)
						# NOTE will appear as keys 'pt' and 'state'
					except Exception as err:
						log.error("There has been an error in action %s: %s\nArguments:\n%s\n", func_name, err, dvl.tools.PP.pformat(_kwargs))
						if DEBUG:
							raise

					# Save the result in a list of results
					results.append(res)

			# Save the result to the action_res dict
			for res in results:
				if res is None:
					continue

				# Else: There was a result.
				res_d = dict(at_step=self.steps, res=res)
				# Distinguish overwriting and appending
				if not action['append_res']:
					if isinstance(kwargs, ParamSpace):
						log.warning("Action %s was not configured to append the result, but was called with a ParamSpace for method kwargs; only the last result will be available.", func_name)

					# Overwrite
					self.action_res[func_name] = res_d
					log.debug("Saved results of step %d", self.steps)

				else:
					# Append
					log.debug("Appending result ...")
					if func_name not in self.action_res:
						# No result yet; create a list
						self.action_res[func_name] = []

					# Add to list
					self.action_res[func_name].append(res_d)

					log.debug("Results of step %d appended.", self.steps)

	# Action methods . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	# NOTE have to start with _actn_

	def _actn_plot_monitors(self, **kwargs):
		"""Plot the monitors, but exclude the time monitor. To plot the exec_time, use the plot_time_monitor method."""

		# Exclude the execution time monitor
		exclude = kwargs.pop('exclude', [])
		if 'exec_time' not in exclude:
			exclude.append('exec_time')

		super().plot_monitors(exclude=exclude, **kwargs)

	def _actn_plot_time_monitor(self, variables: str='all'):
		"""Plot the execution time monitor"""
		log.note("Plotting execution time monitor ...")
		self.time_monitor.plot(variables)

	def _actn_save_monitors(self, cleanup_monitors: bool=True):
		"""Clean up and save the Universe monitors"""
		log.progress("Cleaning up monitors ...")
		if cleanup_monitors:
			self.save_monitors(cleanup=True, close=False)
			# ...but do not close

		log.progress("Saving monitors ...")
		self.save_monitors(cleanup=False, close=True)

		log.note("Monitors cleaned up and saved.")

	def _actn_save_action_results(self, exclude: list=None, update: bool=False):
		"""Saves the action results to the metadata dictionary."""

		if update:
			raise NotImplementedError

		exclude = {} if not exclude else exclude

		d = {k:v for k,v in self.action_res.items() if k not in exclude}
		self.metadata.update_item('action_res', d)

		log.note("Saved action results.")
		return

	# Stop conditions .........................................................

	def init_stop_conditions(self, *, to_check: list=None, **stop_kwargs):
		"""Here, the stop condition functions are initialised and all the required checks are performed; this way, they don't have to be performed during the simulation."""

		if not to_check:
			log.info("No stop conditions to initialise.")
			return

		log.info("Initialising %d stop conditions ...", len(to_check))

		# Set the enabled flag
		self.stop_conds['enabled'] = True

		# Loop over the list of stop conditions that are to be checked
		for sc_name in to_check:
			try:
				func = getattr(self, "_sc_" + sc_name)
			except AttributeError:
				log.error("%s has no stop condition method _sc_%s! Not adding this stop condition.", self.__class__.__name__, sc_name)
				continue
			else:
				# Check if the required monitors are available
				if not all([m in self.monitors for m in func.REQUIRED_MONS]):
					log.warning("Stop condition %s is missing a required monitor; not adding this stop condition."\
					            "\nRequired: %s\nAvailable: %s",
					            sc_name,
					            ", ".join(func.REQUIRED_MONS),
					            ", ".join(self.monitors))
					continue

				# Save the stop condition function and parameters to attribute
				sc_dict 						= self.stop_conds
				sc_dict['funcs'][sc_name] 		= func
				_params							= stop_kwargs.get(sc_name, {})
				if not _params:
					log.caution("No parameters for stop condition '%s' found in configuration.", sc_name)
				sc_dict['check_every'][sc_name]	= _params.pop('check_every', 1)
				sc_dict['min_steps'][sc_name]	= _params.pop('min_steps', 0)
				sc_dict['params'][sc_name] 		= _params

		log.note("%d stop conditions initialised.",
		         len(self.stop_conds['funcs']))

	def check_stop_conditions(self) -> str:
		"""This method is called by run_simulation after run_simulation_step is called. By adding private methods (starting with _sc_) and adding their names (without prefix) to the to_check argument, checks on the current state of the Universe are performed to determine whether to break out of the simulation loop, i.e. stop the simulation.

		NOTE Universe-specific stop condition checks should be implemented in the project universe and without subclassing this method, but with adding _sc_my_stop_condition methods to the subclassed Universe.
		"""

		if not self.stop_conds['enabled']:
			log.debug("Checking of stop conditions disabled.")
			return False, None, None

		if not self.stop_conds['funcs']:
			log.warning("No stop conditions initialised. Call method init_stop_conditions() before calling check_stop_conditions")
			return False, None, None

		log.info("Checking stop condition with %d methods ...",
		          len(self.stop_conds['funcs']))

		for name, sc_func in self.stop_conds['funcs'].items():
			# Check whether the minimum number of steps is reached and whether to check every step
			if self.stop_conds['check_every'][name] > 1:
				if self.steps % self.stop_conds['check_every'][name] != 0:
					continue

			if self.stop_conds['min_steps'][name] > self.steps:
				continue

			log.progress("Checking %s ...", name)

			# Call the method
			ret_val	= sc_func.__call__(**self.stop_conds['params'][name])
			if ret_val:
				# Using the return value, generate a message
				msg = cfg.sc_fstr.format(step=self.steps, msg=ret_val,
				                         name=name)

				log.highlight("Stop condition '%s' fulfilled:  %s", name, msg)
				return True, name, msg

		log.note("All stop conditions evaluated to False. Continuing.")

		return False, None, None

	# Stop condition methods . . . . . . . . . . . . . . . . . . . . . . . . .
	# ... starting with _sc_

	def _sc_timeout(self, mode: str, mean: int=1, maxtime: dict=None) -> tuple:
		"""Checks whether the timeout stop condition was fulfilled."""

		# Check parameters
		if not isinstance(maxtime, dict):
			log.warning("No maxtime argument given to stop condition timeout.")
			return False
		max_step 	= maxtime.get('step', None)
		max_total 	= maxtime.get('total', None)

		# Get time monitor values and calculate the actual time
		n 			= min(len(self.time_monitor), mean)
		steptime 	= np.mean(self.time_monitor['step'][-n:])
		totaltime 	= self.time_monitor['cumulative total'][-1]

		timeout 		= False
		step_timeout	= bool(steptime > max_step)
		total_timeout	= bool(totaltime > max_total)

		if mode == 'step' and step_timeout:
			log.highlight("Timeout on simulation step!  %.1fs > %.1fs", steptime, max_step)
			timeout = True

		elif mode == 'total' and total_timeout:
			log.highlight("Timeout on total time!  %.1fs > %.1fs", totaltime, max_total)
			timeout = True

		elif mode == 'any' and (step_timeout or total_timeout):
			log.highlight("Timeout on simulation step or total time!  %.1fs > %.1fs  ;  %.1fs > %.1fs", steptime, max_step, totaltime, max_total)
			timeout = True

		elif mode == 'both' and step_timeout and total_timeout:
			log.highlight("Timeout on simulation step and total time!  %.1fs > %.1fs  ;  %.1fs > %.1fs", steptime, max_step, totaltime, max_total)
			timeout = True

		if timeout:
			msg = "n_stp={:d}, {:}{:}".format(self.steps,
			                                  "step > {:.1f} ".format(max_step) if step_timeout else "",
			                                  "total > {:.1f} ".format(max_total) if total_timeout else "")
			return msg
		else:
			return False
	_sc_timeout.REQUIRED_MONS = ['exec_time']
